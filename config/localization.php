<?php

return [

    /* -----------------------------------------------------------------
     |  Settings
     | -----------------------------------------------------------------
     */

    'supported-locales'      => ['ru', 'en', 'ky'],

    'accept-language-header' => true,

    'hide-default-in-url'    => true,

    'redirection-code'       => 302,

    'utf-8-suffix'           => '.UTF-8',

    /* -----------------------------------------------------------------
     |  Route
     | -----------------------------------------------------------------
     */

    'route'                  => [
        'middleware' => [
            'localization-session-redirect' => true,
            'localization-cookie-redirect'  => false,
            'localization-redirect'         => true,
            'localized-routes'              => true,
            'translation-redirect'          => true,
        ],
    ],

    /* -----------------------------------------------------------------
     |  Ignored URI/Routes from localization
     | -----------------------------------------------------------------
     */

    'ignored-uri' => [
        //
    ],

    'ignored-routes' => [
        //
    ],

    /* -----------------------------------------------------------------
     |  Locales
     | -----------------------------------------------------------------
     */

    'locales'   => [
        'en'          => [
            'name'     => 'English',
            'script'   => 'Latn',
            'dir'      => 'ltr',
            'native'   => 'English',
            'regional' => 'en_GB',
        ],
        'ky'          => [
            'name'     => 'Kyrgyz',
            'script'   => 'Cyrl',
            'dir'      => 'ltr',
            'native'   => 'Кыргыз',
            'regional' => 'ky_KG',
        ],
        'ru'          => [
            'name'     => 'Russian',
            'script'   => 'Cyrl',
            'dir'      => 'ltr',
            'native'   => 'Русский',
            'regional' => 'ru_RU',
        ],
    ],

];
