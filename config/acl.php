<?php
return [
    'acl_scheme' => [
        'permissions' => [
            'view' => [
                'IndexController@index',
                'IndexController@edit',
                'ImagesController@index',
            ],
            'add' => [
                'IndexController@create',
                'IndexController@store',
                'ImagesController@store',
            ],
            'edit' => [
                'IndexController@update',
                'IndexController@priority',
                'IndexController@images',
                'IndexController@deleteUpload',
                'ImagesController@update',
                'ImagesController@destroy'

            ],
            'delete' => [
                'IndexController@destroy'
            ]
        ],
        'own' => false,

    ],
    'root_group_ids' => [1],
    'always_allow' => [
        'admins' => ['ProfileController@edit', 'ProfileController@update']
    ]
];