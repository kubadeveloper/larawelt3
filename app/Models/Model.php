<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as ParentModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Schema;

/**
 * App\Models\Model
 *
 * @method static Builder|Model active()
 * @method static Builder|Model admin()
 * @method static Builder|Model filtered()
 * @method static Builder|Model items()
 * @method static Builder|Model order()
 * @mixin \Eloquent
 */
class Model extends ParentModel
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function scopeAdmin($query)
    {
        return $query->filtered()->order();
    }

    public function scopeSitemap(Builder $query)
    {
        return $query->order();
    }

    public function scopeFiltered($query)
    {
        return $query;
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeOrder($query)
    {
        return $query;
    }

    public function scopeItems($query)
    {
        return $query->order()->active();
    }

    public static function getTableStatic()
    {
        return with(new static)->getTable();
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            with(new static)->beforeCreate($model);
        });
        static::updating(function ($model) {
            with(new static)->beforeSave($model);
        });

        with(new static)->addGlobalScopes();
    }

    protected function addGlobalScopes()
    {
        if (Schema::hasColumn(self::getTableStatic(), 'lang')) {
            static::addGlobalScope('lang', function (Builder $builder) {
                $builder->where('lang', '=', lang());
            });
        }
    }

    protected function beforeCreate($model)
    {
        $columns = Schema::getColumnListing($model->getTable());
        if (in_array('lang', $columns)) {
            $model->lang = lang();
        }
    }

    protected function beforeSave($model)
    {
        //
    }
}
