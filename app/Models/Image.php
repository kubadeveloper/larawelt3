<?php

namespace App\Models;

class Image
{
    public function __construct($entity, $field, $thumbs)
    {
        foreach ($thumbs as $thumb => $data) {
            $this->{$thumb} = $entity->getImagePath($field, $thumb);
        }
    }

    public function __get($name)
    {
        return null;
    }
}
