<?php

namespace App\Models;

use Baum\Extensions\Eloquent\Collection;
use Baum\Node;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Schema;

/**
 * App\Models\Tree
 *
 * @property-read Collection|Tree[] $children
 * @property-read Tree $parent
 * @method static Builder|Tree active()
 * @method static Builder|Tree admin()
 * @method static Builder|Tree filtered()
 * @method static Builder|Tree items()
 * @method static Builder|Tree order()
 * @method static Builder|Node limitDepth($limit)
 * @method static Builder|Node withoutNode($node)
 * @method static Builder|Node withoutRoot()
 * @method static Builder|Node withoutSelf()
 * @mixin \Eloquent
 */
class Tree extends Node
{
    protected $guarded      = ['id', 'parent_id', 'lidx', 'ridx', 'depth'];

    public function scopeAdmin($query)
    {
        return $query->filtered()->order();
    }

    public function scopeSitemap(Builder $query)
    {
        return $query->active()->where('slug', '!=', '');
    }

    public function scopeFiltered($query)
    {
        return $query;
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeItems($query)
    {
        return $query->order()->active();
    }

    public function scopeOrder($query)
    {
        return $query->orderBy($this->getLeftColumnName());
    }

    public static function getTableStatic()
    {
        return with(new static)->getTable();
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            self::beforeCreate($model);
        });

        with(new static)->addGlobalScopes();
    }

    protected function addGlobalScopes()
    {
        if (Schema::hasColumn(self::getTableStatic(), 'lang')) {
            static::addGlobalScope('lang', function (Builder $builder) {
                $builder->where('lang', '=', lang());
            });
        }
    }

    protected static function beforeCreate($model)
    {
        $columns = Schema::getColumnListing($model->getTable());
        if (in_array('lang', $columns)) {
            $model->lang = lang();
        }
    }
}
