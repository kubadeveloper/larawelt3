<?php

namespace App\Models;

class Files
{
    public function __construct($entity, array $fields)
    {
        foreach ($fields as $field) {
            $this->{$field} = $entity->getFilePath($field);
        }
    }

    public function __get($name)
    {
        return null;
    }
}
