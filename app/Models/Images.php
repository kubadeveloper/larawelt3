<?php

namespace App\Models;

class Images
{
    public function __construct($entity, array $fields, array $configs)
    {
        foreach ($fields as $field) {
            if (array_key_exists($field, $configs) && array_key_exists('thumbs', $configs[$field])) {
                $this->{$field} = new Image($entity, $field, $configs[$field]['thumbs']);
            }
        }
    }

    public function __get($name)
    {
        return null;
    }
}
