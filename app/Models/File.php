<?php

namespace App\Models;

trait File
{
    /**
     * Получение всех полей изображений и файлов.
     *
     * @return object
     */
    public function getUploadsAttribute()
    {
        $module = $this->getModule();
        $uploads = [];

        $uploads['images'] = new Images($this, $this->getImageFields(), module_config('uploads', $module));
        $uploads['files'] = new Files($this, $this->getFileFields());

        return (object)$uploads;
    }

    /**
     * Возвращает названия полей изображений и файлов.
     *
     * @return array
     */
    public function getUploadFields()
    {
        return array_merge($this->getImageFields(), $this->getFileFields());
    }

    /**
     * Возвращает массив названия полей изображений.
     * Для загрузки изображений на сервер в модели определить массив `$imageFields`.
     *
     * @return array
     */
    public function getImageFields()
    {
        return ($this->imageFields ?: []);
    }

    /**
     * Возвращает массив назавния полей файлов.
     * Для загрузки файлов на сервер в модели определить массив `$fileFields`.
     *
     * @return array
     */
    public function getFileFields()
    {
        return ($this->fileFields) ?: [];
    }

    public function getImagePath($field, $slug, $module = null)
    {
        $module = ($module) ?: $this->getModule();
        $image = $this->{$field};

        if ($image && is_file(public_path($this->getPrefixPath($field, $module) . $this->getSlugPath($field, $slug, $module) . $image))) {
            return asset($this->getPrefixPath($field, $module) . $this->getSlugPath($field, $slug, $module) . $image);
        }

        return null;
    }

    public function getFilePath($field, $module = null)
    {
        $module = ($module) ?: $this->getModule();
        $file = $this->{$field};

        if ($file && is_file(public_path($this->getPrefixPath($field, $module) . $file))) {
            return asset($this->getPrefixPath($field, $module) . $file);
        }

        return null;
    }

    /**
     * Возвращает оригинальное название файла
     *
     * @param $field
     *
     * @return bool|\Illuminate\Config\Repository|mixed
     */
    public function getFieldName($field)
    {
        return module_config('uploads.' . $field . '.field_name', $this->getModule());
    }

    /**
     * Возвращает полный путь к файлу.
     *
     * @param $field
     *
     * @return string
     */
    public function getFileAbsPath($field)
    {
        return public_path($this->getPrefixPath($field, $this->getModule()) . $this->{$field});
    }

    /**
     * Возвращает префикс пути поля `$field`.
     *
     * @param $field
     * @param $module
     *
     * @return bool|\Illuminate\Config\Repository|mixed
     */
    protected function getPrefixPath($field, $module)
    {
        return module_config('uploads.' . $field . '.path', $module);
    }

    protected function getSlugPath($field, $slug, $module)
    {
        return module_config('uploads.' . $field . '.thumbs.' . $slug . '.path', $module);
    }

    /**
     * Возвращает название модуля.
     *
     * @return string
     */
    protected function getModule() : string
    {
        if(!$this->module) {
            preg_match('!App\\\Modules\\\(.*)\\\!isU', get_class($this), $matches);

            return lcfirst($matches[1]);
        }

        return $this->module;
    }
}
