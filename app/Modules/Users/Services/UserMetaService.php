<?php

namespace App\Modules\Users\Services;

use App\Modules\Tree\Facades\Breadcrumbs;
use App\Modules\Tree\Facades\Tree;
use Illuminate\Support\Facades\View;

class UserMetaService
{
    /**
     * Запись данных о странице регистрации.
     *
     * @return void
     */
    public function setUserRegisterPageData() : void
    {
        $title = trans('users::auth.register.title');
        $this->setViewData($this->getOgData($title), $this->getMetaData($title));
        $this->setBreadcrumbsData($title, route('user.register'));
    }

    /**
     * Запись данных о странице активации учетной записи.
     *
     * @return void
     */
    public function setUserConfirmPageData() : void
    {
        $title = trans('users::auth.confirmed.title');
        $this->setViewData($this->getOgData($title), $this->getMetaData($title));
        $this->setBreadcrumbsData($title, route('user.register'));
    }

    /**
     * Запись данных о странице входа в личный кабинет.
     *
     * @return void
     */
    public function setLoginPageData() : void
    {
        $title = trans('users::auth.login.title');
        $this->setViewData($this->getOgData($title), $this->getMetaData($title));
        $this->setBreadcrumbsData($title, route('user.login'));
    }

    /**
     * Запись данных о странице восстановления пароля.
     *
     * @return void
     */
    public function setPasswordResetPageData() : void
    {
        $title = trans('users::auth.reset.title');
        $this->setViewData($this->getOgData($title), $this->getMetaData($title));
        $this->setBreadcrumbsData($title, route('user.password.email'));
    }

    /**
     * Запись данных о личном кабинете.
     *
     * @return void
     */
    public function setUserRoomPageData() : void
    {
        $title = trans('users::index.title');
        $this->setViewData($this->getOgData($title), $this->getMetaData($title));
        $this->setBreadcrumbsData($title, route('user.room'));
    }

    /**
     * Запись данный в представление.
     *
     * @param array $og
     * @param array $meta
     * @return void
     */
    protected function setViewData(array $og, array $meta) : void
    {
        View::share('og', (object)$og);
        View::share('meta', (object)$meta);
    }

    /**
     * Запись данных о хлебных крошках.
     *
     * @param $title
     * @param $url
     * @return void
     */
    protected function setBreadcrumbsData($title, $url) : void
    {
        Breadcrumbs::add(Tree::getRoot()->title, home());
        Breadcrumbs::add($title, $url);
    }

    /**
     * Получение данный для мета тегов og.
     *
     * @param string $title
     * @return array
     */
    protected function getOgData(string $title) : array
    {
        return [
            'site_name' => config('app.name'),
            'image'     => asset(config('cms.og-image')),
            'title'     => $title
        ];
    }

    /**
     * Получение данных о названии страницы.
     *
     * @param string $title
     * @param string $h1
     * @return array
     */
    protected function getMetaData(string $title, string $h1 = '') : array
    {
        return [
            'title' => $title,
            'h1'    => $h1 ?: $title
        ];
    }
}
