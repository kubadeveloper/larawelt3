<?php
return [
    'name'                  => 'Ф.И.О',
    'phone'                 => 'Телефон',
    'email'                 => 'Email',
    'password'              => 'Пароль',
    'new_password'          => 'Новый пароль',
    'password_confirmation' => 'Повторите пароль',
    'active'                => 'Активировать',
    'banned'                => 'Заблокировать',
    'remember'              => 'Запомнить меня'
];
