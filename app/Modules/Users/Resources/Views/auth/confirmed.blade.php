@extends('layouts.inner')

@section('content')
    <h5 class="text-center">@lang('users::auth.confirmed.login')</h5>

    <div class="col-md-12 text-center">
        <a class="btn btn-success" href="{{ route('user.login') }}">
            @lang('users::auth.action.login')
        </a>
    </div>
@endsection
