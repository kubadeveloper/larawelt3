@extends('layouts.inner')

@section('content')
    <div class="login">
        {!! AdminBootForm::open(['route' => 'user.login', 'method' => 'post']) !!}

        <div class="col-md-6 col-md-offset-3">
            <div class="col-md-12">
                {!! AdminBootForm::email('email', trans('users::fields.email')) !!}
            </div>

            <div class="col-md-12">
                {!! AdminBootForm::password('password', trans('users::fields.password')) !!}
            </div>

            <div class="col-md-12" style="margin-bottom: 20px">
                <input type="checkbox" name="remember" id="remember">
                <label for="remember">@lang('users::fields.remember')</label>

                <div class="pull-right">
                    <a href="{{ route('user.password.email') }}">
                        @lang('users::auth.action.reset')
                    </a>
                </div>
            </div>

            <div class="col-md-12">
                {!! AdminBootForm::submit(trans('users::auth.action.login')) !!}
            </div>
        </div>

        {!! AdminBootForm::close() !!}
    </div>
@endsection
