@extends('layouts.inner')

@section('content')
    <div class="register">
        @if(Session::get('success'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissible">
                    <h4>@lang('users::auth.register.thank')</h4>
                    @lang('users::auth.register.email_sent')
                </div>
            </div>
        @else
            {!! AdminBootForm::open(['route' => 'user.register', 'method' => 'post']) !!}

            <div class="col-md-6 col-md-offset-3">
                <div class="col-md-12">
                    {!! AdminBootForm::text('name', trans('users::fields.name')) !!}
                </div>

                <div class="col-md-12">
                    {!! AdminBootForm::text('phone', trans('users::fields.phone')) !!}
                </div>

                <div class="col-md-12">
                    {!! AdminBootForm::email('email', trans('users::fields.email')) !!}
                </div>

                <div class="col-md-12">
                    {!! AdminBootForm::password('password', trans('users::fields.password')) !!}
                </div>

                <div class="col-md-12">
                    {!! AdminBootForm::password('password_confirmation', trans('users::fields.password_confirmation')) !!}
                </div>

                <div class="col-md-12">
                    {!! AdminBootForm::submit(trans('users::auth.register.submit')) !!}
                </div>
            </div>

            {!! AdminBootForm::close() !!}
        @endif
    </div>
@endsection
