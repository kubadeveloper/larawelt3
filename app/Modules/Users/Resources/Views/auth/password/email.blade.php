@extends('layouts.inner')

@section('content')
    <div class="reset_password">
        @if(Session::get('status'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissible">{{ Session::get('status') }}</div>
            </div>
        @else
            {!! AdminBootForm::open(['route' => 'user.password.email', 'method' => 'post']) !!}

            <div class="col-md-6 col-md-offset-3">
                <div class="col-md-12">
                    {!! AdminBootForm::email('email', trans('users::fields.email')) !!}
                </div>

                <div class="col-md-12">
                    {!! AdminBootForm::submit(trans('users::index.send')) !!}
                </div>
            </div>

            {!! AdminBootForm::close() !!}
        @endif
    </div>
@endsection
