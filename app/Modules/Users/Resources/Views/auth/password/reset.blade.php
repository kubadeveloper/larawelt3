@extends('layouts.inner')

@section('content')
    <div class="reset_password">
        {!! AdminBootForm::open(['route' => 'user.password.reset', 'method' => 'post']) !!}

        {!! AdminBootForm::hidden('token', $token) !!}
        {!! AdminBootForm::hidden('email', $user->email) !!}

        <div class="col-md-6 col-md-offset-3">
            @if($errors->has('token') || $errors->has('email'))
                <div class="col-md-12 has-error">
                    <div class="help-block">
                        {{ $errors->has('token') ? $errors->first('token') : $errors->first('email') }}
                    </div>
                </div>
            @endif

            <div class="col-md-12">
                {!! AdminBootForm::password('password', trans('users::fields.new_password')) !!}
            </div>

            <div class="col-md-12">
                {!! AdminBootForm::password('password_confirmation', trans('users::fields.password_confirmation')) !!}
            </div>

            <div class="col-md-12">
                {!! AdminBootForm::submit(trans('users::auth.action.reset')) !!}
            </div>
        </div>

        {!! AdminBootForm::close() !!}
    </div>
@endsection
