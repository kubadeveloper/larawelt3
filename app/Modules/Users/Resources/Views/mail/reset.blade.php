<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
</head>

<?php
$fontFamily = 'font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;';
$style = [
    /* Layout ------------------------------ */
    'body'                => 'margin: 0;padding: 0; width: 100%;',
    'email-wrapper'       => 'width: 100%; margin: 0; padding: 0; background-color: #3e3f3a; border: 1px solid #555555;
        border-radius: 5px; box-sizing: border-box;',

    /* Masthead ---------------------------- */
    'email-masthead'      => 'padding: 25px 0; text-align: center;',
    'email-masthead_name' => 'font-size: 25px; color: #FFF; text-decoration: none; text-shadow: 0 1px 0 #fff;',

    /* Mastbody ---------------------------- */
    'email-body'          => 'width: 100%; margin: 0; padding: 0; border-top: 1px solid #555555;
        border-bottom: 1px solid #555555; background-color: #FFF;',
    'email-body_head'     => 'color: #898989; font-size: 18px;',
    'email-body_inner'    => 'width: auto; max-width: 700px; margin: 0 auto; padding: 0;',
    'email-body_button'   => 'font-size: 17px; color: #555555; text-decoration: none; line-height: 50px;
        padding: 0 40px; text-align: center; border: 1px solid #555555; text-transform: uppercase;
        display: inline-block; margin-top: 20px; margin-bottom: 30px;',
    'email-body_link'     => 'font-size: 13px; color: #007dbe; display: inline-block; border-bottom: 1px solid #007dbe;
        text-decoration: none; line-height: 14px; margin-bottom: 25px;',

    /* Mastfooter -------------------------- */
    'email-footer_cell'   => 'color: #FFF; padding: 25px; text-align: center;',
    'paragraph-footer'    => 'color: #FFF; font-size: 12px; line-height: 1.5em;',

    /* Type -------------------------------- */
    'paragraph'           => 'margin: 0; color: #898989; font-size: 16px; line-height: 1.5em;',

    /* Table ------------------------------- */
    'table'               => [
        'td' => 'text-align: center;',
    ]
];
?>

<body style="{{ $style['body'] }}">
    <table cellpadding="0" cellspacing="0" style="{{ $style['email-wrapper'] }}">
        <tr>
            <td style="{{ $style['email-masthead'] }}">
                <a style="{{ $fontFamily }} {{ $style['email-masthead_name'] }}" href="{{ home() }}" target="_blank">
                    @lang('users::mail.reset_password.title', ['name' => config('app.name')])
                </a>
            </td>
        </tr>

        <tr>
            <td style="{{ $style['email-body'] }}">
                <table style="{{ $style['email-body_inner'] }}" align="center" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td style="{{ $style['table']['td'] }}">
                                <p style="{{ $fontFamily }} {{ $style['email-body_head'] }}">
                                    @lang('users::mail.hello', ['name' => $user->name])
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="{{ $style['table']['td'] }}">
                                <p style=" {{ $fontFamily }}{{ $style['paragraph'] }}">
                                    @lang('users::mail.reset_password.get_mail')
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="{{ $style['table']['td'] }}">
                                <p style="{{ $fontFamily }} {{ $style['paragraph'] }}">
                                    @lang('users::mail.reset_password.reset')
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="{{ $style['table']['td'] }}">
                                <a href="{{ route('user.password.token', ['id' => $user->id, 'token' => $token]) }}"
                                   style="{{ $fontFamily }} {{ $style['email-body_button'] }}" target="_blank">
                                    @lang("users::mail.reset_password.button")
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="{{ $style['table']['td'] }}">
                                <p style="{{ $fontFamily }} {{ $style['paragraph'] }}">
                                    @lang('users::mail.btn_problem')
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="{{ $style['table']['td'] }}">
                                <a href="{{ route('user.password.token', ['id' => $user->id, 'token' => $token]) }}"
                                   style="{{ $fontFamily }} {{ $style['email-body_link'] }}" target="_blank">
                                    {{ route('user.password.token', ['id' => $user->id, 'token' => $token]) }}
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>

        <tr>
            <td style="{{ $fontFamily }} {{ $style['email-footer_cell'] }}">
                <p style="{{ $style['paragraph-footer'] }}">
                    @lang('index.copyright', ['end' => date('Y')])
                </p>
            </td>
        </tr>
    </table>
</body>
</html>
