@extends('layouts.inner')

@section('content')
    <div class="alert alert-success text-center">
        {{ Auth::user()->name }}
    </div>
@endsection
