<ul class="nav navbar-nav navbar-right">
    @if(Auth::check())
        <li><a href="{{ route('user.room') }}">{{ Auth::user()->name }}</a></li>
        <li><a href="{{ route('user.logout') }}">@lang('users::auth.action.logout')</a></li>
    @else
        <li><a href="{{ route('user.login') }}">@lang('users::auth.action.login')</a></li>
        <li><a href="{{ route('user.register') }}">@lang('users::auth.action.register')</a></li>
    @endif
</ul>
