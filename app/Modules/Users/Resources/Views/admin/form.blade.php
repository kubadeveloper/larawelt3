@extends('admin::admin.form')

@section('topmenu')
    <div class="header-module-controls">
        @include('admin::common.topmenu.list', ['routePrefix' => $routePrefix])
        @include('admin::common.topmenu.create', ['routePrefix' => $routePrefix])

        @if(method() === 'edit' && is_allowed(Acl::edit()))
            <div class="pull-right">
                @if(!$entity->active)
                    <a class="btn btn-success" href="{{ route('admin.users.activate', ['user' => $entity]) }}">
                        @lang('users::admin.topmenu.activate')
                    </a>
                @else
                    <span class="btn btn-success disabled">@lang('users::admin.topmenu.activated')</span>
                @endif

                @if(!$entity->banned)
                    <a class="btn btn-danger" href="{{ route('admin.users.ban', ['user' => $entity]) }}">
                        @lang('users::admin.topmenu.ban')
                    </a>
                @else
                    <a class="btn btn-primary" href="{{ route('admin.users.unban', ['user' => $entity]) }}">
                        @lang('users::admin.topmenu.unban')
                    </a>
                @endif
            </div>
        @endif
    </div>
@endsection

@section('form_content')
    {!! AdminBootForm::open([
        'model' => $entity,
        'store' => $routePrefix . 'store',
        'update' => $routePrefix . 'update',
        'autocomplete' => 'off',
        'files' => false
    ]) !!}

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                {!! AdminBootForm::text('name', trans('users::fields.name')) !!}
            </div>

            <div class="col-md-6">
                {!! AdminBootForm::text('phone', trans('users::fields.phone')) !!}
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                {!! AdminBootForm::email('email', trans('users::fields.email')) !!}
            </div>

            <div class="col-md-6">
                {!! AdminBootForm::password('password', trans('users::fields.password')) !!}

                @if($entity->id)
                    <span class="help-block">@lang('users::admin.password_tip')</span>
                @endif
            </div>
        </div>
    </div>
@endsection
