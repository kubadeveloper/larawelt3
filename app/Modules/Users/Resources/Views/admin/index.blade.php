@extends('admin::admin.index')

@section('filters')
    {!! AdminBootForm::open([ 'route' => $routePrefix . 'index', 'method' => 'get']) !!}
    <div class="box box-primary box-filters">
        <div class="box-body">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        {!! AdminBootForm::text('filters[email]', trans('users::fields.email')) !!}
                    </div>

                    <div class="col-md-6">
                        {!! AdminBootForm::text('filters[phone]', trans('users::fields.phone')) !!}
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                {!! AdminBootForm::submit(trans('admin::admin.select')) !!}
            </div>
        </div>
    </div>
    {!! AdminBootForm::close() !!}
@endsection

@section('th')
    <th>@sortablelink('created_at', trans('admin::fields.created_at'))</th>
    <th>@sortablelink('name', trans('users::fields.name'))</th>
    <th>@sortablelink('email', trans('users::fields.email'))</th>
    <th>@lang('users::admin.active')</th>
    <th>@lang('users::admin.banned')</th>
    <th>@lang('admin::admin.control')</th>
@endsection

@section('td')
    @foreach ($entities as $entity)
        <tr @if(!$entity->active || $entity->banned) class="unpublished" @endif>
            <td>{{ $entity->created_at }}</td>
            <td>{{ $entity->name }}</td>
            <td>{{ $entity->email }}</td>
            <td align="center">
                <input type="checkbox" style="pointer-events: none;" @if($entity->active) checked @endif>
            </td>
            <td align="center">
                <input type="checkbox" style="pointer-events: none;" @if($entity->banned) checked @endif>
            </td>
            <td>
                @include('admin::common.controls.edit', ['routePrefix' => $routePrefix, 'id' => $entity->id])
                @include('admin::common.controls.destroy', ['routePrefix' => $routePrefix, 'id' => $entity->id])
            </td>
        </tr>
    @endforeach
@endsection
