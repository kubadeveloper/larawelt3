<?php
return [
    'title'        => trans('users::admin.list'),
    'localization' => false,
    'front_link'   => false,
    'acl'          => true,
    'acl_scheme' => [
        'permissions' => [
            'view' => [
                'IndexController@index',
                'IndexController@edit'
            ],
            'add' => [
                'IndexController@create',
                'IndexController@store'
            ],
            'edit' => [
                'IndexController@update',
                'IndexController@activate',
                'IndexController@ban',
                'IndexController@unban'
            ],
            'delete' => [
                'IndexController@destroy'
            ]
        ]
    ]
];
