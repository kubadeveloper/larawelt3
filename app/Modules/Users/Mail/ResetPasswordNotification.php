<?php

namespace App\Modules\Users\Mail;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPasswordNotification extends Notification
{
    public $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    public function via($user)
    {
        return ['mail'];
    }

    public function toMail($user)
    {
        return (new MailMessage)
            ->subject(trans('users::mail.reset_password.title', ['name' => config('app.name')]))
            ->view('users::mail.reset', ['user' => $user, 'token' => $this->token]);
    }
}
