<?php

namespace App\Modules\Users\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Modules\Users\Models\User;

class RegisterConfirmMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function build()
    {
        return $this->from(config('mail.from'))
            ->subject(trans('users::mail.confirm.title', ['name' => config('app.name')]))
            ->view('users::mail.confirm');
    }
}
