<?php

namespace App\Modules\Users\Database\Seeds;

use Illuminate\Database\Seeder;
use App\Modules\Users\Models\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        factory(User::class, 5)->create();
    }
}
