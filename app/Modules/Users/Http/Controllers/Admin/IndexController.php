<?php

namespace App\Modules\Users\Http\Controllers\Admin;

use App\Modules\Admin\Http\Controllers\Admin;
use App\Modules\Users\Models\User;
use Illuminate\Validation\Rule;

class IndexController extends Admin
{
    protected $messages = [
        'store'    => 'admin::admin.messages.store',
        'update'   => 'admin::admin.messages.update',
        'destroy'  => 'admin::admin.messages.destroy',
        'activate' => 'users::admin.messages.activate',
        'ban'      => 'users::admin.messages.ban',
        'unban'    => 'users::admin.messages.unban'
    ];

    public function activate(User $user)
    {
        $user->activate();

        return redirect()->back()->with('message', trans($this->messages['activate']));
    }

    public function ban(User $user)
    {
        $user->ban();

        return redirect()->back()->with('message', trans($this->messages['ban']));
    }

    public function unban(User $user)
    {
        $user->unban();

        return redirect()->back()->with('message', trans($this->messages['unban']));
    }

    public function getRules($request, $id = false)
    {
        return [
            'name'     => 'required|max:255',
            'phone'    => 'nullable|max:255',
            'email'    => [
                'required', 'max:255', 'email',
                Rule::unique('users')->ignore($id)
            ],
            'password' => (method() === 'update' ? 'nullable' : 'required') . '|min:6'
        ];
    }

    public function getModel()
    {
        return new User();
    }
}
