<?php

namespace App\Modules\Users\Http\Controllers;

use App\Modules\Users\Http\Middleware\UserMeta;
use App\Modules\Users\Models\User;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        return view($this->getIndexViewName());
    }

    public function getModel()
    {
        return new User();
    }

    protected function setMiddleware()
    {
        $this->middleware(UserMeta::class);
    }
}
