<?php

namespace App\Modules\Users\Http\Controllers\Auth;

use App\Modules\Users\Http\Middleware\UserMeta;
use App\Modules\Users\Mail\RegisterConfirmMail;
use App\Modules\Users\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    use RedirectsUsers, ValidatesRequests;

    public function __construct()
    {
        $this->middleware(UserMeta::class);
    }

    public function showRegistrationForm()
    {
        return view('users::auth.register');
    }

    public function register(Request $request)
    {
        $this->validate($request, $this->rules(), $this->messages());

        event(new Registered($user = $this->create($request->all())));
        Mail::to($user)->send(new RegisterConfirmMail($user));

        return redirect()->back()->with('success', true);
    }

    public function confirm($token)
    {
        $user = $this->getModel()->newQuery()->where('token', $token)->firstOrFail();
        $user->activate();

        return view('users::auth.confirmed');
    }

    protected function rules() : array
    {
        return [
            'name'     => 'required|max:255',
            'phone'    => 'nullable|max:255',
            'email'    => 'required|max:255|email|unique:users',
            'password' => 'required|min:6|confirmed'
        ];
    }

    protected function messages() : array
    {
        return [
            'required'  => trans('users::validation.required'),
            'max'       => trans('users::validation.max'),
            'min'       => trans('users::validation.min'),
            'email'     => trans('users::validation.email'),
            'unique'    => trans('users::validation.unique'),
            'confirmed' => trans('users::validation.confirmed')
        ];
    }

    protected function create(array $data)
    {
        return $this->getModel()->newQuery()->create([
            'name'     => $data['name'],
            'phone'    => $data['phone'],
            'email'    => $data['email'],
            'password' => $data['password']
        ]);
    }

    protected function getModel()
    {
        return new User();
    }
}
