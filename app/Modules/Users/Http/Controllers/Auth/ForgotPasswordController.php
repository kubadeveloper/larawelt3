<?php

namespace App\Modules\Users\Http\Controllers\Auth;

use App\Modules\Users\Http\Middleware\UserMeta;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails, ValidatesRequests;

    public function __construct()
    {
        $this->middleware(UserMeta::class);
    }

    public function showLinkRequestForm()
    {
        return view('users::auth.password.email');
    }

    protected function validateEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ], [
            'required' => trans('users::validation.required'),
            'email'    => trans('users::validation.email')
        ]);
    }
}
