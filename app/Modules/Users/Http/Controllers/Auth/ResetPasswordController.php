<?php

namespace App\Modules\Users\Http\Controllers\Auth;

use App\Modules\Users\Http\Middleware\UserMeta;
use App\Modules\Users\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{
    use ResetsPasswords, ValidatesRequests;

    public function __construct()
    {
        $this->middleware(UserMeta::class);
    }

    public function showResetForm(User $user, $token)
    {
        return view('users::auth.password.reset', ['user' => $user, 'token' => $token]);
    }

    protected function resetPassword(User $user, $password)
    {
        $user->password = $password;

        $user->setRememberToken(Str::random(60));
        $user->save();

        event(new PasswordReset($user));

        $this->guard()->login($user);
    }

    protected function validationErrorMessages()
    {
        return [
            'token.required' => trans('passwords.token'),
            'email.required' => trans('passwords.token'),
            'email.email'    => trans('passwords.token'),
            'required'       => trans('users::validation.required'),
            'confirmed'      => trans('users::validation.confirmed'),
            'min'            => trans('users::validation.min')
        ];
    }

    protected function redirectPath()
    {
        return route('user.room');
    }
}
