<?php

namespace App\Modules\Users\Http\Controllers\Auth;

use App\Modules\Users\Http\Middleware\UserMeta;
use App\Modules\Users\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    use AuthenticatesUsers, ValidatesRequests;

    public function __construct()
    {
        $this->middleware(UserMeta::class);
    }

    public function showLoginForm()
    {
        return view('users::auth.login');
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
            'password'        => 'required|string'
        ], [
            'required' => trans('users::validation.required')
        ]);
    }

    protected function credentials(Request $request)
    {
        return array_merge($request->only($this->username(), 'password'), [
            'active' => 1, 'banned' => 0
        ]);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $user    = $this->getModel()->newQuery()->where($this->username(), $request->get($this->username()))->first();
        $message = 'auth.failed';

        if ($user) {
            if ($user->banned) $message = 'users::auth.login.is_banned';
            if (!$user->active) $message = 'users::auth.login.is_not_active';
        }

        throw ValidationException::withMessages([
            $this->username() => [trans($message)],
        ]);
    }

    protected function redirectPath()
    {
        return route('user.room');
    }

    protected function getModel()
    {
        return new User();
    }
}
