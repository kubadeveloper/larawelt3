<?php

namespace App\Modules\Users\Http\Middleware;

use App\Modules\Users\Services\UserMetaService;
use Closure;

class UserMeta
{
    protected $meta;

    public function __construct(UserMetaService $service)
    {
        $this->meta = $service;
    }

    public function handle($request, Closure $next)
    {
        switch (action()) {
            case 'RegisterController@showRegistrationForm':
                $this->meta->setUserRegisterPageData();
                break;
            case 'RegisterController@confirm':
                $this->meta->setUserConfirmPageData();
                break;
            case 'LoginController@showLoginForm':
                $this->meta->setLoginPageData();
                break;
            case 'ForgotPasswordController@showLinkRequestForm':
                $this->meta->setPasswordResetPageData();
                break;
            case 'ResetPasswordController@showResetForm':
                $this->meta->setPasswordResetPageData();
                break;
            case 'IndexController@index':
                $this->meta->setUserRoomPageData();
                break;
        }

        return $next($request);
    }
}
