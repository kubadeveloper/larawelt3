<?php

namespace App\Modules\Users\Models;

use App\Models\Filters;
use Illuminate\Database\Eloquent\Builder;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Modules\Users\Mail\ResetPasswordNotification;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable, Sortable, Filters;

    protected $guard = 'user';

    public $table = 'users';

    protected $fillable = ['name', 'phone', 'email', 'password'];

    protected $hidden = ['password', 'remember_token'];

    protected $filters = [
        'email' => ['email', 'LIKE', '%?%'],
        'phone' => ['phone', 'LIKE', '%?%']
    ];

    public function setPasswordAttribute($value)
    {
        if ($value) {
            $this->attributes['password'] = bcrypt($value);
        }
    }

    public function scopeAdmin(Builder $query)
    {
        return $query->filtered()->order();
    }

    public function scopeOrder(Builder $query)
    {
        return $query->latest('created_at');
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($user) {
            if (!$user->active) {
                $user->token = Str::random(30);
            }
        });
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function activate()
    {
        if (!$this->active) {
            $this->active = 1;
            $this->token  = null;
            $this->save();
        }
    }

    public function ban()
    {
        $this->banned = 1;
        if (!$this->active) {
            $this->active = 1;
            $this->token  = null;
        }
        $this->save();
    }

    public function unban()
    {
        $this->banned = 0;
        if (!$this->active) {
            $this->active = 1;
            $this->token  = null;
        }
        $this->save();
    }
}
