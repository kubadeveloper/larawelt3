<?php

Route::localizedGroup(function () {
    Route::group(['prefix' => config('cms.uri'), 'as' => config('cms.admin_prefix')], function () {
        Route::resource('users', 'Admin\IndexController');
        Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
            Route::get('/{user}/activate', 'Admin\IndexController@activate')->name('activate');
            Route::get('/{user}/ban', 'Admin\IndexController@ban')->name('ban');
            Route::get('/{user}/unban', 'Admin\IndexController@unban')->name('unban');
        });
    });

    Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
        Route::get('/', 'IndexController@index')->name('room');

        Route::group(['prefix' => 'register'], function () {
            Route::get('confirm/{token}', 'Auth\RegisterController@confirm')->name('register.confirm');
            Route::get('/', 'Auth\RegisterController@showRegistrationForm')->name('register');
            Route::post('/', 'Auth\RegisterController@register');
        });

        Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Auth\LoginController@login');
        Route::get('logout', 'Auth\LoginController@logout')->name('logout');

        Route::group(['prefix' => 'password', 'as' => 'password.'], function () {
            Route::get('email', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('email');
            Route::post('email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
            Route::get('reset/{user}/{token}', 'Auth\ResetPasswordController@showResetForm')
                ->name('token');
            Route::post('reset', 'Auth\ResetPasswordController@reset')->name('reset');
        });
    });
});
