<?php

namespace App\Modules\News\Models;

use App\Models\File;
use App\Models\Model;
use Illuminate\Database\Eloquent\Builder;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Notifications\Notifiable;

class News extends Model
{
    use Notifiable, Sortable, File;

    protected $imageFields = [
        'image'
    ];

    public function scopeOrder($query)
    {
        return $query->orderBy('date', 'desc');
    }
}
