<?php

namespace App\Modules\News\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NewsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('news')->insert([
            [
                'id'         => 1,
                'lang'       => 'ru',
                'title'      => 'Новости',
                'date'       => date("Y-m-d"),
                'preview'    => 'Новости дня',
                'content'    => 'Новости дня',
                'active'     => 1,
                'on_main'    => 0,
                'created_at' => now()
            ],
            [
                'id'         => 2,
                'lang'       => 'ru',
                'title'      => 'Новости BCC',
                'date'       => date("Y-m-d"),
                'preview'    => 'Новости BCC',
                'content'    => 'Новости BCC',
                'active'     => 1,
                'on_main'    => 1,
                'created_at' => now()
            ]
        ]);
    }
}
