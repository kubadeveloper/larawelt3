<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('lang', ['ru', 'en', 'ky'])->index();
            $table->integer('priority')->default(0)->index();
            $table->string('title');
            $table->date('date')->useCurrent();
            $table->mediumText('preview')->nullable();
            $table->text('content')->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('active');
            $table->tinyInteger('on_main');

            $table->string('meta_title')->nullable();
            $table->string('meta_h1')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->text('meta_description')->nullable();


            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('news');
    }
}
