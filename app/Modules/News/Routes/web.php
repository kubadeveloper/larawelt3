<?php
Route::localizedGroup(function () {
    Route::prefix(config('cms.uri'))
        ->as(config('cms.admin_prefix'))
        ->group(function () {
            Route::resource('news', 'Admin\IndexController');
            Route::delete('news/delete-upload/{id}/{field}', 'Admin\IndexController@deleteUpload')
                ->name('news.delete-upload');
        });
});