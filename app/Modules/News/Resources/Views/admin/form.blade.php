@extends('admin::admin.form')

@section('sidemenu')
    @include('admin::common.sidemenu.all')
@endsection

@section('form_content')
    {!! AdminBootForm::open([
        'model' => $entity,
        'store' => $routePrefix.'store',
        'update' => $routePrefix.'update',
        'autocomplete' => 'off',
        'files' => true
    ]) !!}

    <div class="col-md-6">
        {!! AdminBootForm::text('title', trans('admin::fields.title')) !!}

        <div class="row">
            <div class="col-md-6">
                {!! AdminBootForm::hidden('active', 0) !!}
                {!! AdminBootForm::checkbox('active', trans('admin::fields.active'), 1) !!}
            </div>

            <div class="col-md-6">
                {!! AdminBootForm::hidden('on_main', 0) !!}
                {!! AdminBootForm::checkbox('on_main', trans('admin::fields.on_main'), 1) !!}
            </div>
        </div>

        {!! AdminBootForm::textarea('preview', trans('admin::fields.preview'), null, ['rows' => '5']) !!}
    </div>

    <div class="col-md-6">
        {!! AdminBootForm::text('date', trans('admin::fields.date'), $entity->date ? null : date('Y-m-d')) !!}

        @include('admin::common.forms.image', ['entity' => $entity, 'routePrefix' => $routePrefix, 'field' => 'image'])
    </div>

    <div class="clearfix"></div>

    <div class="col-md-12">
        {!! AdminBootForm::textarea('content', trans('admin::fields.content')) !!}
    </div>

    @include('admin::common.forms.seo')
@endsection
