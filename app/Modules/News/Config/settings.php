<?php
return [
    'title'        => trans('news::admin.title'),
    'localization' => true,
    'front_link'   => true,
    'acl'          => true
];