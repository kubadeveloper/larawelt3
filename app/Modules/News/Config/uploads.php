<?php
return [
    'image' => [
        'path'      => '/uploads/news/',
        'validator' => 'mimes:jpeg,jpg,png|max:10000',
        'thumbs'    => [
            'full'  => [
                'path'   => 'full/',
                'width'  => 800,
                'height' => false
            ],
            'thumb' => [
                'path'   => 'thumb/',
                'width'  => 450,
                'height' => false,
            ],
            'mini'  => [
                'path'   => 'mini/',
                'width'  => 250,
                'height' => false
            ]
        ]
    ]
];
