<?php

namespace App\Modules\News\Providers;

use App\Modules\Admin\Providers\BaseModuleServiceProvider;
use App\Modules\News\Http\ViewComposers\MainComposer;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    public function register() : void
    {
        parent::register();

        $this->app->make('view')->composer('news::main', MainComposer::class);
    }
}
