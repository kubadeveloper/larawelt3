<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::prefix(config('cms.uri'))
    ->as(config('cms.admin_prefix'))
    ->group(function () {
        Route::resource('settings', 'Admin\IndexController');
    });