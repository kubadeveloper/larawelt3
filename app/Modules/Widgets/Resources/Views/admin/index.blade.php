@extends('admin::admin.index')

@section('topmenu')
    <div class="header-module-controls">
        @include('admin::common.topmenu.list', ['routePrefix' => $routePrefix])

        @if(config('app.develop'))
            @include('admin::common.topmenu.create', ['routePrefix' => $routePrefix])
        @endif
    </div>
@endsection

@section('th')
    <th>@sortablelink('slug', trans('admin::fields.slug'))</th>
    <th>@sortablelink('title', trans('admin::fields.title'))</th>
    <th>@lang('admin::admin.control')</th>
@endsection

@section('td')
    @foreach($entities as $entity)
        <tr @if(!$entity->active) class="unpublished" @endif>
            <td>{{ $entity->slug }}</td>
            <td>{{ $entity->title }}</td>
            <td class="controls">
                @include('admin::common.controls.publish', ['routePrefix' => $routePrefix, 'id' => $entity->id])
                @include('admin::common.controls.edit', ['routePrefix' => $routePrefix, 'id' => $entity->id])

                @if(config('app.develop'))
                    @include('admin::common.controls.destroy', ['routePrefix' => $routePrefix, 'id' => $entity->id])
                @else
                    <span class="btn btn-danger btn-sm disabled" title="@lang('admin::admin.access')">
                        <i class="glyphicon glyphicon-trash"></i>
                    </span>
                @endif
            </td>
        </tr>
    @endforeach
@endsection
