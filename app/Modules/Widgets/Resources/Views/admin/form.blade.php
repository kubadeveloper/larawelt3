@extends('admin::layouts.app')

@section('title')
    <h2><a href="{!! route($routePrefix . 'index') !!}">{{ $title }}</a></h2>
@endsection

@section('form_js')
    @if ($entity->type == 'wysiwyg')
        @include('admin::common.forms.ckeditor', ['fields' => [['id' => 'content']]])
    @endif
@endsection

@section('topmenu')
    <div class="header-module-controls">
        @include('admin::common.topmenu.list', ['routePrefix' => $routePrefix])

        @if(config('app.develop'))
            @include('admin::common.topmenu.create', ['routePrefix' => $routePrefix])
        @endif
    </div>
@endsection

@section('content')
    <div class="panel-body">
        @include('admin::common.errors')

        {!! AdminBootForm::open([
            'model' => $entity,
            'store' => $routePrefix . 'store',
            'update' => $routePrefix . 'update',
            'autocomplete' => 'off',
            'files' => true
        ]) !!}

        <div class="col-md-6">
            {!! AdminBootForm::text('title', trans('admin::fields.title')) !!}
        </div>

        <div class="col-md-6">
            {!! AdminBootForm::text('slug', trans('admin::fields.slug'), null, [
                (config('app.develop')) ? null : 'readonly'
            ]) !!}
        </div>

        <div class="col-md-6">
            {!! AdminBootForm::hidden('active', 0) !!}
            {!! AdminBootForm::checkbox('active', trans('admin::fields.active'), 1) !!}
        </div>

        <div class="col-md-6">
            {!! AdminBootForm::select('type', trans('admin::fields.type'), [
                'html' => trans('widgets::admin.html'),
                'wysiwyg' => trans('widgets::admin.wysiwyg')
            ]) !!}
        </div>

        <div class="col-md-12">
            {!! AdminBootForm::textarea('content', trans('admin::fields.content')) !!}
        </div>

        @if ( (method() == 'edit' && is_allowed(Acl::edit())) || (method() =='create' && is_allowed(Acl::add())))
            <div class="col-md-12">
                {!! AdminBootForm::submit(trans('admin::admin.save')) !!}
            </div>
        @endif

        {!! AdminBootForm::close() !!}
    </div>
@endsection
