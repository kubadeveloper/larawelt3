<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWidgetsTable extends Migration
{
    public function up()
    {
        Schema::create('widgets', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('lang', ['ru', 'en', 'ky'])->index();
            $table->tinyInteger('protected')->nullable();
            $table->tinyInteger('active');

            $table->enum('type', ['html', 'wysiwyg'])->index();

            $table->string('slug', 50)->index();
            $table->string('title');
            $table->text('content');
            $table->text('description')->nullable();

            $table->timestamps();
        });

    }

    public function down()
    {
        //
    }
}
