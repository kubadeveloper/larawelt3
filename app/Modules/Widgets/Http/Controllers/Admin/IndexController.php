<?php

namespace App\Modules\Widgets\Http\Controllers\Admin;

use App\Modules\Admin\Http\Controllers\Admin;
use App\Modules\Widgets\Models\Widget;

class IndexController extends Admin
{
    public function getModel()
    {
        return new Widget();
    }

    public function getRules($request, $id = false)
    {
        return [
            'title'   => 'sometimes|required|max:255',
            'content' => 'sometimes|required',
            'slug'    => 'sometimes|required',
        ];
    }

    public function create()
    {
        if (!config('app.develop')) {
            return view('admin::access');
        }

        $entity = $this->getModel();

        $this->after($entity);

        return view($this->getFormViewName(), ['entity' => $entity]);
    }

    public function destroy($id)
    {
        $entity = $this->getModel()->find($id);

        if (!config('app.develop')) {
            return redirect()->back()->withErrors(trans('widgets::admin.unable_delete'));
        }

        $entity->delete();

        $this->after($entity);

        return redirect()->back()->with('message', trans($this->messages['destroy']));
    }
}
