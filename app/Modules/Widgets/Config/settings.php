<?php
return [
    'title'        => trans('widgets::admin.title'),
    'localization' => true,
    'front_link'   => false,
    'acl'          => true
];
