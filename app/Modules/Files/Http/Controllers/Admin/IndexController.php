<?php

namespace App\Modules\Files\Http\Controllers\Admin;

use App\Modules\Admin\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function images()
    {
        return view('files::admin.images');
    }

    public function files()
    {
        return view('files::admin.files');
    }
}
