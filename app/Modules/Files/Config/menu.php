<?php

return [
    'items' => [
        [
            'icon' => 'fa-photo',
            'group' => 'content',
            'route' => 'admin.files.files',
            'priority' => -100,
            'title' => trans('admin::admin.files')
        ],
        [
            'icon' => 'fa-files-o',
            'group' => 'content',
            'route' => 'admin.files.images',
            'priority' => -100,
            'title' => trans('admin::admin.images')
        ]
    ]

];