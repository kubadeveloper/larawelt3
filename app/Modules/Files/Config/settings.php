<?php

return [
    'title' => trans('files::admin.title'),
    'localization' => false,
    'front_link' => false,
    'acl' => true,
    'acl_scheme' => [
        'permissions' => [
            'view' => ['IndexController@images', 'IndexController@files']
        ]
    ]
];