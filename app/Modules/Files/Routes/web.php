<?php

Route::prefix(config('cms.uri'))->group(function () {
    Route::get('files/images', 'Admin\IndexController@images')->name('admin.files.images');
    Route::get('files/files', 'Admin\IndexController@files')->name('admin.files.files');

    Route::group(['prefix' => 'filemanager', 'middleware' => ['web', 'auth:admin']], function () {
        \UniSharp\LaravelFilemanager\Lfm::routes();
    });
});
