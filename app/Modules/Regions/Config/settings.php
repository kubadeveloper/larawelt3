<?php
return [
    'title'        => trans('regions::index.admin.title'),
    'localization' => true,
    'front_link'   => false,
    'acl'          => true
];