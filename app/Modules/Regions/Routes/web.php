<?php
Route::localizedGroup(function () {
    Route::prefix(config('cms.uri'))
        ->as(config('cms.admin_prefix'))
        ->group(function () {
            Route::resource('regions', 'Admin\IndexController');
            Route::put('regions/priority/{id}/{direction}', 'Admin\IndexController@priority')
                ->name('regions.priority');
        });
});