<?php

namespace App\Modules\Regions\Providers;

use App\Modules\Admin\Providers\BaseModuleServiceProvider;
use App\Modules\Regions\Http\ViewComposers\RegionsComposer;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    public function register() : void
    {
        parent::register();

        $this->app->make('view')->composer('affiliates::admin.form', RegionsComposer::class);
    }
}
