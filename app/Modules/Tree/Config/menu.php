<?php
return [
    'items' => [
        [
            'icon'     => 'fa-tree',
            'priority' => 100,
            'group'    => 'content',
            'route'    => 'admin.tree.index',
            'title'    => trans('tree::admin.title')
        ]
    ]
];
