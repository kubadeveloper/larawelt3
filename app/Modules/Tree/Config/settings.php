<?php
return [
    'title'        => trans('tree::admin.title'),
    'localization' => true,
    'front_link'   => true,
    'acl'          => true,
    'modules'      => [
        'news',
        'articles',
        'gallery',
        'feedback' => ['actions' => [['function' => 'index']]]
    ],
    'templates'    => [
        'inner' => trans('tree::admin.templates.inner'),
    ]
];
