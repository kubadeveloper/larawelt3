<?php

namespace App\Modules\Tree\Providers;

use App\Modules\Admin\Providers\BaseModuleServiceProvider;
use App\Modules\Tree\Helpers\Breadcrumbs;
use App\Modules\Tree\Http\ViewComposers\BreadcrumbsComposer;
use App\Modules\Tree\Http\ViewComposers\MenuComposer;
use App\Modules\Tree\Http\ViewComposers\ModulesComposer;
use App\Modules\Tree\Repositories\TreeRepository;
use App\Modules\Tree\Repositories\TreeRoutes;
use Illuminate\Foundation\Application;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    public function boot() : void
    {
        //
    }

    public function register() : void
    {
        $this->loadTranslationsFrom($this->module->getLangDir(), $this->module->slug());
        $this->loadViewsFrom($this->module->getViewsDir(), $this->module->slug());
        $this->bootConfig();

        parent::register();

        $this->app->bind('tree_repository', function () {
            return new TreeRepository();
        });
        $this->app->bind('tree_routes', function (Application $app) {
            return new TreeRoutes($app['tree_repository']);
        });
        $this->app->bind('breadcrumbs', function () {
            return new Breadcrumbs();
        });

        $this->app->make('view')->composer('tree::menu', MenuComposer::class);
        $this->app->make('view')->composer('tree::breadcrumbs', BreadcrumbsComposer::class);
        $this->app->make('view')->composer(['tree::admin.form', 'tree::admin.index'], ModulesComposer::class);
    }
}
