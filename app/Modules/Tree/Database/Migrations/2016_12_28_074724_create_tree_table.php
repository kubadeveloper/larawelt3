<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreeTable extends Migration
{
    public function up()
    {
        Schema::create('tree', function (Blueprint $table) {
            // These columns are needed for Baum's Nested Set implementation to work.
            // Column names may be changed, but they *must* all exist and be modified
            // in the model.
            // Take a look at the model scaffold comments for details.
            // We add indexes on parent_id, lft, rgt columns by default.
            $table->increments('id');
            $table->integer('parent_id')->nullable()->index();
            $table->integer('left')->nullable()->index();
            $table->integer('right')->nullable()->index();
            $table->integer('depth')->nullable();
            $table->enum('lang', ['ru', 'en', 'ky'])->index();
            $table->string('slug')->default('')->index();
            $table->string('redirect_url')->nullable();
            $table->string('title');
            $table->text('content')->nullable();
            $table->string('module')->nullable();
            $table->string('template')->nullable();
            $table->tinyInteger('active')->nullable()->default(0);
            $table->tinyInteger('redirector')->default(0);
            $table->tinyInteger('in_menu')->nullable()->default(0);

            $table->string('meta_title')->nullable();
            $table->string('meta_h1')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->text('meta_description')->nullable();

            // Add needed columns here (f.ex: name, slug, path, etc.)
            // $table->string('name', 255);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('tree');
    }
}
