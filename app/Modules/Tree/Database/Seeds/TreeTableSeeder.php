<?php

namespace App\Modules\Tree\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TreeTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('tree')->insert([
            [
                'id'         => 1,
                'parent_id'  => null,
                'left'       => 1,
                'right'      => 10,
                'depth'      => 0,
                'lang'       => 'ru',
                'slug'       => '',
                'title'      => 'Главная',
                'module'     => null,
                'template'   => 'index',
                'active'     => 1,
                'created_at' => now()
            ],
            [
                'id'         => 2,
                'parent_id'  => 1,
                'left'       => 2,
                'right'      => 3,
                'depth'      => 1,
                'lang'       => 'ru',
                'slug'       => 'gallery',
                'title'      => 'Галерея',
                'module'     => 'gallery',
                'template'   => 'inner',
                'active'     => 1,
                'created_at' => now()
            ],
            [
                'id'         => 3,
                'parent_id'  => 1,
                'left'       => 4,
                'right'      => 5,
                'depth'      => 1,
                'lang'       => 'ru',
                'slug'       => 'news',
                'title'      => 'Новости',
                'module'     => 'news',
                'template'   => 'inner',
                'active'     => 1,
                'created_at' => now()
            ],
            [
                'id'         => 4,
                'parent_id'  => 1,
                'left'       => 6,
                'right'      => 7,
                'depth'      => 1,
                'lang'       => 'ru',
                'slug'       => 'articles',
                'title'      => 'Статьи',
                'module'     => 'articles',
                'template'   => 'inner',
                'active'     => 1,
                'created_at' => now()
            ],
            [
                'id'         => 5,
                'parent_id'  => 1,
                'left'       => 8,
                'right'      => 9,
                'depth'      => 1,
                'lang'       => 'ru',
                'slug'       => 'feedback',
                'title'      => 'Обратная связь',
                'module'     => 'feedback',
                'template'   => 'inner',
                'active'     => 1,
                'created_at' => now()
            ]
        ]);
    }
}
