<?php

namespace App\Modules\Tree\Http\ViewComposers;

use Illuminate\View\View;

class ModulesComposer
{
    public function compose(View $view)
    {
        $settings = module_config('settings.modules', 'tree');
        $modules  = ['' => ''];

        foreach ($settings as $slug => $setting) {
            if (is_array($setting)) {
                $modules[$slug] = $this->getTitle($slug, $setting);
            } else {
                $modules[$setting] = $this->getTitle($setting);
            }
        }

        $view->with('modulesSelect', $modules);
    }

    private function getTitle(string $slug, $setting = null) : string
    {
        if ($setting !== null) {
            if (is_array($setting) && array_key_exists('trans', $setting)) {
                return trans($setting['trans']);
            }

            if (is_string($setting)) {
                return trans($setting);
            }
        }

        if (strlen($slug)) {
            return trans($slug . '::admin.title');
        }

        return '';
    }
}
