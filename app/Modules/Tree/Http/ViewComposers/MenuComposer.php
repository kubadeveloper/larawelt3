<?php

namespace App\Modules\Tree\Http\ViewComposers;

use App\Modules\Tree\Facades\Tree;
use Illuminate\View\View;

class MenuComposer
{
    public function compose(View $view)
    {
        $view->with('items', Tree::getMainMenu());
    }
}
