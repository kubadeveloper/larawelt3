<?php

namespace App\Modules\Tree\Http\ViewComposers;

use App\Modules\Tree\Facades\Breadcrumbs;
use Illuminate\View\View;

class BreadcrumbsComposer
{
    public function compose(View $view)
    {
        $view->with('breadcrumbs', Breadcrumbs::all());
    }
}
