<?php

namespace App\Modules\Tree\Http\Controllers\Admin;

use App\Modules\Admin\Http\Controllers\Admin;
use App\Modules\Tree\Models\Tree;
use Baum\MoveNotPossibleException;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;

class IndexController extends Admin
{
    public $perPage = 100;

    public function getModel()
    {
        return new Tree();
    }

    public function getRules($request, $id = false)
    {
        return [
            'title' => 'sometimes|required|max:255',
            'slug' => [
                'sometimes',
                'required',
                'regex:/(^[A-Za-z0-9_\-]+$)+/',
                'max:60',
                Rule::unique('tree')->ignore($id)->where('lang', lang())
            ],
            'module' => [
                Rule::unique('tree')->ignore($id)->where('lang', lang()),
                'nullable'
            ],
            'redirect_url' => 'sometimes|required_if:redirector,1|max:255',
            'content' => 'max:65535'
        ];
    }

    protected function after($entity)
    {
        if (method() == 'store' || method() == 'update') {
            $parentId = (int)Request::get('parent_id');
            if ($parentId && $parentId != $entity->parent_id) {
                $parent = $this->getModel()->findOrFail($parentId);
                try {
                    $entity->makeChildOf($parent);
                } catch (MoveNotPossibleException $e) {
                    redirect()->back()->withErrors([trans('tree::admin.unable_to_move')]);
                }
            }
        }
    }

    public function priority($id, $direction)
    {
        $entity = $this->getModel()->findOrFail($id);

        try {
            if ($direction == 'up') {
                $entity->moveLeft();
            } else {
                $entity->moveRight();
            }
        } catch (MoveNotPossibleException $e) {
            redirect()->back();
        }

        $this->after($entity);

        return redirect()->back();
    }

    public function edit($id)
    {
        $frontUrl = null;
        $page     = Tree::where('id', $id)->first();

        if ($page && $page->active && $page->slug !== '') {
            $frontUrl = route($page->slug);
        }

        $entity = $this->getModel()->findOrFail($id);

        $this->after($entity);

        return view($this->getFormViewName(), [
            'entity'      => $entity,
            'routePrefix' => $this->routePrefix,
            'frontUrl'    => $frontUrl
        ]);
    }

    public function create()
    {
        $entity = $this->getModel();

        $this->after($entity);

        if ($this->getModel()->count() !== 0) {
            if (Request::get('parent') === null) {
                return redirect(route(\Route::currentRouteName(), ['parent' => $this->getModel()->first()->id]));
            } else {
                $parent = $this->getModel()->find(Request::get('parent'));
                if ($parent !== null && $parent->module !== null) {
                    return view('admin::access');
                }
            }
        }

        return view($this->getFormViewName(), ['entity' => $entity]);
    }

    public function destroy($id)
    {
        $entity = $this->getModel()->find($id);

        if ($entity->isRoot() || (!config('app.develop') && $entity->module !== null)) {
            return redirect()->back()->withErrors(trans('tree::admin.unable_delete'));
        }

        $entity->delete();
        $this->after($entity);

        return redirect()->back()->with('message', trans($this->messages['destroy']));
    }
}
