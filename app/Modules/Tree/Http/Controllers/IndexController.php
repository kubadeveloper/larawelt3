<?php

namespace App\Modules\Tree\Http\Controllers;

use App\Modules\Tree\Facades\Tree;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('breadcrumbs');
        $this->middleware('og');
        $this->middleware('meta');
    }

    public function index(Request $request)
    {
        $page = $request->get('page') ?: Tree::getRoot();

        // Перенаправление
        if ($page->redirector) {
            return redirect(url($page->redirect_url));
        }

        return view($page->template, ['page' => $page]);
    }
}
