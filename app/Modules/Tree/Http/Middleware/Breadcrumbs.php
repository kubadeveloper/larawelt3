<?php

namespace App\Modules\Tree\Http\Middleware;

use Closure;
use App\Modules\Tree\Facades\Breadcrumbs as Facade;
use Illuminate\Http\Request;

class Breadcrumbs
{
    public function handle(Request $request, Closure $next, $renderEntity = true)
    {
        $page = $request->get('page');
        if ($page) {
            $ancestors = $page->ancestorsAndSelf()->get();
            foreach ($ancestors as $ancestor) {
                $url = $ancestor->slug ? route($ancestor->slug) : home();

                if ($ancestor->meta_h1) {
                    Facade::add($ancestor->meta_h1, $url);
                } else {
                    Facade::add($ancestor->title, $url);
                }
            }
        }

        $entity = $request->get('entity');
        if ($entity && $renderEntity === true) {
            if ($entity->meta_h1) {
                Facade::add($entity->meta_h1, route($page->slug . '.show', ['id' => $entity->id]));
            } else {
                Facade::add($entity->title, route($page->slug . '.show', ['id' => $entity->id]));
            }
        }

        return $next($request);
    }
}
