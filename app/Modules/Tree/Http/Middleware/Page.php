<?php

namespace App\Modules\Tree\Http\Middleware;

use App\Modules\Tree\Facades\Tree;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class Page
{
    public function handle(Request $request, Closure $next)
    {
        $route = $request->route()->getName();
        $page  = null;

        if ($route) {
            $route = explode('.', $route)[0];
            $page  = Tree::get($route);
        }

        //Главная страница
        if (!$page) {
            $page = Tree::getRoot();
        }

        if ($page) {
            $request->attributes->add(['page' => $page]);
            View::share('page', $page);
        }

        return $next($request);
    }
}
