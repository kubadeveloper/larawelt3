<?php

namespace App\Modules\Tree\Helpers;

class Breadcrumbs
{
    private $crumbs = [];

    public function all()
    {
        return $this->crumbs;
    }

    public function add($title, $url)
    {
        $this->resetLast();
        $this->crumbs[] = (object)[
            'title' => $title,
            'url'   => $url,
            'last'  => true
        ];
    }

    private function resetLast()
    {
        foreach ($this->crumbs as &$crumb) {
            $crumb->last = false;
        }
    }
}
