<?php

namespace App\Modules\Tree\Models;

use App\Facades\Route;
use App\Models\Tree as ParentTree;
use Kyslik\ColumnSortable\Sortable;

class Tree extends ParentTree
{
    use Sortable;

    protected $table = 'tree';

    public function setSlugAttribute($value)
    {
        if (config('app.develop') || Route::getMethod() === 'store' || $this->module === null) {
            $this->attributes['slug'] = $value;
        }
    }

    public function setModuleAttribute($value)
    {
        if (config('app.develop') || Route::getMethod() === 'store') {
            $this->attributes['module'] = $value;
        }
    }

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($entity) {
            self::beforeSaving($entity);
        });
    }

    protected static function beforeSaving($entity)
    {
        if ($entity->redirector) {
            $entity->module  = null;
            $entity->content = null;
        } else {
            $entity->redirect_url = null;
        }
    }
}
