@extends('admin::admin.form')

@push('js')
    <script src="{!! asset('/adminlte/js/tree.redirector.js') !!}"></script>
@endpush

@section('topmenu')
    <div class="header-module-controls">
        @include('admin::common.topmenu.list', ['routePrefix' => $routePrefix])
        @include('admin::common.topmenu.create', [
            'url' => route($routePrefix . 'create', ['parent' => $entity->parent_id ?: Request::get('parent')])
        ])
        @include('admin::common.topmenu.page', ['fullPage' => trans('tree::admin.full_page')])
    </div>
@endsection

@section('sidemenu')
    @include('admin::common.sidemenu.all')
@endsection

@section('form_content')
    {!! AdminBootForm::open([
        'model' => $entity,
        'store' => $routePrefix . 'store',
        'update' => $routePrefix . 'update'
    ]) !!}

    <div class="col-md-6">
        {!! AdminBootForm::text('title', trans('admin::fields.title')) !!}

        @if (Request::get('parent') || $entity->parent_id)
            <div class="row">
                <div class="col-md-12">
                    @if(config('app.develop') || $entity->id == null)
                        {!! AdminBootForm::select('module',  trans('tree::admin.module'), $modulesSelect) !!}
                    @else
                        {!! AdminBootForm::text('module', trans('tree::admin.module'), $modulesSelect[$entity->module],
                            ['readonly']) !!}
                        <div class="help-block">@lang('admin::admin.access_field')</div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    {!! AdminBootForm::select('parent_id',  trans('tree::admin.parent'), Tree::getSelect(),
                        Request::get('parent')) !!}
                </div>
            </div>
        @endif
    </div>

    <div class="col-md-6">
        @if (Request::get('parent') || $entity->parent_id)
            <div class="row">
                <div class="col-md-12">
                    @if(config('app.develop') || $entity->module == null || $entity->id == null)
                        {!! AdminBootForm::text('slug', trans('tree::admin.slug')) !!}
                    @else
                        {!! AdminBootForm::text('slug', trans('tree::admin.slug'), null, ['readonly']) !!}
                        <div class="help-block">@lang('admin::admin.access_field')</div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-7">
                    {!! AdminBootForm::select('template', trans('tree::admin.template'),
                        module_config('settings.templates')) !!}
                </div>

                <div class="col-md-5">
                    @if($entity->id == null || $entity->module == null)
                        <input name="redirector" type="hidden" value="0">
                        {!! AdminBootForm::checkbox('redirector', trans('tree::admin.redirector'), 1) !!}
                    @else
                        {!! AdminBootForm::hidden('redirector', $entity->redirector) !!}
                        {!! AdminBootForm::checkbox('redirector', trans('tree::admin.redirector'), 1, null,
                            ['disabled']) !!}
                        <div class="help-block">@lang('admin::admin.access_field')</div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    {!! AdminBootForm::hidden('active', 0) !!}
                    {!! AdminBootForm::checkbox('active', trans('admin::fields.active'), 1) !!}
                </div>

                <div class="col-md-6">
                    {!! AdminBootForm::hidden('in_menu', 0) !!}
                    {!! AdminBootForm::checkbox('in_menu', trans('tree::admin.in_menu'), 1) !!}
                </div>
            </div>
        @else
            <div class="row">
                @if (Request::get('parent') || $entity->parent_id)
                    <div class="col-md-8">
                        {!! AdminBootForm::select('template',  trans('tree::admin.template'),
                            module_config('settings.templates')) !!}
                    </div>
                @endif

                <div class="col-md-4">
                    {!! AdminBootForm::hidden('active', 0) !!}
                    {!! AdminBootForm::checkbox('active', trans('admin::fields.active'), 1) !!}
                </div>
            </div>

            <input name="template" type="hidden" value="index">
        @endif
    </div>

    @if((Request::get('parent') !== null || $entity->parent) && (!$entity->id || !$entity->isRoot()))
        <div class="raw" id="redirect_animation">
            <div class="col-md-12">
                {!! AdminBootForm::text('redirect_url', trans('admin::fields.link')) !!}
            </div>
        </div>
    @endif

    <div class="raw" id="content_animation">
        <div class="col-md-12">
            {!! AdminBootForm::textarea('content', trans('admin::fields.content')) !!}
        </div>
    </div>

    @include('admin::common.forms.seo')
@endsection
