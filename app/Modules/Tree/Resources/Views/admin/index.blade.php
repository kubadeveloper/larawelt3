@extends('admin::admin.index')

@section('topmenu')
    <div class="header-module-controls">
        @include('admin::common.topmenu.list', ['routePrefix' => $routePrefix])
        @isset($entities)
            @include('admin::common.topmenu.create', [
                'url' => route($routePrefix . 'create', ['parent' => @$entities[0]->id])
            ])
        @endisset
    </div>
@endsection

@section('content')
    @include('admin::common.errors')

    @if (count($entities) > 0)
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>@lang('tree::admin.title_short')</th>
                    <th>@lang('tree::admin.slug_short')</th>
                    <th>@lang('tree::admin.module')</th>
                    <th>@lang('admin::fields.priority')</th>
                    <th>@lang('admin::admin.control')</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($entities as $entity)
                    <tr @if (!$entity->active) class="unpublished" @endif>
                        <td>
                            {!! str_repeat('<span class="fa padding"></span>', $entity->depth) !!}
                            @if($entity->isRoot())
                                <span class="fa fa-cog"></span>
                            @elseif($entity->isLeaf())
                                <span class="fa fa-sticky-note-o"></span>
                            @else
                                <span class="fa fa-folder"></span>
                            @endif
                            {{ $entity->title }}
                        </td>
                        <td>{{ $entity->slug }}</td>
                        <td>{{ $modulesSelect[$entity->module] }}</td>
                        <td class="priority">
                            @if(!$entity->isRoot())
                                @include ('admin::common.controls.priority', [
                                    'routePrefix' => $routePrefix,
                                    'entity' => $entity
                                ])
                            @endif
                        </td>
                        <td class="controls">
                            @include('admin::common.controls.plus', [
                                'routePrefix' => $routePrefix,
                                'id' => $entity->id,
                                'disabled' => $entity->module
                            ])
                            @include('admin::common.controls.publish', [
                                'routePrefix' => $routePrefix,
                                'id' => $entity->id
                            ])
                            @include('admin::common.controls.edit', [
                                'routePrefix' => $routePrefix,
                                'id' => $entity->id
                            ])
                            @include('admin::common.controls.destroy', [
                                'routePrefix' => $routePrefix,
                                'id' => $entity->id,
                                'disabled' => $entity->isRoot() || (!config('app.develop') && $entity->module !== null)
                            ])
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {!! $entities->appends(\Request::except('page'))->render() !!}
    @else
        <p>
            <a href="{!! route($routePrefix . 'create') !!}" class="btn btn-primary icon-plus icon-white">
                <span>@lang('tree::admin.create_root')</span>
            </a>
        </p>
    @endif
@endsection
