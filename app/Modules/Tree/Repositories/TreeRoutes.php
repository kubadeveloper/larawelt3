<?php

namespace App\Modules\Tree\Repositories;

use stdClass;

class TreeRoutes
{
    /**
     * Репозиторий дерева сайта.
     *
     * @var TreeRepository
     */
    protected $tree;

    /**
     * Создание экземпляра класса.
     *
     * @param TreeRepository $tree
     * @return void
     */
    public function __construct(TreeRepository $tree)
    {
        $this->tree = $tree;
    }

    /**
     * Получение списка роутов.
     *
     * @return array
     */
    public function get() : array
    {
        $pages  = $this->tree->getActive();
        $routes = [];
        $url    = [];

        foreach ($pages as $index => $page) {
            $pageUrl = $this->getUrl($pages, $index, $url);

            if ($page->module) {
                $actions = $this->getActions($page->module);

                foreach ($actions as $action) {
                    if (!count($action)) continue;

                    $directory  = $this->getDirectory($page->module, $action);
                    $controller = $this->getController($page->module, $action);
                    $routes[]   = $this->getRouteData(
                        $pageUrl . ($action['url'] ?? ''),
                        $page->slug . ($action['name'] ?? ''),
                        $this->getAction($directory, $controller, $action['function'])
                    );
                }
            } else {
                $routes[] = $this->getRouteData($pageUrl, $page->slug, $this->getAction());
            }
        }

        return $routes;
    }

    /**
     * Получение списка роутов.
     *
     * @return array
     */
    public function getFromAllLocales() : array
    {
        $pages = collect([]);

        foreach (config('localization.locales') as $key => $value) {
            $pages->push($this->tree->getActiveFromLocale($key));
        }

        $pages = $pages->flatten();

        $routes = [];
        $url    = [];

        foreach ($pages as $index => $page) {
            $pageUrl = $this->getUrl($pages, $index, $url);

            if ($page->module) {
                $actions = $this->getActions($page->module);

                foreach ($actions as $action) {
                    if (!count($action)) continue;

                    $directory  = $this->getDirectory($page->module, $action);
                    $controller = $this->getController($page->module, $action);

                    $routeData =  $this->getRouteData(
                        $pageUrl . ($action['url'] ?? ''),
                        $page->slug . ($action['name'] ?? ''),
                        $this->getAction($directory, $controller, $action['function'])
                    );

                    $routeData = $this->getRouteData($pageUrl, $page->slug, $this->getAction());
                    $routeData->lang = $page->lang;
                    $routes[] = $routeData;
                }
            } else {
                $routeData = $this->getRouteData($pageUrl, $page->slug, $this->getAction());
                $routeData->lang = $page->lang;
                $routes[] = $routeData;
            }
        }

        return $routes;
    }

    /**
     * Получение url страницы.
     *
     * @param $items
     * @param int $index
     * @param array $url
     * @return string
     */
    private function getUrl($items, int $index, array &$url) : string
    {
        $current = $items[$index];

        if ($index > 0) {
            $previous = $items[$index - 1];

            if ($current->depth > $previous->depth) {
                $url[] = $previous->slug;
            }

            if ($current->depth < $previous->depth) {
                $diff = $current->depth - $previous->depth;
                $url  = array_slice($url, 0, $diff);
            }
        }

        return ltrim(implode('/', $url) . '/' . $current->slug, '/');
    }

    /**
     * Получение данных роута.
     *
     * @param string $url
     * @param string $name
     * @param string $action
     * @return stdClass
     */
    private function getRouteData(string $url, string $name, string $action) : stdClass
    {
        return (object)['url' => $url, 'name' => $name, 'action' => $action];
    }

    /**
     * Получение функции обработчика.
     *
     * @param string $directory
     * @param string $controller
     * @param string $function
     * @return string
     */
    private function getAction(string $directory = 'Tree', string $controller = 'IndexController',
                               string $function = 'index') : string
    {
        return '\App\Modules\\' . $directory . '\Http\Controllers\\' . $controller . '@' . $function;
    }

    /**
     * Получение директории модуля.
     *
     * @param string $module
     * @param array $action
     * @return string
     */
    private function getDirectory(string $module, array $action) : string
    {
        if (isset($action['directory'])) {
            return $action['directory'];
        }

        return $this->getConfigField('directory', $module) ?: ucfirst($module);
    }

    /**
     * Получение контроллера модуля.
     *
     * @param string $module
     * @param array $action
     * @return string
     */
    private function getController(string $module, array $action) : string
    {
        if (isset($action['controller'])) {
            return $action['controller'];
        }

        return $this->getConfigField('controller', $module) ?: 'IndexController';
    }

    /**
     * Получение данных экшенов модуля.
     *
     * @param string $module
     * @return array
     */
    private function getActions(string $module) : array
    {
        $actions = $this->getConfigField('actions', $module);

        return is_array($actions) ? $actions : $this->getDefaultActions();
    }

    /**
     * Получение стандартных экшенов модуля.
     *
     * @return array
     */
    private function getDefaultActions() : array
    {
        return [['function' => 'index'], ['function' => 'show', 'url' => '/{id}', 'name' => '.show']];
    }

    /**
     * Получение поля из конфига модуля.
     *
     * @param string $field
     * @param string $module
     * @return mixed|string|array|null
     */
    private function getConfigField(string $field, string $module)
    {
        $setting = $this->getModuleSetting($module);

        return isset($field, $setting[$field]) ? $setting[$field] : null;
    }

    /**
     * Получение конфига модуля.
     *
     * @param string $module
     * @return array
     */
    private function getModuleSetting(string $module) : array
    {
        $settings = $this->getModulesSetting();

        return isset($settings[$module]) && is_array($settings[$module]) ? $settings[$module] : [];
    }

    /**
     * Получение списка модулей из конфига.
     *
     * @return array
     */
    private function getModulesSetting() : array
    {
        return module_config('settings.modules', 'tree');
    }
}
