<?php

namespace App\Modules\Tree\Repositories;

use App\Modules\Tree\Models\Tree;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as Collect;
use Illuminate\Support\Facades\Route;

class TreeRepository
{
    /**
     * Список записей.
     *
     * @var \Illuminate\Support\Collection
     */
    private $entities;

    /**
     * Создание экземпляра класса.
     *
     * @return void
     */
    public function __construct()
    {
        $this->entities = collect();
    }

    /**
     * Получение записи по слагу.
     *
     * @param string $slug
     * @return \App\Modules\Tree\Models\Tree
     */
    public function get(string $slug)
    {
        $entity = $this->entities()->where('slug', $slug)->first();

        if ($entity === null) {
            $entity = $this->getModel()->where('slug', $slug)->first();
        }

        if ($entity !== null) {
            $this->setEntity($entity);
        }

        return $entity;
    }

    /**
     * Получение записи по модулю.
     *
     * @param string $module
     * @param string $locale
     * @return \App\Modules\Tree\Models\Tree
     */
    public function getByModule(string $module, string $locale = '')
    {
        $entity = $this->entities($locale)->where('module', $module)->first();

        if ($entity === null) {
            $query = $this->getModel();

            if ($locale && isset($query->getGlobalScopes()['lang'])) {
                $query = $query->newQueryWithoutScope('lang')->where('lang', $locale);
            }

            $entity = $query->active()->where('module', $module)->first();
        }

        if ($entity !== null) {
            $this->setEntity($entity);
        }

        return $entity;
    }

    /**
     * Получение корневого узла.
     *
     * @return \App\Modules\Tree\Models\Tree
     */
    public function getRoot()
    {
        $entity = $this->entities()->where('parent_id', null)->first();

        if ($entity === null) {
            $entity = $this->getModel()->whereNull('parent_id')->first();
        }

        if ($entity !== null) {
            $this->setEntity($entity);
        }

        return $entity;
    }

    /**
     * Получение активных записей.
     *
     * @return \App\Modules\Tree\Models\Tree[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getActive() : Collection
    {
        return $this->getModel()->items()->get();
    }

    /**
     * Получение активных записей со всех локалей
     *
     * @return \App\Modules\Tree\Models\Tree[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getActiveFromLocale($lang) : Collection
    {
        return $this->getModel()->newQueryWithoutScopes()->where('lang', $lang)->active()->get();
    }

    /**
     * Получение записей для главного меню.
     *
     * @return \App\Modules\Tree\Models\Tree[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getMainMenu() : Collection
    {
        return $this->getModel()->items()->where('depth', 1)->where('in_menu', 1)->get();
    }

    /**
     * Получение списка страниц.
     *
     * @return array
     */
    public function getSelect() : array
    {
        $pages = $this->getModel()->admin()->whereNull('module')->get();
        $keyed = [];

        foreach ($pages as $page) {
            $keyed[$page->id] = str_repeat('- ', $page->depth) . $page->title;
        }

        return $keyed;
    }

    /**
     * Получение ссылки на страницу.
     *
     * @param $id
     * @return string
     */
    public function getFrontUrl($id) : string
    {
        $url = '';

        if (module() && module_config('settings.front_link')) {
            $page = $this->getByModule(module());
            if ($page && Route::has((string)$page->slug . '.show')) {
                $url = route($page->slug . '.show', $id);
            }
        }

        return $url;
    }

    /**
     * Запись записи в коллекцию.
     *
     * @param \App\Modules\Tree\Models\Tree $entity
     * @return void
     */
    private function setEntity(Tree $entity) : void
    {
        if (!$this->entities->has($entity->id)) {
            $this->entities->put($entity->id, $entity);
        }
    }

    /**
     * Получение списка записей.
     *
     * @param string $locale
     * @return \Illuminate\Support\Collection
     */
    private function entities(string $locale = '') : Collect
    {
        return $this->entities->where('lang', $locale ?: lang());
    }

    /**
     * Получение объекта модели.
     *
     * @return \App\Modules\Tree\Models\Tree
     */
    private function getModel() : Tree
    {
        return new Tree();
    }
}
