<?php

Route::localizedGroup(function () {
    Route::prefix(config('cms.uri'))->as(config('cms.admin_prefix'))->group(function () {
        Route::resource('tree', 'Admin\IndexController');
        Route::put('tree/priority/{id}/{direction}', 'Admin\IndexController@priority')
            ->name('tree.priority');
    });
});
