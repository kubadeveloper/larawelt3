<?php

namespace App\Modules\Tree\Facades;

use App\Modules\Search\Search as BaseSearch;
use Illuminate\Support\Facades\Route;

class Search extends BaseSearch
{
    protected $searchFields = ['title', 'content', 'meta_h1', 'meta_title', 'meta_keywords', 'meta_description'];

    protected function getUrl($entity) : string
    {
        if (!$entity->slug || !Route::has($entity->slug)) {
            return '';
        }

        return route($entity->slug);
    }

    protected function getModuleTitle() : string
    {
        return trans('tree::index.search_title');
    }

    protected function hasTreePage() : bool
    {
        return true;
    }
}
