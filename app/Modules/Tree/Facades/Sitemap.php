<?php

namespace App\Modules\Tree\Facades;

use App\Modules\Tree\Models\Tree;
use App\Modules\Sitemap\Sitemap as BaseSitemap;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Tree\Repositories\TreeRoutes;

class Sitemap extends BaseSitemap
{
    private $activeField = 'active';

    private $treeUrls;

    protected function getModel(): Model
    {
        return new Tree();
    }

    protected function getUrl($entity) : string
    {
        if ($entity->slug == '') {
            return false;
        }

        $searchedItem = $this->treeUrls->filter(function($item) use ($entity) {
            return $item->name == $entity->slug && $item->lang == $entity->lang;
        })->first();

        if (!$searchedItem) {
            return false;
        }

        $url = '';

        if ($searchedItem->lang != config('app.locale')) {
            $url .= $searchedItem->lang . '/';
        }

        $url .= $searchedItem->url;

        return host() . '/' .$url;
    }

    public function getLocs(int $limit, int $offset) : array
    {
        $res = '';
        $result = [];
        $attributes = $this->getModel()->first()->getAttributes();

        if (is_array($attributes)) {
            $res = $this->getModel()->newQueryWithoutScopes();

            if (array_key_exists($this->activeField, $attributes)) {
                $res = $res->where($this->activeField, 1);
            }

            $res = $res->limit($limit)
                ->offset($offset)
                ->orderByRaw('FIELD(lang, ' . $this->getLocalesPriority() .')')
                ->get();
        }

        $this->setTreeUrls();

        if ($res) {
            foreach ($res as $ent) {
                $result[] = $this->prepareLoc($ent);
            }
        }

        usort($result, function($a, $b) {
            if ($a['priority'] == $b['priority']) {
                return 0;
            }
            return ($a['priority'] > $b['priority']) ? -1 : 1;
        });

        return $result;
    }

    protected function prepareLoc($entity) : array
    {
        $loc = array_merge(['loc' => $this->getUrl($entity)], $this->defaultParams);

        if ($entity->parent_id == null) {
            if ($entity->lang == config('app.locale')) {
                $loc['loc'] = host();
                $loc['priority'] = '1';
            } else {
                $loc['loc'] = host() . '/' . $entity->lang;
                $loc['priority'] = '0.9';
            }
        }

        if (isset($entity->updated_at) && $entity->updated_at) {
            $loc['lastmod'] = $this->getLastMod($entity->updated_at);
        }

        return $loc;
    }

    public function setTreeUrls()
    {
        $this->treeUrls = collect(app()->tree_routes->getFromAllLocales());
    }
}
