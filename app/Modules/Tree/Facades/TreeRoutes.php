<?php

namespace App\Modules\Tree\Facades;

use Illuminate\Support\Facades\Facade;

class TreeRoutes extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'tree_routes';
    }
}
