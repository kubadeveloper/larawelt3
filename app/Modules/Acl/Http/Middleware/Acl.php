<?php

namespace App\Modules\Acl\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;

class Acl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($route = Route::currentRouteName()) {
            if (!is_route_allowed($route)) {
                abort(401, $route);
            }
        }

        return $next($request);
    }
}
