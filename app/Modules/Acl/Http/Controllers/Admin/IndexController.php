<?php

namespace App\Modules\Acl\Http\Controllers\Admin;

use App\Modules\Admin\Http\Controllers\Admin;
use App\Modules\Acl\Models\Acl;

class IndexController extends Admin
{
    /**
     * Возвращает модель. Функция используется в родительском контроллере
     *
     * @return Acl
     */
    public function getModel()
    {
        return new Acl;
    }

    public function getRules($request, $id = false)
    {
        return [
            'title' => 'sometimes|required|max:255'
        ];
    }
}