<?php

namespace App\Modules\Acl\Providers;

use App\Modules\Acl\Permissions;
use App\Modules\Acl\Helper;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;

class AclServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('PermissionsService', function () {
            return new Permissions(Auth::user());
        });
        $this->app->bind('HelperService', function () {
            return new Helper();
        });
    }
}
