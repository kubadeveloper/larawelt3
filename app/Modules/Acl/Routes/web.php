<?php

Route::prefix(config('cms.uri'))
    ->name(config('cms.admin_prefix'))
    ->as(config('cms.admin_prefix'))
    ->group(function () {
        Route::resource('acl', 'Admin\IndexController');
    });
