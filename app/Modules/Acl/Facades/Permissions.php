<?php

namespace App\Modules\Acl\Facades;

use Illuminate\Support\Facades\Facade;
use RuntimeException;

class Permissions extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'PermissionsService';
    }

    /**
     * Хотим, чтобы методы подсистемы были вызваны через магический метод
     *
     * @param string $method
     * @param array $args
     * @return bool|mixed
     *
     */
    public static function __callStatic($method, $args)
    {
        $instance = static::getFacadeRoot();
        if (!$instance) {
            throw new RuntimeException('A facade root has not been set.');
        }

        return $instance->__call($method, $args);
    }
}
