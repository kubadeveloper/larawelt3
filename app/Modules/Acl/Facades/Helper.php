<?php

namespace App\Modules\Acl\Facades;

use Illuminate\Support\Facades\Facade;

class Helper extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'HelperService';
    }
}
