<?php

return [
    'title' => trans('acl::admin.title'),
    'localization' => false,
    'front_link' => false,
    'acl' => true,
];