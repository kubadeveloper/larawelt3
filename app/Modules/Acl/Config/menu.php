<?php

return[
    'items' => [
        [
            'icon'  => 'fa-users',
            'group' =>'admins',
            'route' => 'admin.acl.index',
            'title' => trans('acl::admin.title'),
            'slug'  => 'acl'
        ]
    ]
];