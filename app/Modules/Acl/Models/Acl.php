<?php

namespace App\Modules\Acl\Models;

use App\Models\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Notifications\Notifiable;

class Acl extends Model
{
    use Notifiable,
        Sortable;

    protected $table = 'acl';

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($model) {
            with(new static)->beforeDelete($model);
        });
    }

    public function scopeOrder($query)
    {
        return $query;
    }

    public function getPermissionsAttribute()
    {
        if (!isset($this->attributes['permissions'])) {
            return null;
        }

        return unserialize($this->attributes['permissions']);
    }

    public function setPermissionsAttribute($value)
    {
        if ($this->root) {
            $this->attributes['permissions'] = null;
        } else {
            $this->attributes['permissions'] = serialize($value);
        }
    }

    protected function beforeSave($model)
    {
        if ($model->root) {
            abort(401, 'Not allowed');
        }
    }

    public function admins()
    {
        return $this->hasMany("App\Modules\Admins\Models\Admin", 'group_id', 'id');
    }

    public function beforeDelete($model)
    {
        foreach ($model->admins as $admin) {
            $admin->update(['group_id' => null]);
        }
    }
}
