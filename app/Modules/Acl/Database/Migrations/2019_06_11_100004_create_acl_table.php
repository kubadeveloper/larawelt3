<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAclTable extends Migration
{
    public function up()
    {
        Schema::create('acl', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->tinyInteger('root')->default(0);
            $table->text('permissions')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('acl');
    }
}
