<?php

namespace App\Modules\Acl\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AclTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('acl')->insert(
            [
                'id'         => 1,
                'title'      => 'Администратор',
                'root'       => 1,
                'created_at' => now()
            ]
        );
    }
}
