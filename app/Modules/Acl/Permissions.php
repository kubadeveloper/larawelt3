<?php

namespace App\Modules\Acl;

use App\Facades\Route as RouteFacade;
use App\Modules\Admins\Models\Admin;
use App\Modules\Acl\Models\Acl;
use App\Modules\Acl\Facades\Helper;
use Exception;
use Illuminate\Routing\Route;

class Permissions
{
    /**
     * @var Admin
     */
    private $user;

    /**
     * @param Admin $admin
     */
    public function __construct(Admin $admin)
    {
        $this->user = $admin;
    }

    /**
     * @return Acl
     *
     * @throws Exception
     */
    private function getAcl()
    {
        if (!$this->user->acl instanceof Acl) {
            throw new Exception('Bad permissions');
        }

        return $this->user->acl;
    }

    /**
     * @return null|array
     *
     * @throws Exception
     */
    private function getPermissions()
    {
        return $this->getAcl()->permissions;
    }

    /**
     * @return bool
     *
     * @throws Exception
     */
    public function isRoot()
    {
        return (bool)$this->getAcl()->root;
    }

    /**
     * @param string $module
     * @return bool
     *
     * @throws Exception
     */
    public function isModuleIsAllowed($module)
    {
        if (!$module) {
            $module = module();
        }
        if (!is_array($this->getPermissions())) {
            return false;
        }
        if (!array_key_exists($module, $this->getPermissions())) {
            return false;
        }

        return true;
    }

    /**
     * @param string $permission
     * @param string $module
     * @param Admin|Permissions|null $admin
     * @return boolean;
     *
     * @throws Exception
     */
    public function isPermissionAllowed($permission, $module)
    {
        if (!$this->isModuleIsAllowed($module)) {
            return false;
        }
        if (!array_key_exists($permission, $this->getPermissions()[$module])) {
            return false;
        }
        if (!$this->getPermissions()[$module][$permission]) {
            return false;
        }

        return true;
    }

    /**
     * @param string $action
     * @param static $module
     * @return bool
     *
     * @throws Exception
     */
    public function isActionAllowed($action, $module)
    {

        if ($this->isAlwaysAllow($action, $module)) {
            return true;
        }

        if (!Helper::inAcl($module)) {
            return true;
        }

        if (!$this->isModuleIsAllowed($module)) {
            return false;
        }
        $permission = Helper::getPermissionByAction($action, $module);
        if (!$permission) {
            return false;
        }

        return $this->isPermissionAllowed($permission, $module);
    }

    /**
     * @param $action
     * @param $module
     * @return bool
     */
    public function isAlwaysAllow($action, $module)
    {
        if (!$config = Helper::getAlwaysAllows()) {
            return false;
        }

        if (!isset($config[$module])) {
            return false;
        }

        return in_array($action, $config[$module]);
    }

    /**
     * @param string $route
     * @param null $admin
     * @return bool
     *
     * @throws Exception
     */
    public function isRouteAllowed(Route $route)
    {
        $action = RouteFacade::getAction($route);
        $module = RouteFacade::getModule($route);

        return $this->isActionAllowed($action, $module);
    }

    /**
     * Если метод проверяет права, то в случае если пользователь относится к рутовой группе сразу возвращаем true
     *
     * @param string $method
     * @param string $arguments
     * @return bool|mixed
     *
     * @throws Exception
     */
    public function __call($method, $arguments)
    {
        if (method_exists($this, $method) && strstr($method, 'Allowed')) {
            if ($this->isRoot()) {
                return true;
            }

            return call_user_func_array([$this, $method], $arguments);
        }
    }
}