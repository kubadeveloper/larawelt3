@extends('admin::admin.form')

@section('form_content')
    {!!
        AdminBootForm::open([
            'model'  => $entity,
            'store'  => $routePrefix.'store',
            'update' => $routePrefix.'update',
            'autocomplete' => 'off'
        ])
    !!}

    {{--Пример текстового поля--}}
    <div class="col-md-12">
        {!! AdminBootForm::text('title', trans('admin::fields.title')) !!}
    </div>
    <div class="col-md-12">
        <table class="table">
            <thead>
            <th>{{ trans('acl::admin.module')  }} </th>
            @foreach(Acl::getPermissions() as $permission)
                <th width="120" class="text-center">{{ trans('acl::admin.permissions.'.$permission) }}</th>
            @endforeach
            <!--<th width="120">{{ trans('acl::admin.own_only')  }}</th>-->
            </thead>
            <tbody
            @foreach(Acl::getSchemes() as $slug => $module)
                <tr>
                    <td>{{ $module['title'] }}</td>

                    @foreach(Acl::getPermissions() as $permission)
                        <td class="text-center">
                            @if (array_key_exists($permission, $module['acl_scheme']['permissions']))
                                <input
                                        type="hidden"
                                        value="0"
                                        name="permissions[{{ $slug }}][{{$permission}}]"
                                />
                                <input
                                        type="checkbox"
                                        name="permissions[{{ $slug }}][{{$permission}}]"
                                        value="1"
                                        {{ ($entity->permissions != null &&
                                        isset ($entity->permissions[$slug][$permission]) &&
                                        $entity->permissions[$slug][$permission]) ?
                                        'checked' : '' }}
                                />
                            @endif
                        </td>
                    @endforeach
                    <!--<td>
                        @if (array_key_exists('own', $module['acl_scheme']))
                            <input
                                    type="hidden"
                                    value="0"
                                    name="permissions[{{ $slug }}][own]"
                            />

                            <input
                                    type="checkbox"
                                    name="permissions[{{ $slug }}][own]"
                                    value="1"
                                    {{ ($entity->permissions != null &&
                                    isset($entity->permissions[$slug]['own']) &&
                                    $entity->permissions[$slug]['own']) ?
                                    'checked' : '' }}
                            />
                        @endif
                    </td>-->
                </tr>
                @endforeach
                </tbody>
        </table>
    </div>
@endsection