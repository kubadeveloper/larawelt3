@extends('admin::admin.index')

@section('th')
    <th width="350">@sortablelink('title', trans('acl::admin.fields.title'))</th>
    <th width="100">@sortablelink('title', trans('acl::admin.fields.root'))</th>
    <th>@lang('admin::admin.control')</th>
@endsection

@section('td')
    @foreach ($entities as $entity)
        <tr>
            <td>{{ $entity->title }}</td>
            <td>{{ $entity->root }}</td>
            <td class="controls">
                @if (!in_array($entity->id, config('acl.root_group_ids')))
                @include('admin::common.controls.edit', ['routePrefix' => $routePrefix, 'id' => $entity->id,])
                @include('admin::common.controls.destroy', ['routePrefix' => $routePrefix, 'id' => $entity->id,])
                @endif
            </td>
        </tr>
    @endforeach
@endsection