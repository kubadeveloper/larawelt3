<?php

return [
    'title' => 'Группы и права',
    'more' => 'Acl',
    'permissions' => [
        'view' => 'Просмотр',
        'add' => 'Добавление',
        'edit' => 'Редактирование',
        'delete' => 'Удаление'
    ],
    'own_only' => 'Только свои',
    'module' => 'Наименование модуля',
    'fields' => [
        'title' => 'Название',
        'root' => 'Суперадминистратор',
        'permissions' => 'Права'
    ]

];