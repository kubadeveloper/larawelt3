<?php

namespace App\Modules\Acl;

use KubaDev\Modules\Facades\Module;
use App\Modules\Acl\Models\Acl;
use Illuminate\Database\Eloquent\Collection;

class Helper
{
    const VIEW   = 'view';

    const ADD    = 'add';

    const EDIT   = 'edit';

    const DELETE = 'delete';

    const OWN    = 'own';

    /**
     * @return array
     */
    public function getSchemes()
    {
        $schemes = [];
        foreach (Module::all() as $moduleTitle => $moduleInfo) {
            $moduleScheme = $this->getModuleScheme($moduleInfo['slug']);
            if ($moduleScheme) {
                $schemes[$moduleInfo['slug']] = $moduleScheme;
            }
        }

        return $schemes;
    }

    /**
     * @param $module
     *
     * @return array|bool
     */
    public function getModuleScheme($module)
    {
        if (!$config = module_config('settings', strtolower($module))) {
            return false;
        }

        if (isset($config['acl']) && $config['acl'] === true) {
            return [
                'title' => $config['title'],
                'acl_scheme' => isset($config['acl_scheme']) ? $config['acl_scheme'] : config('acl.acl_scheme')
            ];
        }
    }

    /**
     * @param $module
     *
     * @return bool
     */
    public function inAcl($module)
    {
        if (!$config = module_config('settings', strtolower($module))) {
            return false;
        }

        return (isset($config['acl']) && $config['acl'] === true);
    }

    public function getAlwaysAllows()
    {
        if (!$config = config('acl.always_allow')) {
            return false;
        }

        return $config;
    }

    /**
     * @param string $action
     * @param string $module
     *
     * @return bool|string
     */
    public function getPermissionByAction($action, $module)
    {
        $scheme = $this->getModuleScheme($module);
        foreach ($scheme['acl_scheme']['permissions'] as $permission => $actions) {
            if (in_array($action, $actions)) {
                return $permission;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getPermissions()
    {
        return [self::VIEW, self::ADD, self::EDIT, self::DELETE];
    }

    /**
     * @return string
     */
    public function view()
    {
        return self::VIEW;
    }

    /**
     * @return string
     */
    public function edit()
    {
        return self::EDIT;
    }

    /**
     * @return string
     */
    public function add()
    {
        return self::ADD;
    }

    /**
     * @return string
     */
    public function delete()
    {
        return self::DELETE;
    }

    /**
     * @return Collection
     */
    public function getGroupsSelect()
    {
        return Acl::get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['title']];
        });
    }
}
