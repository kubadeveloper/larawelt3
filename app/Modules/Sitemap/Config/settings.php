<?php
return [
    'localization' => true,
    'limit'        => 250,
    'locales_priority' => ['ru', 'en', 'ky'],
];
