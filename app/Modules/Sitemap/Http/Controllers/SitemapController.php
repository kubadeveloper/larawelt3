<?php

namespace App\Modules\Sitemap\Http\Controllers;

use KubaDev\Modules\Repositories\LocalRepository;
use Illuminate\Routing\Controller;

class SitemapController extends Controller
{
    protected $modules;

    protected $limit;

    private $treeModuleName = 'Tree';

    public function __construct()
    {
        $this->modules = app('modules');
        $this->limit = module_config('settings.limit', 'sitemap');
    }

    public function robots()
    {
        $view = 'sitemap::robots.disallow';
        $data = [];

        if (config('cms.indexation')) {
            $view = 'sitemap::robots.robots';
            $data['content'] = implode('', file(public_path('/robots/robots.txt')));
        }

        return response()->view($view, $data)->header('Content-Type', 'text/plain');
    }

    public function index()
    {
        $limit = module_config('settings.limit');
        $modules = $this->modules->all();
        $items = [];
        $result = null;

        // Вставляем Дерево сайта в начало Сайтмапа
        if (isset($modules[$this->treeModuleName])) {
            $treeModule = $modules->pull($this->treeModuleName);
            $modules->prepend($treeModule, $this->treeModuleName);
        }

        foreach ($modules as $module) {
            $searchedName = '\\App\\Modules\\' . $module['basename'] . '\\Facades\\Sitemap';
            if (!class_exists($searchedName)) {
                continue;
            }
            $moduleSitemapFacade = new $searchedName($module['basename']);
            $items[] = $moduleSitemapFacade->getLocs($limit, 0);
        }

        if (empty($items)) {
            return false;
        }

        $items = collect($items)->flatten(1)->toArray();

        return response()->view('sitemap::xml.index', ['params' => $items])->header('Content-Type', 'text/xml');
    }
}
