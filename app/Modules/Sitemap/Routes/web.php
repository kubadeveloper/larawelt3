<?php

Route::get('robots.txt', 'SitemapController@robots');
Route::get('sitemap.xml', 'SitemapController@index');
