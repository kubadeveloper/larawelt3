<?php
Route::prefix(config('cms.uri'))
    ->as(config('cms.admin_prefix'))
    ->group(function () {
        Route::resource('admins', 'Admin\IndexController');
        Route::get('profile/edit/{id}', 'Admin\ProfileController@edit')
            ->name('profile.edit');
        Route::put('profile/update/{id}', 'Admin\ProfileController@update')
            ->name('profile.update');
    });
