<?php
return [
    'title'        => trans('admins::admin.title'),
    'localization' => false,
    'acl'          => true,
    'acl_scheme' => [
        'permissions' => [
            'view' => ['IndexController@index', 'IndexController@edit'],
            'add' => ['IndexController@create', 'IndexController@store'],
            'edit' => ['IndexController@update'],
            'delete' => ['IndexController@destroy']
        ]
    ]
];