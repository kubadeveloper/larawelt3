<?php
return [
    'groups' => [
        [
            'title'    => trans('admins::admin.title'),
            'slug'     => 'admins',
            'icon'     => 'fa-users',
            'priority' => 100
        ]
    ],
    'items' => [
        [
            'icon'  => 'fa-user',
            'group' => 'admins',
            'route' => 'admin.admins.index',
            'title' => trans('admins::admin.title_list')
        ],
    ]
];
