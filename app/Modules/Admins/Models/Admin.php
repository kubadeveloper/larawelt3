<?php

namespace App\Modules\Admins\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;

class Admin extends Authenticatable
{
    use Notifiable, Sortable;

    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'group_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password = null)
    {
        if ($password) {
            $this->attributes['password'] = bcrypt($password);
        }
    }

    public function group()
    {
        return $this->acl();
    }

    public function acl()
    {
        return $this->belongsTo('App\Modules\Acl\Models\Acl', 'group_id');
    }

    public function scopeAdmin($query)
    {
        return $query->filtered()->order();
    }

    public function scopeFiltered($query)
    {
        return $query->where('id', '<>', 1);
    }

    public function scopeList($query)
    {
        return $query->filtered()->order();
    }

    public function scopeOrder($query)
    {
        return $query;
    }
}
