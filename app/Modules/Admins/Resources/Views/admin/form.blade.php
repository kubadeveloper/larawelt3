@extends('admin::layouts.app')

@section('title')
    <h2>
        <a href="{!! route($routePrefix . 'index') !!}">{{ $title }}</a>
    </h2>
@endsection

@section('content')
    <div class="panel-body">
        @include('admin::common.errors')

        {!! AdminBootForm::open([
            'model' => $entity,
            'store' => $routePrefix . 'store',
            'update' => $routePrefix . 'update',
            'autocomplete' => 'off'
        ]) !!}

        <input type="text" style="display:none">
        <input type="password" style="display:none">

        <div class="col-md-6">
            {!! AdminBootForm::text('name', trans('admins::admin.name')) !!}
        </div>

        <div class="col-md-6">
            {!! AdminBootForm::email('email', trans('admins::admin.email')) !!}
        </div>

        <div class="col-md-6">
            {!! AdminBootForm::password('password', trans('admins::admin.password')) !!}

            @if ($entity->id)
                <span class="help-block">@lang('admins::admin.password_tip')</span>
            @endif
        </div>

        <div class="col-md-6">
        {!! BootForm::select('group_id', 'Группа', Acl::getGroupsSelect(), $entity->group_id) !!}
        </div>

        @if ( (method() == 'edit' && is_allowed(Acl::edit())) || (method() =='create' && is_allowed(Acl::add())))
        <div class="col-md-12">
            {!! AdminBootForm::submit(trans('admin::admin.save')) !!}
        </div>
        @endif

        {!! AdminBootForm::close() !!}
    </div>
@endsection