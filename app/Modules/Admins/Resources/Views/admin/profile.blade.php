@include('admin::common.meta-head')

<div class="wrapper">
    @include('admin::common.header')

    @include('admin::common.menu')

    <div class="content-wrapper">
        <section class="content-header">
            <h2>
                @lang('admins::admin.profile')
            </h2>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-body">
                    <div class="panel-body">
                        @include('admin::common.errors')

                        {!! AdminBootForm::open([
                            'model' => $entity,
                            'update' => $routePrefix . 'update',
                            'autocomplete' => 'off'
                        ]) !!}

                        <input type="text" style="display:none">
                        <input type="password" style="display:none">

                        <div class="col-md-6">
                            {!! AdminBootForm::text('name', trans('admins::admin.name')) !!}
                        </div>

                        <div class="col-md-6">
                            {!! AdminBootForm::email('email', trans('admins::admin.email')) !!}
                        </div>

                        <div class="col-md-6">
                            {!! AdminBootForm::password('password', trans('admins::admin.password')) !!}

                            @if ($entity->id)
                                <span class="help-block">@lang('admins::admin.password_tip')</span>
                            @endif
                        </div>
                        <div class="col-md-12">
                            {!! AdminBootForm::submit(trans('admin::admin.save')) !!}
                        </div>

                        {!! AdminBootForm::close() !!}
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('admin::common.footer')

</div>

@include('admin::common.meta-footer')