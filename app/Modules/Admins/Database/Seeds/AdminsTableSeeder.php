<?php

namespace App\Modules\Admins\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('admins')->insert([
            [
                'id'         => 2,
                'name'       => 'admin',
                'email'      => 'admin@admin.ru',
                'password'   => bcrypt('QAZadmin'),
                'group_id'   => 1,
                'created_at' => now()
            ]
        ]);
    }
}
