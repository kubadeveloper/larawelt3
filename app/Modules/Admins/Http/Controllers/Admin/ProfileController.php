<?php

namespace App\Modules\Admins\Http\Controllers\Admin;

use App\Modules\Admins\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class ProfileController extends IndexController
{
    protected $routePrefix = 'admin.profile.';

    public function getModel()
    {
        return new Admin;
    }

    public function destroy($id)
    {
        abort(403,  trans('admin::admin.messages.now_allowed'));
    }

    public function index()
    {
        abort(403,  trans('admin::admin.messages.now_allowed'));
    }

    public function create()
    {
        abort(403,  trans('admin::admin.messages.now_allowed'));
    }

    public function store(Request $request)
    {
        abort(403,  trans('admin::admin.messages.now_allowed'));
    }

    protected function getFormViewName()
    {
        return $this->viewPrefix . 'admin.profile';
    }

    public function update(Request $request, $id)
    {
        if ($id != Auth::user()->id) {
            abort(403,  trans('admin::admin.messages.now_allowed'));
        }

        $this->validate($request, $this->getRules($request, $id), [], $this->getAttributes());
        $entity = $this->getModel()->findOrFail($id);
        $data = $request->all();
        unset($data['group_id']);
        $entity->update($data);
        $this->after($entity);

        return redirect()->back()->with('message', trans($this->messages['update']));
    }

    public function edit($id)
    {

        if ($id != Auth::user()->id) {
            abort(403, trans('admin::admin.messages.now_allowed'));
        }

        $entity = $this->getModel()->findOrFail($id);
        View::share('entity', $entity);
        $this->after($entity);

        return view($this->getFormViewName(), [
            'entity' => $entity,
            'routePrefix' => $this->routePrefix
        ]);
    }
}
