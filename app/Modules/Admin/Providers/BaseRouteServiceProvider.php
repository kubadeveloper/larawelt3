<?php

namespace App\Modules\Admin\Providers;

use App\Modules\Admin\Repositories\ModuleRepository;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class BaseRouteServiceProvider extends ServiceProvider
{
    /**
     * Репозиторий модуля.
     *
     * @var ModuleRepository
     */
    protected $module;

    /**
     * Загрузка сервисов приложения.
     *
     * @return void
     */
    public function boot() : void
    {
        $this->setModule();
        $this->setNamespace();
        parent::boot();
    }

    /**
     * Регистрация роутов модуля.
     *
     * @return void
     */
    public function map() : void
    {
        $this->mapWebRoutes();
        $this->mapApiRoutes();
    }

    /**
     * Регистрация web роутов модуля.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group($this->module->getWebRoutesFilePath());
    }

    /**
     * Регистрация api роутов модуля.
     *
     * @return void
     */
    protected function mapApiRoutes() : void
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group($this->module->getApiRoutesFilePath());
    }

    /**
     * Запись репозитория модуля.
     *
     * @return void
     */
    protected function setModule() : void
    {
        preg_match('!App\\\Modules\\\(.*)\\\\!isU', get_class($this), $matches);
        $this->module = new ModuleRepository($matches[1]);
    }

    /**
     * Запись пространства имен контроллеров.
     *
     * @return void
     */
    protected function setNamespace() : void
    {
        $this->namespace = $this->module->getControllersNamespace();
    }
}
