<?php

namespace App\Modules\Admin\Providers;

use KubaDev\Modules\Support\ServiceProvider;
use App\Modules\Admin\Repositories\ModuleRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\File;

class BaseModuleServiceProvider extends ServiceProvider
{
    /**
     * Репозиторий модуля.
     *
     * @var ModuleRepository
     */
    protected $module;

    /**
     * Создание экземпляра класса.
     *
     * @param Application $app
     * @return void
     */
    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->setModule();
    }

    /**
     * Загрузка событий приложения.
     *
     * @return void
     */
    public function boot() : void
    {
        $this->loadTranslationsFrom($this->module->getLangDir(), $this->module->slug());
        $this->loadViewsFrom($this->module->getViewsDir(), $this->module->slug());
        $this->bootConfig();
    }

    /**
     * Регистрация привазок к сервис контейнеру.
     *
     * @return void
     */
    public function register() : void
    {
        $this->registerRouteServiceProvider();
    }

    /**
     * Загрузка кофигов модуля.
     *
     * @return void
     */
    protected function bootConfig() : void
    {
        if (!File::exists($this->module->getConfigDir())) return;
        $files = File::files($this->module->getConfigDir());

        foreach ($files as $file) {
            $this->mergeConfigFrom($file->getPathname(), $this->getConfigKey($file->getBasename('.php')));
        }
    }

    /**
     * Регистрация сервис провайдера роутов.
     *
     * @return void
     */
    protected function registerRouteServiceProvider() : void
    {
        $this->app->register($this->module->getRouteServiceProviderClass());
    }

    /**
     * Получение ключа для регистрации конфига модуля.
     *
     * @param string $filename
     * @return string
     */
    protected function getConfigKey(string $filename) : string
    {
        return "modules.items.{$this->module->slug()}.{$filename}";
    }

    /**
     * Запись репозитория модуля.
     *
     * @return void
     */
    protected function setModule() : void
    {
        preg_match('!App\\\Modules\\\(.*)\\\\!isU', get_class($this), $matches);
        $this->module = new ModuleRepository($matches[1]);
    }
}
