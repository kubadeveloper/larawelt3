<?php

namespace App\Modules\Admin\Providers;

use App\Modules\Admin\Http\ViewComposers\LanguagesComposer;
use App\Modules\Admin\Http\ViewComposers\MenuComposer;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    public function register() : void
    {
        parent::register();

        $this->app->make('view')->composer('admin::common.languages', LanguagesComposer::class);
        $this->app->make('view')->composer('admin::common.menu', MenuComposer::class);
    }
}
