<?php

Route::prefix(config('cms.uri'))
    ->as(config('cms.admin_prefix'))
    ->group(function () {
    Route::get('/', 'HomeController@index');
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::get('logout', 'Auth\LoginController@logout');
});
