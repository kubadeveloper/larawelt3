<?php

namespace App\Modules\Admin\Repositories;

class ModuleRepository
{
    protected $dirName;

    /**
     * Создание экземпляра класса.
     *
     * @param string $dirName
     * @return void
     */
    public function __construct(string $dirName)
    {
        $this->dirName = $dirName;
    }

    /**
     * Получение слага модуля.
     *
     * @return string
     */
    public function slug()
    {
        return mb_strtolower($this->dirName);
    }

    /**
     * Получение класса сервис провайдера роутов модуля.
     *
     * @return string
     */
    public function getRouteServiceProviderClass() : string
    {
        return 'App\Modules\\' . $this->dirName . '\Providers\RouteServiceProvider';
    }

    /**
     * Получение пространства имен контроллеров.
     *
     * @return string
     */
    public function getControllersNamespace() : string
    {
        return 'App\Modules\\' . $this->dirName . '\Http\Controllers';
    }

    /**
     * Получение пути к директории конфигурационных файлов модуля.
     *
     * @return string
     */
    public function getConfigDir() : string
    {
        return $this->baseDirectory('/Config');
    }

    /**
     * Получение пути к директории переводов модуля.
     *
     * @return string
     */
    public function getLangDir() : string
    {
        return $this->baseDirectory('/Resources/Lang');
    }

    /**
     * Получение пути к директории представлений модуля.
     *
     * @return string
     */
    public function getViewsDir() : string
    {
        return $this->baseDirectory('/Resources/Views');
    }

    /**
     * Получение пути к файлу web роутов модуля.
     *
     * @return string
     */
    public function getWebRoutesFilePath() : string
    {
        return $this->baseDirectory('/Routes/web.php');
    }

    /**
     * Получение пути к файлу api роутов модуля.
     *
     * @return string
     */
    public function getApiRoutesFilePath() : string
    {
        return $this->baseDirectory('/Routes/api.php');
    }

    /**
     * Получение пути к директории модуля.
     *
     * @param string $path
     * @return string
     */
    protected function baseDirectory(string $path = '') : string
    {
        $modulePath = 'Modules/' . $this->dirName;

        if ($path) {
            $modulePath .= $path;
        }

        return app_path($modulePath);
    }
}
