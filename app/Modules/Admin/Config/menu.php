<?php
return [
    'groups' => [
        [
            'title'    => trans('admin::admin.content'),
            'slug'     => 'content',
            'icon'     => 'fa-file',
            'priority' => 99
        ],
        [
            'title'    => trans('admin::admin.modules'),
            'slug'     => 'modules',
            'icon'     => 'fa-file',
            'priority' => 98
        ]
    ],
];
