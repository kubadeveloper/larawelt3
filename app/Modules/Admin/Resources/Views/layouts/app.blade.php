@include('admin::common.meta-head')

<div class="wrapper">
    @include('admin::common.header')

    @include('admin::common.menu')

    <div class="content-wrapper">
        <section class="content-header">
            @yield('title')

            @include('admin::common.languages')

            @hasSection('topmenu')
                @yield('topmenu')
            @else
                @include('admin::common.topmenu.all')
            @endif
        </section>

        <section class="content">
            <div class="box">
                <div class="box-body">
                    @yield('content')
                </div>
            </div>
        </section>
    </div>

    @include('admin::common.footer')

    @yield('sidemenu')

</div>

@include('admin::common.meta-footer')