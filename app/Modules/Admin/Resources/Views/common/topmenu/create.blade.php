@if(is_allowed(Acl::add()))
    <a class="btn btn-primary" href="{!! isset($url) ? $url : route($routePrefix . 'create') !!}">
        <i class="glyphicon glyphicon-plus"></i> @lang('admin::admin.add')
    </a>
@endif
