<div class="images-list" style="margin-bottom: 20px">
    @if ($entity->{$field} && $entity->getImagePath($field, 'full') || $entity->getImagePath($field, 'mini'))
        {!! AdminBootForm::label((isset($label)) ? $label : trans('admin::admin.image')) !!}

        <div class="clearfix"></div>

        <div class="timeline-body">
            @if ($entity->getImagePath($field, 'full'))
                <a href="{!! $entity->getImagePath($field, 'full') !!}" rel="ajax">
                    <img src="{!! $entity->getImagePath($field, 'mini') ?: $entity->getImagePath($field, 'thumb') !!}">
                </a>
            @else
                <img src="{!! $entity->getImagePath($field, 'mini') ?: $entity->getImagePath($field, 'thumb') !!}">
            @endif

            <a class="btn btn-danger {{ (isset($btnClasses)) ? $btnClasses : '' }}"
               onclick="deleteUpload.apply(this, ['{{ trans('admin::admin.delete_image_sure') }}'])"
               data-href="{!! route($routePrefix . 'delete-upload', ['id' => $entity, 'field' => $field]) !!}"
               data-token="{{ csrf_token() }}">
                <i class="glyphicon glyphicon-trash"></i>
            </a>
        </div>

        <div class="clearfix"></div>
    @else
        {!! AdminBootForm::file($field, (isset($label)) ? $label : trans('admin::admin.image'), ['accept' => 'image/*']) !!}

        @isset($helpBlock)
            <p class="help-block">{!! $helpBlock !!}</p>
        @endif
    @endif
</div>
