@push('js')
    <script src="{!! asset('/adminlte/plugins/ckeditor/ckeditor.js') !!}"></script>
    <script src="{!! asset('/adminlte/plugins/ckeditor/adapters/jquery.js') !!}"></script>
    <script>
        var CKEditorInstances = [];
        var CKIndex = 0;
        @foreach ($fields as $field)
            CKEditorInstances[CKIndex] = $('#{!! $field['id'] !!}').ckeditor({
                filebrowserImageBrowseUrl: '/{{ config('cms.uri') }}/filemanager?type=Images',
                filebrowserImageUploadUrl: '/{{ config('cms.uri') }}/filemanager/upload?type=Images&_token={{ csrf_token() }}',
                filebrowserBrowseUrl: '/{{ config('cms.uri') }}/filemanager?type=Files',
                filebrowserUploadUrl: '/{{ config('cms.uri') }}/filemanager/upload?type=Files&_token={{ csrf_token() }}',
                removeButtons: 'Underline',
                allowedContent: true,
                height: "350px"
            });
            CKIndex++;
        @endforeach
    </script>
@endpush
