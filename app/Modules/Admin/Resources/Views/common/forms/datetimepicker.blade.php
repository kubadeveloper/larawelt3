{{-- documentation - https://eonasdan.github.io/bootstrap-datetimepicker/ --}}
@push('css')
    <link rel="stylesheet" href="{!! asset('/adminlte/plugins/datetimepicker/bootstrap-datetimepicker.css') !!}">
@endpush

@push('js')
    <script src="{!! asset('/adminlte/plugins/datetimepicker/moment.js') !!}"></script>
    <script src="{!! asset('/adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') !!}"></script>

    <script>
        $(function () {
            @foreach($fields as $field)
            $('#{{ $field['id'] }}').datetimepicker({
                @isset($field['format'])
                    format: '{{ $field['format'] }}',
                @else
                    format: 'YYYY-MM-DD HH:mm',
                @endisset
                locale: 'ru'
            });
            @endforeach
        });
    </script>
@endpush
