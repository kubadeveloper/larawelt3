@if($entity->{$field})
    <div class="form-group">
        {!! AdminBootForm::label((isset($label)) ? $label : trans('admin::admin.file')) !!}

        <div class="input-group">
            <input type="text" class="form-control" value="{{ $entity->{$entity->getFieldName($field) ?: $field} }}" disabled>

            <a href="{{ route($routePrefix . 'download', ['id' => $entity->id, 'field' => $field]) }}" class="input-group-addon btn bg-blue">
                <i class="glyphicon glyphicon-download"></i>
            </a>

            <span class="input-group-addon btn bg-red"
                  onclick="deleteUpload.apply(this, ['{{ trans('admin::admin.delete_file_sure') }}'])"
                  data-href="{!! route($routePrefix . 'delete-upload', ['id' => $entity->id, 'field' => $field]) !!}"
                  data-token="{{ csrf_token() }}">
                <i class="glyphicon glyphicon-trash"></i>
            </span>
        </div>
    </div>
@else
    {!! AdminBootForm::file($field, (isset($label)) ? $label : trans('admin::admin.file'), [
            'accept' => $accept ?? ''
        ]) !!}

    @isset($helpBlock)
        <p class="help-block">{!! $helpBlock !!}</p>
    @endif
@endif
