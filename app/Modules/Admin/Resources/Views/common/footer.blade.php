<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.1
    </div>
    <strong>Copyright &copy; {{ date('Y') }} LaraCMS.</strong>
    All rights reserved.
</footer>

<div class="control-sidebar-bg"></div>
