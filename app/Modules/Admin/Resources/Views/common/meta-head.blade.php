<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>{{ config('app.name', 'LaraCMS') }}</title>

    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <link rel="stylesheet" href="{!! asset('/adminlte/plugins/colorbox/jquery.colorbox.css') !!}">
    <link rel="stylesheet" href="{!! asset('/adminlte/css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('/adminlte/css/font-awesome.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('/adminlte/css/AdminLTE.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('/adminlte/css/custom.css') !!}">

    @stack('css')
</head>
<body class="layout-boxed">
