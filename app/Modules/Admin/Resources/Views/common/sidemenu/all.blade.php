@if (isset($routePrefix))
    <div class="fixed-menu-container">
        <ul class="menu-fixed">
            @include('admin::common.sidemenu.list', ['routePrefix' => $routePrefix])
            @include('admin::common.sidemenu.create')
            @include('admin::common.sidemenu.submit')
        </ul>
    </div>
@endif