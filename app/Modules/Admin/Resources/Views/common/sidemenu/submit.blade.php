<li class="menu-fixed__item {{ (count($errors) > 0) ? 'has-error' : '' }}">
    <a href="#" class="menu-fixed__link js-submit" title="@lang('admin::admin.save')">
        <i class="fa fa-floppy-o"></i>
    </a>
</li>