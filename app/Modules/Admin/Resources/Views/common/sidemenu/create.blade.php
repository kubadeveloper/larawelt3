@if(is_allowed(Acl::add()))
    <li class="menu-fixed__item">
        <a href="{!! route($routePrefix . 'create') !!}" class="menu-fixed__link" title="@lang('admin::admin.add')">
            <i class="fa fa-plus"></i>
        </a>
    </li>
@endif