<li class="menu-fixed__item">
    <a href="{!! route($routePrefix . 'index') !!}" class="menu-fixed__link" title="@lang('admin::admin.list')">
        <i class="fa fa-list"></i>
    </a>
</li>