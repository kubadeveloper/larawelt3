<header class="main-header">
    <a href="/{{ config('cms.uri') }}/" class="logo">
        <img src="{!! asset('/adminlte/img/logo.png') !!}" alt="CMS" class="logo__image">
        <span class="logo__text">LaraCMS</span>
    </a>

    <nav class="navbar navbar-static-top">
        @if (Auth::user())
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="{!! home() !!}">
                            <i class="fa fa-home"></i>
                            <span class="hidden-xs">@lang('admin::admin.return_to_site')</span>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="{!! route('admin.profile.edit', Auth::user()->id) !!}">
                            <i class="fa fa-user"></i>
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="{{ url('/'.config('cms.uri').'/logout') }}">
                            <i class="fa fa-sign-out"></i>
                            <span class="hidden-xs">@lang('admin::admin.exit')</span>
                        </a>
                    </li>
                </ul>
            </div>
        @endif
    </nav>
</header>
