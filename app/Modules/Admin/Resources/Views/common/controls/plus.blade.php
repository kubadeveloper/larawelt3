@if(isset($disabled) && $disabled)
    <span class="btn btn-default btn-sm disabled" title="@lang('admin::admin.access')">
        <i class="glyphicon glyphicon-plus"></i>
    </span>
@else
    <a class="btn btn-default btn-sm" href="{!! route($routePrefix . 'create', ['parent' => $id]) !!}">
        <i class="glyphicon glyphicon-plus"></i>
    </a>
@endif
