@if(is_allowed(Acl::view()))
<a class="btn btn-primary btn-sm" title="@lang('admin::admin.look')"
   href="{!! route($routePrefix . 'edit', [$id]) !!}">
    <i class="glyphicon glyphicon-eye-open"></i>
</a>
@endif
