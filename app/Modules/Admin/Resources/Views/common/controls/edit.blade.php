@if(is_allowed(Acl::view()))
    <a class="btn btn-primary btn-sm" href="{{ route($routePrefix . 'edit', [$id]) }}"
       title="@lang('admin::admin.edit')">
        <i class="glyphicon glyphicon-pencil"></i>
    </a>
@endif
