@if(is_allowed(Acl::delete()))
    @if(isset($disabled) && $disabled)
        <span class="btn btn-danger btn-sm disabled" title="@lang('admin::admin.access')">
             <i class="glyphicon glyphicon-trash"></i>
        </span>
    @else
        {!! AdminBootForm::open(['route' => [$routePrefix . 'destroy', $id], 'method' => 'delete']) !!}
        <button type="submit" class="btn btn-danger btn-sm" title="@lang('admin::admin.delete')">
            <i class="glyphicon glyphicon-trash"></i>
        </button>
        {!! AdminBootForm::close() !!}
    @endif
@endif
