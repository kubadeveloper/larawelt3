@if (isset($supportedLocales))
    <ol class="breadcrumb">
        @foreach($supportedLocales as $key => $locale)
            <li class="{{ lang() == $key ? 'active' : '' }}">
                <a href="{{ localization()->getLocalizedURL($key) }}" hreflang="{{ $key }}">
                    {{ $locale->native() }}
                </a>
            </li>
        @endforeach
    </ol>
@endif
