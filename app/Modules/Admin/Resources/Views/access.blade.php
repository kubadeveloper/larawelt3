@extends('admin::layouts.app')

@section('title')
    <h2>{{ $title }}</h2>
@endsection

@section('topmenu')
    <div class="header-module-controls">
        <a class="btn btn-default" href="javascript:history.back()">
            <i class="glyphicon glyphicon-arrow-left"></i> @lang('admin::admin.back')
        </a>
    </div>
@endsection

@section('content')
    <h4>@lang('admin::admin.access_page')</h4>
@endsection