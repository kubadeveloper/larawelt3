@extends('admin::layouts.app')

@push('js')
    <script src="{!! asset('/adminlte/js/entity.delete.js') !!}"></script>
@endpush

@section('form_js')
    @include('admin::common.forms.datepicker', ['fields' => [['id' => 'date', 'date' => date('Y-m-d')]]])
    @include('admin::common.forms.ckeditor', ['fields' => [['id' => 'content']]])
@endsection

@section('title')
    <h2><a href="{!! URL::route($routePrefix . 'index') !!}">{{ $title }}</a></h2>
@endsection

@section('topmenu')
    @include('admin::common.topmenu.entry', ['routePrefix' => $routePrefix])
@endsection

@section('content')
    <div class="panel-body">
        @include('admin::common.errors')

        @yield('form_content')

        @if ( (method() == 'edit' && is_allowed(Acl::edit())) || (method() =='create' && is_allowed(Acl::add())))
        <div class="col-md-12">
            {!! AdminBootForm::submit(trans('admin::admin.save')) !!}
        </div>
        @endif

        {!! AdminBootForm::close() !!}
    </div>
@endsection