@if ($entities)
    @foreach ($entities as $entity)
        <div class="timeline-body" id="item-{{ $entity->id }}">
            @if ($entity->getImagePath('image', 'full'))
                <a href="{!! $entity->getImagePath('image', 'full') !!}" rel="ajax">
                    <img src="{!! $entity->getImagePath('image', 'mini') ?: $entity->getImagePath('image', 'thumb') !!}"
                         class="margin thumb">
                </a>
            @else
                <img src="{!! $entity->getImagePath('image', 'mini') ?: $entity->getImagePath('image', 'thumb') !!}"
                     class="margin thumb">
            @endif

            <a data-href="{!! route($routePrefix . 'destroy', ['gallery' => $parent, 'image' => $entity])  !!}"
               class="btn btn-danger">
                <i class="glyphicon glyphicon-trash"></i>
            </a>
        </div>
    @endforeach
@else
    <p>@lang('admin::admin.no_images')</p>
@endif
