<p class="help-block">@lang('admin::admin.files_help_block', ['values' => $mimes])</p>

@if(count($entities))
    <div class="form-group files-form-group">
        @foreach($entities as $entity)
            <input type="hidden" value="{{ route($routePrefix . 'index', [$parent]) }}">
            <div class="input-group file-input-group" style="margin-top: 10px;">
                <input type="text" class="form-control file-input-name" value="{{ $entity->{$config['field_name']} }}"
                    title="@lang('admin::admin.file_name')">

                <a data-href="{{ route($routePrefix . 'update', [$parent, $entity->id]) }}"
                   class="input-group-addon btn btn-update bg-success disabled-update" title="@lang('admin::admin.rename')">
                    <i class="glyphicon glyphicon-floppy-disk"></i>
                </a>

                <a data-href="{{ route($routePrefix . 'copy', ['id' => $entity->id, 'field' => $config['field']]) }}"
                   class="input-group-addon btn btn-copy bg-light" title="@lang('admin::admin.copy')">
                    <i class="glyphicon glyphicon-copy"></i>
                </a>

                <a href="{{ route($routePrefix . 'download', [$entity->id, $config['field']]) }}"
                   class="input-group-addon btn bg-blue" title="@lang('admin::admin.download')">
                    <i class="glyphicon glyphicon-download"></i>
                </a>

                <a data-href="{{ route($routePrefix . 'destroy', [$parent, $entity->id]) }}"
                   class="input-group-addon btn btn-destroy bg-red" title="@lang('admin::admin.delete')">
                    <i class="glyphicon glyphicon-trash"></i>
                </a>
            </div>
        @endforeach
    </div>
@else
    <p>@lang('admin::admin.no_files')</p>
@endif
