@if ($id)
    {!! AdminBootForm::label($label) !!}

    <div class="clearfix">
        <span class="btn btn-success fileinput-button">
            <i class="icon-plus icon-white"></i>
            <span>@lang('admin::fields.select_file')</span>
            <input type="hidden" value="{{ route($routePrefix . 'index', [$id]) }}">
            <input class="upload-files" type="file" name="{{ $field }}" accept="{{ $accept ?? '' }}" multiple>
            <input type="hidden" value="{{ route($routePrefix . 'store', [$id]) }}">
        </span>
    </div>

    <div id="{{ $field . '-list' }}" class="files-list"></div>
@else
    <p class="help-block">@lang('admin::fields.add_file')</p>
@endif
