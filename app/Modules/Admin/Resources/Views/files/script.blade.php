@push('js')
    @if($id)
        <script>
            $(document).ready(function () {

                $('.upload-files').each(function() {
                    loadImages('#' + $(this).attr('name') + '-list', $(this).prev().val());
                });

                $(document).on('keyup', '.file-input-name', function() {
                    $(this).next().removeClass('disabled-update');
                });

                $(document).on('click', '.file-input-group a.btn-update', function() {
                    var fileListBlock = $(this).parent().parent().parent();
                    var uploadListUrl = $(this).parent().prev().val();
                    var file_name = $(this).parent().children().first().val();
                    var formData = new FormData();
                    formData.append('file_name', file_name);
                    formData.append('_method', 'put');

                    $.ajax({
                        url: $(this).attr('data-href'),
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        data: formData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function (data)
                        {
                            if (data.state === 'success') {
                                loadImages(fileListBlock, uploadListUrl);
                            }
                        },
                        error: function(error) {
                            alert(error.status + ' ' + error.statusText);
                        }
                    });
                });

                $(document).on('click', '.file-input-group a.btn-copy', function() {
                    $.ajax({
                        url: $(this).attr('data-href'),
                        method: 'GET',
                        data: {},
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function (data)
                        {
                            if(data.state === 'success') {
                                var $temp = $("<input>");
                                $("body").append($temp);
                                $temp.val(data.path).select();
                                document.execCommand("copy");
                                $temp.remove();
                            }
                        },
                        error: function(error) {
                            alert(error.status + ' ' + error.statusText);
                        }
                    });
                });

                $(document).on('click', '.file-input-group a.btn-destroy', function (event) {
                    var fileListBlock = $(this).parent().parent().parent();
                    var uploadListUrl = $(this).parent().prev().val();

                    if (confirm('Удалить файл?')) {
                        $.ajax({
                            url: $(this).attr('data-href'),
                            type: 'DELETE',
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            },
                            success: function (data) {
                                loadImages(fileListBlock, uploadListUrl);
                            }
                        });
                        return false;
                    } else {
                        return false;
                    }
                });

                $(document).on('change', '.upload-files', function () {
                    var uploadInput = $(this);
                    var multipleMax = {{ (isset($multipleMax)) ? $multipleMax : 0 }};

                    if(multipleMax && uploadInput[0].files.length > multipleMax) {
                        alert('Вам разрешено загружать максимум ' + multipleMax + ' файла.');

                        return;
                    }

                    if(uploadInput[0].files.length > 0) {
                        var formData = new FormData();
                        $.each(uploadInput[0].files, function(index, file) {
                            formData.append('files[]', file);
                        });

                        formData.append('field', uploadInput.attr('name'));

                        $.ajax({
                            url: uploadInput.next().val(),
                            method: 'POST',
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            },
                            data: formData,
                            processData: false,
                            contentType: false,
                            cache: false,
                            beforeSend: function() {
                                uploadInput.prop('disabled', true);
                            },
                            success: function (data)
                            {
                                updateFilesInputs(uploadInput);

                                if(data.state === 'success') {
                                    uploadInput.prop('disabled', false);
                                    loadImages('#' + uploadInput.attr('name') + '-list', uploadInput.prev().val());
                                }
                            },
                            error: function(error) {
                                updateFilesInputs(uploadInput);
                                uploadInput.prop('disabled', false);
                                var errors = $.map(error.responseJSON.errors, function(errors) {
                                    return errors;
                                });

                                if(errors.length) {
                                    alert(errors[0]);
                                } else {
                                    alert(error.status + ' ' + error.statusText);
                                }
                            }
                        });
                    }
                });

                function updateFilesInputs(event) {
                    event.wrap('<form>').closest('form').get(0).reset();
                    event.unwrap();
                }

                function loadImages(filesList, url) {
                    $(filesList).load(url);
                }
            });
        </script>
    @endif
@endpush