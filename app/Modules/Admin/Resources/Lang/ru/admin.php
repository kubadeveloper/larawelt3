<?php
return [
    'return_to_site' => 'Вернуться на сайт',
    'delete_record_sure' => 'Удалить запись?',
    'no_records' => 'Нет записей',
    'save' => 'Сохранить',
    'delete' => 'Удалить',
    'edit' => 'Редактировать',
    'look' => 'Просмотреть',
    'up' => 'Поднять',
    'down' => 'Опустить',
    'publish' => 'Опубликовать',
    'unpublish' => 'Снять с публикации',
    'image' => 'Изображение',
    'file' => 'Файл',
    'delete_image_sure' => 'Удалить изображение?',
    'delete_file_sure' => 'Удалить файл?',
    'add' => 'Добавить',
    'list' => 'Список',
    'full_page' => 'Страница записи',
    'save_and_create' => 'Сохранить и создать новую',
    'form_error' => 'Ошибка при сохранении формы',
    'select' => 'Выбрать',
    'messages' => [
        'store' => 'Запись успешно добавлена',
        'update' => 'Данные успешно обновлены',
        'destroy' => 'Запись удалена',
        'now_allowed' => 'Доступ запрещен'
    ],
    'control' => 'Управление',
    'exit' => 'Выйти',
    'images' => 'Изображения',
    'no_images' => 'Нет загруженных изображений',
    'no_files' => 'Нет загруженных файлов',
    'files' => 'Файлы',
    'modules' => 'Модули',
    'content' => 'Содержание',
    'main_page' => 'Главная страница',
    'main_text' => 'Скоро тут будет руководство пользователя...',
    'access' => 'Недоступно',
    'access_page' => 'Данная страница недоступна',
    'access_field' => 'Данное поле недоступно',
    'back' => 'Назад',
    'image_too_large' => 'Максимально допустимый размер изображения: 3000x3000px',
    'files_help_block' => 'Файлы должны быть одного из следующих типов: :values',
    'file_name' => 'Название файла для отображения',
    'rename' => 'Переименовать',
    'copy' => 'Копировать путь к файлу',
    'download' => 'Скачать'
];
