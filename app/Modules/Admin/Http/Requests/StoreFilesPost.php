<?php

namespace App\Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFilesPost extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'files.*' => $this->getValidatorMimes(module_config('uploads.' . $this->get('field'))['validator'])
        ];
    }

    public function messages()
    {
        return [
            'mimes' => 'Файлы должны быть одного из следующих типов: :values.'
        ];
    }

    protected function getValidatorMimes($validator)
    {
        if(preg_match('#.*(mimes:[^|]+).*#', $validator, $matches)) {
            return $validator;
        }

        return 'mimetypes:' . implode(',', config('lfm.' . 'valid_file_mimetypes')) . '|' . $validator;
    }
}
