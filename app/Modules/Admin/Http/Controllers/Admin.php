<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Modules\Tree\Facades\Tree;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as RequestFacade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Arr;

abstract class Admin extends Controller
{
    protected $viewPrefix;

    protected $routePrefix;

    protected $perPage  = 10;

    protected $messages = [
        'store'   => 'admin::admin.messages.store',
        'update'  => 'admin::admin.messages.update',
        'destroy' => 'admin::admin.messages.destroy',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->setRoutePrefix();
        $this->setViewPrefix();
        $this->fetchEntity();
        $this->share();
    }

    public function index()
    {
        $entities = $this->getModel()->sortable()->admin()->paginate($this->perPage);

        $this->after($entities);

        return view($this->getIndexViewName(), ['entities' => $entities]);
    }

    public function create()
    {
        $entity = $this->getModel();

        $this->after($entity);

        return view($this->getFormViewName(), ['entity' => $entity]);
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->getRules($request), $this->getMessages(), $this->getAttributes());

        $entity = $this->getModel()->create($request->all());

        $this->after($entity);

        return redirect()->route($this->routePrefix . 'edit', [$entity->id])
            ->with('message', trans($this->messages['store']));
    }

    public function edit($id)
    {
        $entity = View::shared('entity');

        $this->after($entity);

        return view($this->getFormViewName(), [
            'routePrefix' => $this->routePrefix,
            'frontUrl'    => Tree::getFrontUrl($id)
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, $this->getRules($request, $id), $this->getMessages(), $this->getAttributes());

        $entity = $this->getModel()->findOrFail($id);
        $entity->update($request->all());

        $this->after($entity);

        return redirect()->back()->with('message', trans($this->messages['update']));
    }

    public function destroy($id)
    {
        $entity = $this->getModel()->find($id);
        $entity->delete();

        $this->after($entity);

        return redirect()->back()->with('message', trans($this->messages['destroy']));
    }

    public function getRules($request, $id = false)
    {
        return [];
    }

    protected function after($entity)
    {
        //
    }

    protected function getMessages()
    {
        return [];
    }

    protected function getAttributes()
    {
        $attributes = trans('admin::fields');

        return array_merge($attributes, $this->getCustomAttributes());
    }

    protected function getCustomAttributes() : array
    {
        return [];
    }

    protected function fetchEntity()
    {
        if (method() === 'edit' && $id = Arr::first(RequestFacade::route()->parameters)) {
            View::share('entity', $this->getModel()->findOrFail($id));
        }
    }

    protected function getIndexViewName()
    {
        return $this->viewPrefix . 'admin.index';
    }

    protected function getFormViewName()
    {
        return $this->viewPrefix . 'admin.form';
    }

    protected function setRoutePrefix()
    {
        if (module() && !$this->routePrefix) {
            $this->routePrefix = config('cms.admin_prefix') . module() . '.';
        }
    }

    protected function setViewPrefix()
    {
        if (module() && !$this->viewPrefix) {
            $this->viewPrefix = module() . '::';
        }
    }

    protected function share()
    {
        View::share('title', module_config('settings.title'));
        View::share('routePrefix', $this->routePrefix);
    }

    abstract public function getModel();
}
