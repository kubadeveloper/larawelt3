<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Modules\Admin\Http\Requests\StoreFilesPost;
use Illuminate\Http\Request;
use App\Facades\Uploader;
use Illuminate\Support\Facades\View;

abstract class Files extends Controller
{
    public $routePrefix = '';
    public $indexView = 'admin::files.index';
    public $configField = '';

    abstract public function getParentModel();

    abstract public function getModel();

    public function __construct()
    {
        parent::__construct();
        $this->share();
    }

    public function index($id)
    {
        $entity = $this->getParentModel()->find($id);
        $entities = $entity->files()->order()->get();

        return view($this->indexView, [
            'entities'    => $entities,
            'routePrefix' => $this->routePrefix,
            'parent'      => $entity,
            'config'      => $this->getConfig()
        ]);
    }

    public function store(StoreFilesPost $request, $parent)
    {
        $entity = $this->getParentModel()->findOrFail($parent);
        $config = $this->getConfig();

        foreach ($request->all()['files'] as $key => $file) {
            $fileName = pathinfo($request->file('files')[$key]->getClientOriginalName(), PATHINFO_FILENAME);

            if (Uploader::upload($file, $config)) {
                $file = $this->getModel();
                $file->{$config['field']} = Uploader::getName();
                $file->{$config['field_name']} = str_replace(' ', '-', $fileName);
                $file->parent()->associate($entity);
                $file->save();
            }
        }

        return response()->json(['state' => 'success'], 200);
    }

    public function update(Request $request, $parent, $id)
    {
        $config = $this->getConfig();
        $entity = $this->getModel()->find($id);
        $fileName[$config['field_name']] = str_replace(' ', '-', $request->get('file_name'));
        $entity->update($fileName);

        return response()->json(['state' => 'success'], 200);
    }

    public function destroy($parent, $id)
    {
        $entity = $this->getModel()->find($id);

        if (Uploader::delete($entity->{$this->getConfig()['field']}, $this->getConfig())) {
            $entity->delete();
        }
    }

    public function download($id, $field)
    {
        $entity = $this->getModel()->findOrFail($id);
        $fileName = $entity->{$this->getConfig()['field_name']} . '.' . $this->getExtension($entity->{$field});

        return response()->download($entity->getFileAbsPath($field), $fileName);
    }

    public function getFilePath($id, $field)
    {
        $entity = $this->getModel()->findOrFail($id);
        $path = $entity->getFilePath($field);

        return response()->json(['state' => 'success', 'path' => $path], 200);
    }

    protected function getConfig()
    {
        $config = module_config('uploads');
        if (isset($config[$this->configField])) {
            return $config[$this->configField];
        }
    }

    protected function getExtension($fileName)
    {
        $extension = '';

        if(count($fileNameArray = explode('.', $fileName)) > 1) {
            $extension = array_pop($fileNameArray);
        }

        return $extension;
    }

    protected function getValidatorMines($validator)
    {
        if(preg_match('#.*mimes:([^|]+).*#', $validator, $matches)) {
            return $matches[1];
        }

        return implode(',', config('lfm.folder_categories.image.valid_mime'));
    }

    protected function share()
    {
        View::share('mimes', $this->getValidatorMines($this->getConfig()['validator']));
    }
}
