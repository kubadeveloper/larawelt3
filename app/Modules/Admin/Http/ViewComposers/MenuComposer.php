<?php

namespace App\Modules\Admin\Http\ViewComposers;

use Illuminate\View\View;
use KubaDev\Modules\Facades\Module;

class MenuComposer
{
    public function compose(View $view = null)
    {
        $modules = Module::all();
        $groups = [];
        $items = [];

        if (empty($modules)) {
            return;
        }
        foreach ($modules as $module => $info) {
            $config = module_config('menu', strtolower($module));

            if (isset($config['groups'])) {
                $groups = array_merge($groups, $config['groups']);
            }

            if (isset($config['items'])) {
                $items = array_merge($items, $config['items']);
            }
        }
        if (empty($groups)) {
            return;
        }

        if (empty($items)) {
            return;
        }
        $groups = collect($groups)->sortBy('title')->sortByDesc('priority');
        $items = collect($items)->sortBy('title')->sortByDesc('priority');
        $groups = $groups->toArray();
        foreach ($groups as &$group) {
            foreach ($items as $item) {
                if ($item['group'] == $group['slug']) {
                    if (isset($item['route']) && is_route_allowed($item['route'])){
                        $group['items'][] = $item;
                    }
                }
            }
        }
        //очищаем пустые группы без вложенных ссылок
        $items = [];
        foreach ($groups as $item) {
            if (isset($item['items']) && !empty($item['items'])){
                $items[]= $item;
            }
        }

        $view->with('items', $items);
    }
}
