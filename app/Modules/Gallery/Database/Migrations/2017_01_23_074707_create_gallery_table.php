<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryTable extends Migration
{
    public function up()
    {
        Schema::create('gallery', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->integer('priority')->nullable()->index();
            $table->tinyInteger('active');
            $table->date('date')->useCurrent();

            $table->string('title_en')->nullable();
            $table->string('title_ru')->nullable();
            $table->string('title_ky')->nullable();
            $table->mediumText('preview_en')->nullable();
            $table->mediumText('preview_ru')->nullable();
            $table->mediumText('preview_ky')->nullable();
            $table->text('content_en')->nullable();
            $table->text('content_ru')->nullable();
            $table->text('content_ky')->nullable();

            $table->string('image')->nullable();


            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('gallery');
    }
}
