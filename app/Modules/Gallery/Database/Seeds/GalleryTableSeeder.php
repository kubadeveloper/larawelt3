<?php

namespace App\Modules\Gallery\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GalleryTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('gallery')->insert(
            [
                'id'         => 1,
                'priority'   => 0,
                'active'     => 1,
                'date'       => date("Y-m-d"),
                'title_ru'   => 'Природа',
                'preview_ru' => 'Приро́да — материальный мир Вселенной',
                'content_ru' => 'Приро́да — материальный мир Вселенной',
                'created_at' => now()
            ]
        );
    }
}
