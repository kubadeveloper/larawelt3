<?php

Route::localizedGroup(function () {
    Route::prefix(config('cms.uri'))
        ->as(config('cms.admin_prefix'))
        ->group(function () {
            Route::resource('gallery', 'Admin\IndexController');
            Route::put('gallery/priority/{id}/{direction}', 'Admin\IndexController@priority')
                ->name('gallery.priority');
            Route::resource('gallery.images', 'Admin\ImagesController');
        });
});

