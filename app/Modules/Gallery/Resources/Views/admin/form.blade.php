@extends('admin::admin.form')

@section('form_content')
    {!! AdminBootForm::open([
        'model' => $entity,
        'store' => $routePrefix . 'store',
        'update' => $routePrefix . 'update',
        'autocomplete' => 'off',
        'files' => true
    ]) !!}

    <div class="col-md-6">
        {!! AdminBootForm::text('title', trans('admin::fields.title')) !!}
    </div>

    <div class="col-md-6">
        {!! AdminBootForm::text('date', trans('admin::fields.date'), $entity->date ? null : date('Y-m-d')) !!}
    </div>

    <div class="col-md-6">
        {!! AdminBootForm::hidden('active', 0) !!}
        {!! AdminBootForm::checkbox('active', trans('admin::fields.active'), 1) !!}
    </div>

    <div class="col-md-6 ">
        {!! AdminBootForm::text('priority', trans('admin::fields.priority')) !!}
    </div>

    <div class="col-md-12">
        {!! AdminBootForm::textarea('preview', trans('admin::fields.preview'), null, ['rows' => '5']) !!}
    </div>

    <div class="col-md-12">
        {!! AdminBootForm::textarea('content', trans('admin::fields.content')) !!}

        <div class="clearfix"></div>
    </div>

    <div class="col-md-12">
        @include('admin::images.form', ['id' => $entity->id, 'routePrefix' => $routePrefix . 'images.'])
    </div>
@endsection