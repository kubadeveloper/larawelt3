<?php
return [
    'title'        => trans('gallery::admin.title'),
    'localization' => true,
    'front_link'   => true,
    'acl'          => true
];