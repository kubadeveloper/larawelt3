<?php
return [
    'image' => [
        'path'      => '/uploads/gallery/',
        'validator' => 'mimes:jpeg,jpg,png|max:10000',
        'field'     => 'image',
        'thumbs'    => [
            'full'  => [
                'path'   => 'full/',
                'width'  => 800,
                'height' => false
            ],
            'thumb' => [
                'path'   => 'thumb/',
                'width'  => 350,
                'height' => false,
            ],
            'mini'  => [
                'path'   => 'mini/',
                'width'  => 150,
                'height' => false
            ]
        ]
    ]
];