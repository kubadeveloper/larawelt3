<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesReviewsFilesTable extends Migration
{
    public function up()
    {
        Schema::create('articles_reviews_files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('review_file');
            $table->string('file_name');
            $table->integer('parent_id')->unsigned();
            $table->foreign('parent_id')->references('id')->on('articles')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('articles_reviews_files');
    }
}
