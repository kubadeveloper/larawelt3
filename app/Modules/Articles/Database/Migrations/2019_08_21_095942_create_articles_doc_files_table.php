<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesDocFilesTable extends Migration
{
    public function up()
    {
        Schema::create('articles_doc_files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('document_file');
            $table->string('file_name');
            $table->integer('parent_id')->unsigned();
            $table->foreign('parent_id')->references('id')->on('articles')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('articles_doc_files');
    }
}
