<?php

namespace App\Modules\Articles\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticlesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('articles')->insert(
            [
                'id'         => 1,
                'lang'       => 'ru',
                'priority'   => '0',
                'title'      => 'Статья',
                'date'       => date("Y-m-d"),
                'preview'    => 'Статья',
                'content'    => 'Статья́ — это жанр журналистики',
                'active'     => 1,
                'created_at' => now()
            ]
        );
    }
}
