<?php

namespace App\Modules\Articles\Facades;

use App\Modules\Sitemap\Sitemap as BaseSitemap;

class Sitemap extends BaseSitemap
{
    protected $model = 'Article';
}
