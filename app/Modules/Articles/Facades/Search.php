<?php

namespace App\Modules\Articles\Facades;

use App\Modules\Search\Search as BaseSearch;

class Search extends BaseSearch
{
    protected $model = 'Article';
}
