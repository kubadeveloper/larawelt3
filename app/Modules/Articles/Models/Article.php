<?php

namespace App\Modules\Articles\Models;

use App\Models\File;
use App\Models\Model;
use App\Models\Filters;
use Illuminate\Database\Eloquent\Builder;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Notifications\Notifiable;

class Article extends Model
{
    use Notifiable, Sortable, File, Filters;

    public    $filters     = [
        'title' => ['title', 'LIKE', '%?%']
    ];

    protected $imageFields = ['image'];

    protected $fileFields = ['file'];

    public function scopeSitemap(Builder $query)
    {
        return $query->active();
    }

    public function scopeOrder($query)
    {
        return $query->orderBy('priority', 'desc')->orderBy('date', 'desc');
    }

    public function docFiles()
    {
        return $this->hasMany(DocFiles::class, 'parent_id', 'id');
    }

    public function reviewsFiles()
    {
        return $this->hasMany(ReviewsFiles::class, 'parent_id', 'id');
    }
}
