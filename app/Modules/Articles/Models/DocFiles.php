<?php

namespace App\Modules\Articles\Models;

use App\Models\Model;
use App\Models\File;

class DocFiles extends Model
{
    use File;

    protected $table = 'articles_doc_files';

    public function scopeOrder($query)
    {
        return $query->latest();
    }

    public function parent()
    {
        return $this->belongsTo(Article::class, 'parent_id');
    }
}
