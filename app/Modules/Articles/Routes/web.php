<?php

Route::localizedGroup(function () {
    Route::prefix(config('cms.uri'))
        ->as(config('cms.admin_prefix'))
        ->group(function () {
            Route::resource('articles', 'Admin\IndexController');
            Route::put('articles/priority/{id}/{direction}', 'Admin\IndexController@priority')
                ->name('articles.priority');
            Route::delete('articles/delete-upload/{id}/{field}', 'Admin\IndexController@deleteUpload')
                ->name('articles.delete-upload');
            Route::get('articles/download/{id}/{field}', 'Admin\IndexController@download')
                ->name('articles.download');

            Route::resource('articles.documents', 'Admin\DocFilesController')->except('create', 'show', 'edit');
            Route::get('articles/documents/download/{id}/{field}', 'Admin\DocFilesController@download')
                ->name('articles.documents.download');
            Route::get('articles/documents/copy/{id}/{field}', 'Admin\DocFilesController@getFilePath')
                ->name('articles.documents.copy');

            Route::resource('articles.reviews', 'Admin\ReviewsFilesController')->except('create', 'show', 'edit');
            Route::get('articles/reviews/download/{id}/{field}', 'Admin\ReviewsFilesController@download')
                ->name('articles.reviews.download');
            Route::get('articles/reviews/copy/{id}/{field}', 'Admin\ReviewsFilesController@getFilePath')
                ->name('articles.reviews.copy');
        });
});