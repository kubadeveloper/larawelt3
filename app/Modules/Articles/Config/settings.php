<?php
return [
    'title'        => trans('articles::admin.title'),
    'localization' => true,
    'front_link'   => true,
    'acl'          => true
];