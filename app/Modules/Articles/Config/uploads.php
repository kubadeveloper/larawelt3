<?php
return [
    'image' => [
        'path'      => '/uploads/articles/',
        'validator' => 'max:10000',
        'thumbs'    => [
            'full'  => [
                'path'   => 'full/',
                'width'  => 800,
                'height' => false
            ],
            'thumb' => [
                'path'   => 'thumb/',
                'width'  => 350,
                'height' => false,
            ],
            'mini'  => [
                'path'   => 'mini/',
                'width'  => 150,
                'height' => false
            ]
        ]
    ],
    'file'  => [
        'path'       => '/uploads/articles/files/',
        'validator'  => 'mimes:xlsx',
        'field_name' => 'file_name'
    ],
    'document_file'  => [
        'path'       => '/uploads/articles/files/',
        'validator'  => 'mimes:xlsx',
        'field'      => 'document_file',
        'field_name' => 'file_name'
    ],
    'review_file'  => [
        'path'       => '/uploads/articles/files/',
        'validator'  => 'mimes:pdf',
        'field'      => 'review_file',
        'field_name' => 'file_name'
    ]
];