<?php

namespace App\Modules\Articles\Http\Controllers\Admin;

use App\Modules\Admin\Http\Controllers\Files;
use App\Modules\Articles\Models\Article;
use App\Modules\Articles\Models\DocFiles as Model;

class DocFilesController extends Files
{
    public $routePrefix = 'admin.articles.documents.';

    public $configField = 'document_file';

    public function getParentModel()
    {
        return new Article();
    }

    public function getModel()
    {
        return new Model();
    }

    public function index($id)
    {
        $entity = $this->getParentModel()->find($id);
        $entities = $entity->docFiles()->order()->get();

        return view($this->indexView, [
            'entities'    => $entities,
            'routePrefix' => $this->routePrefix,
            'parent'      => $entity,
            'config'      => $this->getConfig()
        ]);
    }
}