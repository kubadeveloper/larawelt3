<?php

namespace App\Modules\Articles\Http\Controllers\Admin;

use App\Http\Controllers\File;
use App\Modules\Admin\Http\Controllers\Admin;
use App\Modules\Admin\Http\Controllers\Priority;
use App\Modules\Articles\Models\Article;

class IndexController extends Admin
{
    use File, Priority;

    public function getModel()
    {
        return new Article();
    }

    public function getRules($request, $id = false)
    {
        return [
            'title'    => 'sometimes|required|max:255',
            'priority' => 'sometimes|required'
        ];
    }
}
