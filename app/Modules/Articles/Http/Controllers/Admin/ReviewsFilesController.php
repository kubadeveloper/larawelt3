<?php

namespace App\Modules\Articles\Http\Controllers\Admin;

use App\Modules\Admin\Http\Controllers\Files;
use App\Modules\Articles\Models\Article;
use App\Modules\Articles\Models\ReviewsFiles as Model;

class ReviewsFilesController extends Files
{
    public $routePrefix = 'admin.articles.reviews.';

    public $configField = 'review_file';

    public function getParentModel()
    {
        return new Article();
    }

    public function getModel()
    {
        return new Model();
    }

    public function index($id)
    {
        $entity = $this->getParentModel()->find($id);
        $entities = $entity->reviewsFiles()->order()->get();

        return view($this->indexView, [
            'entities'    => $entities,
            'routePrefix' => $this->routePrefix,
            'parent'      => $entity,
            'config'      => $this->getConfig()
        ]);
    }
}