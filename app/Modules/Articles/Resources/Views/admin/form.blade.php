@extends('admin::admin.form')

@include('admin::files.script', ['id' => $entity->id, 'multipleMax' => 4])

@section('form_content')
    {!! AdminBootForm::open([
        'model' => $entity,
        'store' => $routePrefix . 'store',
        'update' => $routePrefix . 'update',
        'autocomplete' => 'off',
        'files' => true
    ]) !!}

    <div class="col-md-6">
        {!! AdminBootForm::text('title', trans('admin::fields.title')) !!}
    </div>

    <div class="col-md-6">
        {!! AdminBootForm::text('date', trans('admin::fields.date'), $entity->date ?: date('Y-m-d')) !!}
    </div>

    <div class="col-md-6">
        {!! AdminBootForm::hidden('active', 0) !!}
        {!! AdminBootForm::checkbox('active', trans('admin::fields.active'), 1) !!}
    </div>

    <div class="col-md-6">
        {!! AdminBootForm::text('priority', trans('admin::fields.priority'), $entity->priority ?: 0) !!}
    </div>

    <div class="clearfix"></div>

    <div class="col-md-6">
        {!! AdminBootForm::textarea('preview', trans('admin::fields.preview'), null, ['rows' => '5']) !!}
    </div>

    <div class="col-md-6">
        @include('admin::common.forms.image', ['entity' => $entity, 'routePrefix' => $routePrefix, 'field' => 'image'])
    </div>

    <div class="col-md-6">
        @include('admin::common.forms.file', [
            'entity'      => $entity,
            'routePrefix' => $routePrefix,
            'label'       => trans('admin::fields.file'),
            'field'       => 'file',
            'accept'      => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ])
    </div>

    <div class="col-md-12">
        {!! AdminBootForm::textarea('content', trans('admin::fields.content')) !!}
        <div class="clearfix"></div>
    </div>

    @include('admin::common.forms.seo')

    @if($entity->id)
        <div class="col-md-12">
            @include('admin::files.form', [
                'label'       => trans('articles::fields.documents'),
                'id'          => $entity->id,
                'routePrefix' => $routePrefix . 'documents.',
                'field'       => 'document_file',
                'accept'      => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            ])
        </div>

        <div class="col-md-12">
            @include('admin::files.form', [
                'label'       => trans('articles::fields.reviews'),
                'id'          => $entity->id,
                'routePrefix' => $routePrefix . 'reviews.',
                'field'       => 'review_file',
                'accept'      => 'application/pdf'
            ])
        </div>
    @else
        <p class="help-block">@lang('admin::fields.add_file')</p>
    @endif
@endsection