<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockerTable extends Migration
{
    public function up()
    {
        Schema::create('blocker', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('value_from')->nullable();
            $table->string('value_to')->nullable();
            $table->tinyInteger('active')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('blocker');
    }
}
