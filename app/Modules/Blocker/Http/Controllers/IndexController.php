<?php

namespace App\Modules\Blocker\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Blocker\Models\Blocker;

class IndexController extends Controller
{
    public function getModel()
    {
        return new Blocker;
    }

    public function block()
    {
        return view('blocker::blocked');
    }
}
