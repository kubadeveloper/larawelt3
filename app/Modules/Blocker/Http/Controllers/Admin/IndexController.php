<?php

namespace App\Modules\Blocker\Http\Controllers\Admin;

use App\Modules\Admin\Http\Controllers\Admin;
use App\Modules\Blocker\Models\Blocker;

class IndexController extends Admin
{
    public function getModel()
    {
        return new Blocker();
    }

    public function getRules($request, $id = false)
    {
        return [
            'title' => 'sometimes|required|max:255',
            'value_from' => 'sometimes|required',
            'value_to' => 'sometimes|required'
        ];
    }
}
