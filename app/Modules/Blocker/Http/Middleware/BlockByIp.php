<?php

namespace App\Modules\Blocker\Http\Middleware;

use Closure;
use App\Modules\Blocker\Models\Blocker;

class BlockByIp
{
    public function handle($request, Closure $next)
    {
        $ip = ip2long(request()->ip());

        $entities = Blocker::items()->get();
        $blockedIp = [];

        foreach ($entities as $entity) {
            $from = ip2long($entity->value_from);
            $to = ip2long($entity->value_to);

            if ($ip >= $from && $ip <= $to) {
                return redirect()->route('blocker.blocked');
            }
        }

        return $next($request);
    }
}
