<?php

Route::localizedGroup(function () {
    Route::prefix(config('cms.uri'))
        ->as(config('cms.admin_prefix'))
        ->group(function (){
            Route::resource('blocker', 'Admin\IndexController');
        });

    Route::get('blocked', 'IndexController@block')->name('blocker.blocked');
});

