<?php
return [
    'items' => [
        [
            'icon'  => 'fa-ban',
            'group' => 'modules',
            'route' => 'admin.blocker.index',
            'title' => 'Блокировщик IP'
        ]
    ]
];