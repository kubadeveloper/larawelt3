@extends('admin::admin.form')

@section('form_content')

    {!! AdminBootForm::open([
        'model' => $entity,
        'store' => $routePrefix.'store',
        'update' => $routePrefix.'update',
        'autocomplete' => 'off',
        'files' => true
    ]) !!}

    <div class="col-md-6">
        {!! AdminBootForm::text('title', trans('admin::fields.title')) !!}
    </div>

    <div class="col-md-6">
        {!! AdminBootForm::hidden('active', 0) !!}
        {!! AdminBootForm::checkbox('active', trans('admin::fields.active'), 1) !!}
    </div>

    <div class="clearfix"></div>

    <div class="col-md-6">
        {!! AdminBootForm::text('value_from', 'Диапазон - от', null) !!}
    </div>

    <div class="col-md-6">
        {!! AdminBootForm::text('value_to', 'Диапазон - до', null) !!}
    </div>

@endsection
