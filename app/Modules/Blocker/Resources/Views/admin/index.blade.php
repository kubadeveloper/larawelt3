@extends('admin::admin.index')

@section('th')
    <th>@sortablelink('title', trans('admin::fields.title'))</th>
    <th>@sortablelink('value_from', 'Диапазон')</th>
    <th>@lang('admin::admin.control')</th>
@endsection

@section('td')
    @foreach ($entities as $entity)
        <tr @if (!$entity->active) class="unpublished" @endif>
            <td>{{ $entity->title }}</td>
            <td>
                {{ $entity->value_from }} - {{ $entity->value_to }}
            </td>
            <td class="controls">@include ('admin::common.controls.all', ['routePrefix'=>$routePrefix, 'id'=>$entity->id])</td>
        </tr>
    @endforeach
@endsection