<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>@lang('blocker::index.blocked', ['ip' => request()->ip()])</title>

    <style>
        html,
        body {
            margin: 0;
            height: 100%;
            font-family: "Calibri";
        }

        * {
            box-sizing: border-box;
            outline: none;
        }

        .blocker {
            display: table;
            width: 100%;
            height: 100%;
            padding: 20px;
        }

        .blocker__inner {
            display: table-cell;
            vertical-align: middle;
            font-size: 32px;
            text-align: center;
            color: #333;
            font-weight: 100;
        }
    </style>
</head>
<body>
    <div class="blocker">
        <div class="blocker__inner">
            @lang('blocker::index.blocked', ['ip' => request()->ip()])
        </div>
    </div>
</body>
</html>