<?php

namespace App\Modules\Blocker\Models;

use App\Models\Model;
use Kyslik\ColumnSortable\Sortable;

class Blocker extends Model
{
    use Sortable;

    public $table = 'blocker';
}
