<?php

namespace App\Modules\Search\Providers;

use App\Modules\Admin\Providers\BaseModuleServiceProvider;
use App\Modules\Search\Http\ViewComposers\MainComposer;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    public function register() : void
    {
        parent::register();

        $this->app->make('view')->composer('search::main', MainComposer::class);
    }
}
