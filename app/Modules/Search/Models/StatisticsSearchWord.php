<?php

namespace App\Modules\Search\Models;

use Illuminate\Database\Eloquent\Model;

class StatisticsSearchWord extends Model
{
    public $timestamps = false;
}
