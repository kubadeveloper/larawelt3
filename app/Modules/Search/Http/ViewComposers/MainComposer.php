<?php

namespace App\Modules\Search\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Http\Request;

class MainComposer
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function compose(View $view)
    {
        $fieldName = module_config('settings.field_name', 'search', 'query');
        $query     = $this->request->get($fieldName);

        $view->with('query', $query);
    }
}
