<?php

namespace App\Modules\Search\Http\Middleware;

use App\Modules\Tree\Facades\Breadcrumbs;
use App\Modules\Tree\Facades\Tree;
use Closure;
use Illuminate\Support\Facades\View;

class SearchMeta
{
    public function handle($request, Closure $next)
    {
        $title = trans('search::index.title');
        $og    = [
            'site_name' => config('app.name'),
            'image'     => asset(config('cms.og-image')),
            'title'     => $title
        ];
        $meta  = [
            'title' => $title,
            'h1'    => $title
        ];

        View::share('og', (object)$og);
        View::share('meta', (object)$meta);

        Breadcrumbs::add(Tree::getRoot()->title, home());
        Breadcrumbs::add($title, route('search'));

        return $next($request);
    }
}
