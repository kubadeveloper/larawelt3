<?php

namespace App\Modules\Search\Http\Controllers;

use App\Modules\Search\Http\Middleware\SearchMeta;
use App\Modules\Search\Models\StatisticsSearchWord;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use KubaDev\Modules\Repositories\LocalRepository;

class IndexController extends Controller
{
    protected $modules;

    protected $fieldName;

    protected $timeout;

    protected $resultLimit;

    public function __construct()
    {
        $this->middleware(SearchMeta::class);

        $this->modules     = app('modules');
        $this->fieldName   = module_config('settings.field_name', 'search', 'query');
        $this->timeout     = module_config('settings.timeout', 'search', 3);
        $this->resultLimit = module_config('settings.result_limit', 'search', 1000);
    }

    public function search(Request $request)
    {
        $query     = (string)$request->get($this->fieldName);
        $validator = $this->validator($query, $request->ip());

        if ($validator->fails()) {
            return view('search::error')->withErrors($validator);
        }

        $modules = $this->modules->all();
        $results = [];
        $total   = 0;

        foreach ($modules as $module) {
            $className = 'App\Modules\\' . $module['basename'] . '\Facades\Search';
            if (!class_exists($className)) {
                continue;
            }

            $class  = new $className($query, $module['basename']);
            $result = $class->getResult();

            if (count($result) && $result['total']) {
                $results[$module['slug']] = $result;
                $total                    += $result['total'];
            }
        }

        $results = collect($results)->sortByDesc('priority')->toArray();

        $statistics          = $this->getModel();
        $statistics->date    = date('Y-m-d H:i:s');
        $statistics->lang    = lang();
        $statistics->ip      = ip2long($request->ip());
        $statistics->query   = $query;
        $statistics->results = $total;
        $statistics->save();

        if ($total > $this->resultLimit) {
            $validator->errors()->add($this->fieldName, trans('search::index.errors.many'));

            return view('search::error')->withErrors($validator);
        }

        return view('search::index', ['query' => $query, 'total' => $total, 'results' => $results]);
    }

    protected function validator(string &$query, $ip)
    {
        $validator = Validator::make([$this->fieldName => $query], $this->rules(), $this->messages());

        if ($validator->fails()) {
            return $validator;
        }

        $query = str_replace(['*', '%'], '', $query);

        if (!$query) {
            $validator->errors()->add($this->fieldName, trans('search::index.errors.empty'));

            return $validator;
        }

        if (mb_strlen($query) < 3) {
            $validator->errors()->add($this->fieldName, trans('search::index.errors.short_word'));

            return $validator;
        }

        $queries = explode(' ', $query);

        foreach ($queries as $i => $q) {
            $q = trim($q);
            if (mb_strlen($q) < 3) {
                unset($queries[$i]);
            } else {
                $queries[$i] = $q;
            }
        }

        $query = implode(' ', $queries);

        if (mb_strlen($query) < 3) {
            $validator->errors()->add($this->fieldName, trans('search::index.errors.short_words'));

            return $validator;
        }

        if (!$this->isAllowedIp($ip)) {
            $validator->errors()->add($this->fieldName, trans('search::index.errors.timeout', [
                'seconds' => $this->timeout
            ]));

            return $validator;
        }

        return $validator;
    }

    protected function rules() : array
    {
        return [
            $this->fieldName => 'bail|required|min:3|max:55'
        ];
    }

    protected function messages() : array
    {
        return [
            'required' => trans('search::index.errors.empty'),
            'min'      => trans('search::index.errors.short_word'),
            'max'      => trans('search::index.errors.long_word')
        ];
    }

    protected function isAllowedIp($ip) : bool
    {
        $stats = $this->getModel()->newQuery()
            ->where('ip', ip2long($ip))
            ->latest('date')
            ->first();

        if ($stats === null) {
            return true;
        }

        $diff = time() - strtotime($stats->date);

        return $diff > $this->timeout;
    }

    protected function getModel()
    {
        return new StatisticsSearchWord();
    }
}
