<?php
return [
    'title'        => trans('search::index.title'),
    'localization' => false,
    'field_name'   => 'q',
    'timeout'      => 3,
    'result_limit' => 1000
];
