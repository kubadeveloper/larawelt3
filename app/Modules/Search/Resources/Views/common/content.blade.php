@if(count($items))
    <div>
        <h2>{{ $title }}</h2>
        <ul>
            @foreach($items as $item)
                <li>
                    <h4><a href="{{ $item->search->url }}">{!! $item->search->title !!}</a></h4>

                    @isset($item->search->preview)
                        <p>{!! $item->search->preview !!}</p>
                    @endisset
                </li>
            @endforeach
        </ul>
    </div>
@endif
