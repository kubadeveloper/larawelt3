@extends('layouts.inner')

@section('content')
    @if($total && count($results))
        <p>@lang('search::index.total_results', ['query' => $query,'total' => $total])</p>

        @foreach($results as $result)
            {!! $result['content'] !!}
        @endforeach
    @else
        @lang('search::index.nothing_found')
    @endif
@endsection
