<form class="navbar-form navbar-right" action="{{ route('search') }}" method="GET">
    <div class="input-group">
        <input type="text" name="{{ module_config('settings.field_name', 'search', 'query') }}" value="{{ $query }}"
               placeholder="@lang('search::index.placeholder')" minlength="3" maxlength="55" required>

        <div class="input-group-btn">
            <button class="btn btn-default" type="submit">
                <i class="glyphicon glyphicon-search"></i>
            </button>
        </div>
    </div>
</form>
