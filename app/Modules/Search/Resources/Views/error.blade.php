@extends('layouts.inner')

@section('content')
    @if($errors->has(module_config('settings.field_name', 'search', 'query')))
        <div class="alert alert-danger">
            {{ $errors->first(module_config('settings.field_name', 'search', 'query')) }}
        </div>
    @endif
@endsection
