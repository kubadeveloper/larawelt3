<?php

Route::localizedGroup(function () {
    Route::get('search', 'IndexController@search')->name('search');
});
