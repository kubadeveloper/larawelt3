<?php

namespace App\Modules\Search;

use App\Modules\Search\Helpers\Stem;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class Search
{
    /**
     * Название папки модуля.
     *
     * @var string
     */
    protected $module;

    /**
     * Название класса модели.
     *
     * @var string
     */
    protected $model;

    /**
     * Название роута.
     *
     * @var string
     */
    protected $route;

    /**
     * Строка запроса.
     *
     * @var string
     */
    protected $query;

    /**
     * Приоритет результата поиска.
     *
     * @var integer
     */
    protected $priority = 0;

    /**
     * Обработчик строк.
     *
     * @var \App\Modules\Search\Helpers\Stem
     */
    protected $stem;

    /**
     * Название стандартного представления.
     *
     * @var string
     */
    protected $viewName = 'search::common.content';

    /**
     * Поля в которых будет производиться поиск.
     *
     * @var array
     */
    protected $searchFields = [
        'title', 'preview', 'content', 'meta_h1', 'meta_title', 'meta_keywords', 'meta_description'
    ];

    /**
     * Поля которые не будут выделяться.
     *
     * @var array
     */
    protected $highLightExcept = [
        'content', 'meta_h1', 'meta_title', 'meta_keywords', 'meta_description'
    ];

    /**
     * Слова запроса.
     *
     * @var array
     */
    protected $queryWords = [];

    /**
     * Создание экземпляра класса.
     *
     * @param string $query
     * @param string $module
     * @return void
     */
    public function __construct(string $query, string $module)
    {
        $this->query  = $query;
        $this->module = $module;

        if (is_null($this->model)) {
            $this->model = $module;
        }

        if (is_null($this->route)) {
            $this->route = lcfirst($module) . '.show';
        }

        $this->stem = new Stem();
    }

    /**
     * Получение полей поиска.
     *
     * @return array
     */
    public function getSearchFields() : array
    {
        return $this->searchFields;
    }

    /**
     * Получение название представления.
     *
     * @return string
     */
    public function getViewName() : string
    {
        return $this->viewName;
    }

    /**
     * Получение результата запроса.
     *
     * @return array
     *
     * @throws \Throwable
     */
    public function getResult() : array
    {
        if (!$this->hasTreePage()) return [];

        $entities = $this->getEntities();
        $entities = $this->prepareEntities($entities);

        return ['total' => count($entities), 'content' => $this->getContent($entities), 'priority' => $this->priority];
    }

    /**
     * Получение html для отображения результата.
     *
     * @param Collection $entities
     * @return string
     *
     * @throws \Throwable
     */
    protected function getContent(Collection $entities) : string
    {
        return view($this->getViewName(), ['title' => $this->getModuleTitle(), 'items' => $entities])->render();
    }

    /**
     * Получение записей таблицы.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function getEntities() : Collection
    {
        return $this->getModel()->newQuery()->items()
            ->where(function ($query) {
                $this->buildSearchWhere($query);
            })
            ->get();
    }

    /**
     * Получение записей с дополнительными данными.
     *
     * @param \Illuminate\Database\Eloquent\Collection $entities
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function prepareEntities(Collection $entities) : Collection
    {
        $highLightFields = array_diff($this->searchFields, $this->highLightExcept);

        foreach ($entities as &$entity) {
            $tmp = [];

            foreach ($highLightFields as $highLightField) {
                $function             = $this->getFunctionName($highLightField);
                $tmp[$highLightField] = method_exists($this, $function) ? $this->$function($entity)
                    : $this->highLight($entity->{$highLightField});
            }

            $tmp['url']     = $this->getUrl($entity);
            $entity->search = (object)$tmp;
        }

        return $entities;
    }

    /**
     * Построить запрос для поиска.
     *
     * @param Builder $query
     * @return void
     */
    protected function buildSearchWhere(Builder &$query) : void
    {
        $searchWords = $this->getPrepareQuery();
        $fields      = $this->getSearchFields();
        $isFirst     = true;

        foreach ($fields as $field) {
            $where   = $isFirst ? 'where' : 'orWhere';
            $isFirst = false;

            $query->$where(function (Builder $query) use ($field, $searchWords) {
                $isFirst = true;

                foreach ($searchWords as $searchWord) {
                    $where   = $isFirst ? 'where' : 'orWhere';
                    $isFirst = false;

                    $query->$where($field, 'LIKE', $searchWord);
                }
            });
        }
    }

    /**
     * Получить подготовленную строку запроса.
     *
     * @return array
     */
    protected function getPrepareQuery() : array
    {
        $query      = strip_tags(mb_strtolower($this->query, 'UTF-8'));
        $query      = preg_replace('/[^\w\sа-яё]/iu', ' ', $query);
        $words      = explode(' ', $query);
        $queryWords = [];
        $swaps      = [
            0  => [0, 1, 2],
            1  => [0, 2, 1],
            2  => [1, 0, 2],
            3  => [1, 2, 0],
            4  => [2, 0, 1],
            5  => [2, 1, 0],
            6  => [0, 1],
            7  => [1, 0],
            8  => [0, 2],
            9  => [2, 0],
            10 => [1, 2],
            11 => [2, 1]
        ];

        foreach ($words as $word) {
            if (strlen($word) >= 3) {
                $tmp          = $this->stem->stemWord($word);
                $queryWords[] = $tmp;

                $this->setQueryWord($tmp);
            }
        }

        if (count($queryWords) === 0) {
            return $queryWords;
        }

        if (count($queryWords) === 1) {
            return ['%' . $queryWords[0] . '%'];
        }

        $result = [];

        for ($i = 0; $i < count($swaps); $i++) {
            $curWords = [];

            if (sizeof($swaps[$i]) <= count($queryWords)) {
                for ($j = 0; $j < count($swaps[$i]); $j++) {
                    if ($swaps[$i][$j] >= count($queryWords)) {
                        $curWords = [];
                        break;
                    }

                    $curWords[] = $queryWords[$swaps[$i][$j]];
                }
            }

            if (count($curWords)) {
                $result[] = '%' . implode('%', $curWords) . '%';
            }
        }

        if (count($queryWords) > 3) {
            for ($i = 0; $i < count($queryWords); $i++) {
                $result[] = '%' . $queryWords[$i] . '%';
            }
        }

        return $result;
    }

    /**
     * Запись слов запроса.
     *
     * @param string $word
     * @return void
     */
    protected function setQueryWord(string $word) : void
    {
        $this->queryWords[] = $word;
    }

    /**
     * Получение ссылки на запись.
     *
     * @param $entity
     * @return string
     */
    protected function getUrl($entity) : string
    {
        return route($this->route, ['id' => $entity->getRouteKey()]);
    }

    /**
     * Получение объекта модели.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function getModel() : Model
    {
        $className = '\App\Modules\\' . $this->module . '\Models\\' . $this->model;

        return new $className();
    }

    /**
     * Название модуля.
     *
     * @return string
     */
    protected function getModuleTitle() : string
    {
        return (string)trans(lcfirst($this->module) . '::index.title');
    }

    /**
     * Существует ли страница в дереве сайта.
     *
     * @return bool
     */
    protected function hasTreePage() : bool
    {
        return module_exist(lcfirst($this->module));
    }

    /**
     * Получение названия функции обработчика поля.
     *
     * @param string $key
     *
     * @return string
     */
    protected function getFunctionName(string $key) : string
    {
        $key = explode('_', $key);

        for ($i = 0; $i < count($key); $i++) {
            $key[$i] = ucfirst($key[$i]);
        }

        $key = implode('', $key);

        return 'get' . $key . 'Field';
    }

    /**
     * Выделить текст поиска в резултатах.
     *
     * @param $str
     * @return mixed
     */
    protected function highLight($str)
    {
        if (!is_string($str)) return $str;

        $mark = '<mark class="h-highlight">$1</mark>';

        if (count($this->queryWords)) {
            foreach ($this->queryWords as $word) {
                $str = preg_replace("/({$this->query})/iu", $mark, $str);
            }

            return $str;
        }

        if (!mb_strpos($this->query, ' ')) {
            return preg_replace("/({$this->query})/iu", $mark, $str);
        }

        $words = explode(' ', $this->query);
        if (is_array($words)) {
            foreach ($words as $word) {
                $str = preg_replace("/({$word})/iu", $mark, $str);
            }
        }

        return $str;
    }
}
