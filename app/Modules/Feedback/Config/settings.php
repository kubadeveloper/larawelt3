<?php
return [
    'title'        => trans('feedback::admin.title'),
    'localization' => true,
    'front_link'   => false,
    'acl'          => true,
    'acl_scheme' => [
        'permissions' => [
            'view' => ['IndexController@index', 'IndexController@edit']
        ]
    ]
];

