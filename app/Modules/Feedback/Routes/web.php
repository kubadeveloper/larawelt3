<?php

Route::localizedGroup(function () {
    Route::prefix(config('cms.uri'))
        ->as(config('cms.admin_prefix'))
        ->group(function () {
            Route::resource('feedback', 'Admin\IndexController');
        });
    Route::post('feedback', 'IndexController@store')->name('feedback.store');
});