<?php

namespace App\Modules\Feedback\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FeedbackTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('feedback')->insert(
            [
                'id'         => 1,
                'lang'       => 'ru',
                'date'       => date("Y-m-d"),
                'ip'         => ip2long('127.0.0.1'),
                'name'       => 'Обратная связь',
                'email'      => 'admin@admin.ru',
                'message'    => 'Hello Kuba Dev',
                'created_at' => now()
            ]
        );
    }
}
