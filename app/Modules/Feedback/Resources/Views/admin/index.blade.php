@extends('admin::admin.index')

@section('topmenu')
    <div class="header-module-controls">
        @include('admin::common.topmenu.list', ['routePrefix' => $routePrefix])
    </div>
@endsection

@section('filters')
    {!! AdminBootForm::open(['route' => 'admin.settings.store', 'method' => 'post']) !!}
    <div class="box box-primary box-filters">
        <div class="box-header"></div>
        <div class="box-body">
            <div class="col-md-4">
                {!! AdminBootForm::text('settings[lat]', trans('feedback::index.lat'),  Settings::get('lat')) !!}
            </div>

            <div class="col-md-4">
                {!! AdminBootForm::text('settings[lng]', trans('feedback::index.lng'),  Settings::get('lng')) !!}
            </div>

            <div class="col-md-4">
                {!! AdminBootForm::text('settings[zoom]', trans('feedback::index.zoom'),  Settings::get('zoom')) !!}
            </div>

            <div class="col-md-12">
                {!! AdminBootForm::submit(trans('admin::admin.save')) !!}
            </div>
        </div>
    </div>
    {!! AdminBootForm::close() !!}
@endsection

@section('th')
    <th>@sortablelink('date', trans('admin::fields.date'))</th>
    <th>@sortablelink('email', trans('admin::fields.email'))</th>
    <th>@sortablelink('name', trans('admin::fields.name'))</th>
    <th>@lang('admin::admin.control')</th>
@endsection

@section('td')
    @foreach ($entities as $entity)
        <tr>
            <td>{{ $entity->date }}</td>
            <td>{{ $entity->email }}</td>
            <td>{{ $entity->name }}</td>
            <td class="controls">
                @include('admin::common.controls.look', ['routePrefix' => $routePrefix, 'id' => $entity->id])
                @include('admin::common.controls.destroy', ['routePrefix' => $routePrefix, 'id' => $entity->id])
            </td>
        </tr>
    @endforeach
@endsection