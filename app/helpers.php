<?php

use App\Facades\Route;
use App\Modules\Acl\Facades\Permissions;
use App\Modules\Widgets\Facades\Widget;
use App\Modules\Tree\Facades\Tree;
use Illuminate\Support\Facades\App;

if (!function_exists('host')) {
    function host()
    {
        return sprintf("%s://%s", host_protocol(), @$_SERVER['SERVER_NAME']);
    }
}
if (!function_exists('host_protocol')) {
    function host_protocol()
    {
        return isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http';
    }
}
if (!function_exists('lang')) {
    function lang()
    {
        return localization()->getCurrentLocale();
    }
}
if (!function_exists('widget')) {
    function widget($slug)
    {
        return Widget::get($slug);
    }
}
if (!function_exists('home')) {
    function home()
    {
        return localization()->getLocalizedURL(lang(), '/');
    }
}
if (!function_exists('module')) {
    function module()
    {
        return Route::getModule();
    }
}
if (!function_exists('module_config')) {
    function module_config($section = false, $module = false, $default = null)
    {
        if (!$module) {
            $module = module();
        }
        if (!$module) {
            return false;
        }
        $str = 'modules.items.' . $module;
        if ($section) {
            $str .= '.' . $section;
        }

        return config($str, $default);
    }
}
//IndexController@index
if (!function_exists('action')) {
    function action()
    {
        return Route::getAction();
    }
}
////index
if (!function_exists('method')) {
    function method()
    {
        return Route::getMethod();
    }
}
if (!function_exists('is_allowed')) {
    function is_allowed($permission, $module = null)
    {
        if (!$module) {
            $module = module();
        }

        return Permissions::isPermissionAllowed($permission, $module);
    }
}
if (!function_exists('is_action_allowed')) {
    function is_action_allowed($action = null, $module = null)
    {
        if (!$action) {
            $action = action();
        }
        if (!$module) {
            $module = module();
        }

        return Permissions::isActionAllowed($action, $module);
    }
}
if (!function_exists('is_route_allowed')) {
    function is_route_allowed($route = null)
    {
        if (is_string($route)) {
            $route = Route::getRouteByName($route);
        }
        if (!$route) {
            $route = Route::getCurrentRoute();
        }

        return Permissions::isRouteAllowed($route);
    }
}
if (!function_exists('module_exist')) {
    function module_exist(string $module, string $locale = '') : bool
    {
        return (bool)Tree::getByModule($module, $locale);
    }
}
if (!function_exists('module_route')) {
    function module_route(string $module, string $suffix = '', array $data = [])
    {
        $tree  = Tree::getByModule($module);
        $route = $tree->slug;

        if ($suffix) $route .= '.' . $suffix;

        return route($route, $data);
    }
}
if (!function_exists('app_env')) {
    function app_env($env = null)
    {
        return $env ? App::environment() === $env : App::environment();
    }
}
if (!function_exists('app_is_prod')) {
    function app_is_prod()
    {
        return app_env('production');
    }
}
