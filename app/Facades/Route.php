<?php

namespace App\Facades;

class Route extends \Illuminate\Support\Facades\Route
{
    //index
    public static function getMethod($route = null)
    {
        if (!$route) {
            $route = parent::getCurrentRoute();
        }
        if (!$route) {
            return false;
        }
        $action = explode('@', $route->getActionName());
        if (!isset($action[1])) {
            return false;
        }

        return $action[1];
    }

    public static function getModule($route = null)
    {
        if (!$route) {
            $route = parent::getCurrentRoute();
        }
        if (!$route) {
            return false;
        }
        $action = $route->getActionName();
        preg_match('!App\\\Modules\\\(.*)\\\!isU', $action, $res);
        if (isset($res[1])) {
            return strtolower($res[1]);
        }

        return false;
    }

    //IndexController@index
    public static function getAction($route = null)
    {
        if (!$route) {
            $route = parent::getCurrentRoute();
        }
        if (!$route) {
            return false;
        }
        $action = $route->getActionName();
        preg_match('!.*\\\([^\\\]*@[^\\\]*)$!isU', $action, $res);
        if (!isset($res[1])) {
            return false;
        }

        return $res[1];
    }

    public static function getRouteByName($name)
    {
        foreach (parent::getRoutes() as $route) {
            if (isset($route->action) && isset($route->action['as']) && $route->action['as'] == $name) {
                return $route;
            }
        }

        return false;
    }
}