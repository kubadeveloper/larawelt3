<?php

namespace App\Http\Controllers;

abstract class IndexController extends Controller
{
    public function index()
    {
        return view('layouts.app');
    }

    public function about()
    {
        dd('ABOUT!!');
    }
}