<?php

namespace App\Http\Controllers;

use App\Facades\Uploader;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

trait File
{
    /**
     * Если в используемом контроллере определен метод after,
     * вызвать данную функцию в конце after.
     *
     * @param $entity
     *
     * @throws ValidationException
     */
    protected function after($entity)
    {
        if (method() == 'store' || method() == 'update') {
            $this->upload($entity);
        }

        if (method() == 'destroy') {
            $this->deleteUploads($entity);
        }
    }

    /**
     * Загрузка изображений и файлов на сервер.
     *
     * @param $entity
     *
     * @throws ValidationException
     */
    public function upload($entity)
    {
        $configs = $this->getConfigs();
        $fields  = $this->uploadsValidator($entity, $configs);

        foreach ($fields as $field) {
            if (Request::hasFile($field)) {
                $file = Request::file($field);

                if (Uploader::upload($file, $configs[$field])) {
                    $entity->{$field} = Uploader::getName();

                    if($fieldName = $entity->getFieldName($field)) {
                        $entity->{$fieldName} = Request::file($field)->getClientOriginalName();
                    }
                }
            }
        }

        $entity->save();
    }

    /**
     * Скачивание файла.
     *
     * @param $id
     * @param $field
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download($id, $field)
    {
        $entity = $this->getModel()->findOrFail($id);

        return response()->download($entity->getFileAbsPath($field), $entity->{$entity->getFieldName($field)});
    }

    /**
     * Удаление изображения или файла.
     *
     * @param $id
     * @param $field
     */
    public function deleteUpload($id, $field)
    {
        $entity  = $this->getModel()->findOrFail($id);
        $configs = $this->getConfigs();

        if (array_key_exists($field, $configs)) {
            if (isset($entity->{$field})) {
                if (Uploader::delete($entity->{$field}, $configs[$field])) {
                    $entity->{$field} = null;

                    if($entity->getFieldName($field)) {
                        $entity->{$entity->getFieldName($field)} = null;
                    }

                    $entity->save();
                }
            }
        }
    }

    /**
     * Удаление изображений и файлов при удалении записи.
     *
     * @param $entity
     */
    protected function deleteUploads($entity)
    {
        $configs = $this->getConfigs();

        foreach ($entity->getUploadFields() as $field) {
            Uploader::delete($entity->{$field}, $configs[$field]);
        }
    }

    /**
     * Валидация загружаемых на сервер изображений и файлов.
     *
     * @param $entity
     * @param $configs
     *
     * @return array|null
     *
     * @throws ValidationException
     */
    protected function uploadsValidator($entity, $configs)
    {
        $intersections = array_flip(array_intersect_key(array_flip($entity->getUploadFields()), $entity->getAttributes()));

        if (count($intersections) === 0) {
            return [];
        }

        $rules = [];
        foreach ($intersections as $intersection) {
            $rules[$intersection] = $this->getValidatorMimes($configs[$intersection]['validator']);
        }

        $this->validateImageDimensions();

        $validator = Validator::make(Request::all(), $rules, $this->getUploadMessages(), $this->getUploadAttributes());
        if ($validator->fails()) {
            foreach ($intersections as $intersection) {
                if (array_key_exists($intersection, $validator->errors()->messages())) {
                    $entity->{$intersection} = null;
                }
            }

            $entity->save();

            throw new ValidationException($validator);
        }

        return $intersections;
    }

    /**
     * Получение сообщений ошибок валидации.
     *
     * @return array
     */
    protected function getUploadMessages() : array
    {
        return [];
    }

    /**
     * Получение названия полей валидации.
     *
     * @return array
     */
    protected function getUploadAttributes() : array
    {
        return [];
    }

    /**
     * Получение конфигов.
     *
     * @return bool|\Illuminate\Config\Repository|mixed
     */
    protected function getConfigs()
    {
        return module_config('uploads');
    }

    /**
     * Не позволяем загружать изображения со стороной более 3000 пикселей
     *
     * @throws ValidationException
     */
    public function validateImageDimensions()
    {
        $imageSizeValidator = Validator::make(Request::all(), [
                'image' => 'dimensions:max_width=3000,max_height=3000'
            ], [
                'dimensions' => trans('admin::admin.image_too_large')
            ]
        );

        if ($imageSizeValidator->fails()) {
            throw new ValidationException($imageSizeValidator);
        }
    }

    protected function getValidatorMimes($validator)
    {
        if(preg_match('#.*(mimes:[^|]+).*#', $validator, $matches)) {
            return $validator;
        }

        return 'mimetypes:' . implode(',', config('lfm.folder_categories.file.valid_mime')) . '|' . $validator;
    }
}
