<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\TransformsRequest;

class StripTags extends TransformsRequest
{
    protected $except = [
        '_token', 'captcha', 'email', 'password', 'password_confirmation'
    ];

    public function handle($request, Closure $next, ...$attributes)
    {
        $index = array_search(config('cms.uri'), $request->segments());

        /*
        * В админке не применяем обрезку т.к теги из полей Ckeditor будут тоже обрезаться.
        * Проверка индекса добавлена для “защиты от дурака“, случай если на фронте в роуте будет содержаться адрес
        * админки(в текущий момент guarddog)
        */
        if ((is_bool($index) && $index !== false) || (is_int($index) && $index <= 1)) {
            return $next($request);
        }

        $this->attributes = $attributes;

        $this->clean($request);

        return $next($request);
    }

    protected function transform($key, $value)
    {
        if (in_array($key, $this->except, true)) {
            return $value;
        }

        if (is_array($value)) {
            $tmp = [];

            foreach ($value as $key => $item) {
                $tmp[$key] = is_string($item) ? strip_tags($item) : $item;
            }

            return $tmp;
        }

        return is_string($value) ? strip_tags($value) : $value;
    }
}
