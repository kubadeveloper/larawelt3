<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class WWWRedirect
{
    public function handle(Request $request, Closure $next)
    {
        $host = $request->getHost();

        if (app_is_prod() && preg_match('/^www\../', $host)) {
            return redirect()->secure(preg_replace('/www\./', '', $request->fullUrl()), 301);
        }

        return $next($request);
    }
}
