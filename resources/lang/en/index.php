<?php
return [
    '401'     => ['title' => 'Error 401', 'content' => 'Access denied'],
    '403'     => ['title' => 'Error 403', 'content' => 'Forbidden'],
    '404'     => ['title' => 'Error 404', 'content' => 'Page not found'],
    'browser' => 'You are using an <strong>outdated</strong> browser. Please
        <a href="http://browsehappy.com" target="_blank">update your browser</a> to better display the site.'
];
