@extends('layouts.inner')

@section('meta-title', config('app.name'))
@section('h1', trans('index.403.title'))

@section('content')
    <p>@lang('index.403.content')</p>
@endsection
