@extends('layouts.inner')

@section('meta-title', config('app.name'))
@section('h1', trans('index.401.title'))

@section('content')
    <p>@lang('index.401.content')</p>
@endsection
