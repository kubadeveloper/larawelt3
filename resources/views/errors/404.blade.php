@extends('layouts.inner')

@section('meta-title', config('app.name'))
@section('h1', trans('index.404.title'))

@section('content')
    <p>@lang('index.404.content')</p>
@endsection
