@if(isset($og) && is_object($og))
    @foreach($og as $property => $content)
        @if($property && $content)
            <meta property="og:{{ $property }}" content="{{ $content }}">
        @endif
    @endforeach
@endif

@if(isset($meta) && is_object($meta))
    @if(isset($meta->keywords) && $meta->keywords)
        <meta name="keywords" content="{{ $meta->keywords }}">
    @endif
    @if(isset($meta->description) && $meta->description)
        <meta name="description" content="{{ $meta->description }}">
    @endif
@endif
