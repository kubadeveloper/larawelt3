<!DOCTYPE html>
<html class="no-js" lang="{{ lang() }}">
<head>
    <meta charset="utf-8">

    @hasSection('meta-title')
        <title>@yield('meta-title')</title>
    @elseif(isset($meta->title) && $meta->title)
        <title>{{ $meta->title }}</title>
    @endif

    @include('common.meta')

    <meta name="format-detection" content="telephone=no">
    <meta name="robots" content="noodp, noydir">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="HandheldFriendly" content="true">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/modules.css">
    <link rel="stylesheet" href="/css/main.css">

    @stack('css')

    <script src="/js/jquery-2.2.4.min.js"></script>
    <script src="/js/modernizr.js"></script>
    <script src="/js/bootstrap.js"></script>
</head>
<body class="page">
    <!--[if lt IE 9]><p class="browsehappy">@lang('index.browser')</p><![endif]-->

    <div class="page__content">
        <div class="page__wrapper">
            <div class="page__header">
                <header class="header">
                    <div class="navbar navbar-default">
                        <div class="container">
                            <div class="navbar-header">
                                <button class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navbar</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>

                                <a class="navbar-brand" href="{{ home() }}">
                                    {{ config('app.name') }}
                                </a>
                            </div>

                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                @include('tree::menu')
                                @include('common.languages')
                                @include('users::main')
                                @include('search::main')
                            </div>
                        </div>
                    </div>
                </header>
            </div>

            <div class="page__main">
                <div class="container">
                    @yield('page_content')
                </div>
            </div>

            <div class="page__buffer"></div>
        </div>

        <div class="page__footer">
            <footer class="footer">
                <div class="container">
                    <address>{!! widget('footer') !!}</address>
                </div>
            </footer>
        </div>
    </div>

    <script src="/js/plugins.js"></script>
    <script src="/js/main.js"></script>

    @stack('js')
</body>
</html>
