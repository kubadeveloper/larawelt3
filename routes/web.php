<?php

use App\Modules\Tree\Facades\TreeRoutes;

try {
    Route::localizedGroup(function () {
        Route::middleware(['page', 'meta'])->group(function () {
            foreach (TreeRoutes::get() as $route) {
                Route::get($route->url, $route->action)->name($route->name);
            }
        });
    });
} catch (PDOException $e) {
    //
}
