<?php

use App\Modules\Acl\Database\Seeds\AclTableSeeder;
use App\Modules\Admins\Database\Seeds\AdminsTableSeeder;
use App\Modules\Articles\Database\Seeds\ArticlesTableSeeder;
use App\Modules\Feedback\Database\Seeds\FeedbackTableSeeder;
use App\Modules\Gallery\Database\Seeds\GalleryTableSeeder;
use App\Modules\News\Database\Seeds\NewsTableSeeder;
use App\Modules\Tree\Database\Seeds\TreeTableSeeder;
use App\Modules\Users\Database\Seeds\UsersTableSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AclTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
        $this->call(ArticlesTableSeeder::class);
        $this->call(FeedbackTableSeeder::class);
        $this->call(GalleryTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(TreeTableSeeder::class);
        $this->call(UsersTableSeeder::class);
    }
}
