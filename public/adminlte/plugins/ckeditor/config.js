CKEDITOR.editorConfig = function( config ) {
    config.pasteFromWordRemoveFontStyles = true;
    config.forcePasteAsPlainText = true;
    config.extraPlugins = 'pastefromword,youtube';

    /*config.contentsCss = [
        '/css/slick.css',
        '/css/jquery.fancybox.css',
        '/css/style.css',
        '/css/reset.css',
        '/css/media.css',
        '/css/fonts.css',
        '/css/custom.css'
    ];*/
    CKEDITOR.on( 'dialogDefinition', function( ev ) {
        // Take the dialog name and its definition from the event data.
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;
        // Check if the definition is from the dialog we're
        // interested in (the "Table" dialog).
        if ( dialogName == 'table' ) {
            // Get a reference to the "Table Info" tab.
            var infoTab = dialogDefinition.getContents( 'info' );
            txtWidth = infoTab.get( 'txtWidth' );
            txtWidth['default'] = '100%';
        }
    });

    config.stylesSet = [
        {name: 'Чёрный текст', element: 'span', styles:{color:'#212121'}},
        {name: 'Красный текст', element: 'span', styles:{color:'#cc1518'}},
        {name: 'Серый текст', element: 'span', styles:{color:'#8e8e8e'}},
        {name: 'Бордовый текст', element: 'span', styles:{color:'#a90329'}}
    ];

    config.toolbar = [
        { name: 'document', items: [ 'Source', '-', 'Save', 'NewPage', /*'Print',*/ 'Templates','Undo','Redo' ] },
        { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-','RemoveFormat' ] },
        { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-' ] },
        { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
        '/',
        { name: 'styles', items: [ 'Styles', 'Format', 'FontSize' ] },
        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
        { name: 'insert', items: [ 'Image',  'Table', 'Youtube', 'HorizontalRule', 'SpecialChar'/*, 'Iframe'*/ ] },
        { name: 'about', items: [ 'About' ] }
    ];

};

CKEDITOR.on('dialogDefinition', function( ev ) {
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;

    if(dialogName === 'table') {
        var infoTab = dialogDefinition.getContents('info');
        var cellSpacing = infoTab.get('txtCellSpace');
        cellSpacing['default'] = "0";
        var cellPadding = infoTab.get('txtCellPad');
        cellPadding['default'] = "0";
        var border = infoTab.get('txtBorder');
        border['default'] = "0";
    }
});