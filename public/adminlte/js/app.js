'use strict';

// Admin namespace
var admin = {};

// Layout fix
admin.layout = (function() {
    var body = document.body;
    var html = document.documentElement;

    var sidebarElement = document.querySelector('.main-sidebar');
    var contentElement = document.querySelector('.content-wrapper');
    var headerElement = document.querySelector('.main-header');
    var footerElement = document.querySelector('.main-footer');

    function init() {
        events();
        setHeight();
    }

    function events() {
        window.onresize = function() {
            setHeight();
        }
    }

    function setHeight() {
        // Высота остальных элементов
        var headerHeight = headerElement.offsetHeight;
        var footerHeight = footerElement.offsetHeight;

        // Сбрасываем высоту
        sidebarElement.style.minHeight = '';
        contentElement.style.minHeight = '';

        // Находим высоту контента
        var pageHeight = Math.max(
            body.scrollHeight, html.scrollHeight,
            body.offsetHeight, html.offsetHeight,
            body.clientHeight, html.clientHeight
        );

        // Указываем высоту
        sidebarElement.style.minHeight = pageHeight - headerHeight + 'px';
        contentElement.style.minHeight = pageHeight - headerHeight - footerHeight + 'px';
    }

    init();

    return {
        setHeight: setHeight
    }
})();

// Sidebar
admin.sidebar = (function() {
    var sidebar = '.main-sidebar';

    function events() {
        $(document).on('click', '.hamburger', function() {
            openSidebar();
        });

        $(document).on('click', '.main-sidebar__close', function() {
            closeSidebar();
        });

        $(document).on('click', function(event) {
            if ($(event.target).closest(sidebar).length
                || $(event.target).closest('.hamburger').length ) {
                return;
            };
            closeSidebar();
        });
    }

    function openSidebar() {
        $(sidebar).addClass('is-opened');
    }

    function closeSidebar() {
        $(sidebar).removeClass('is-opened');
    }

    events();

    return {
        open: openSidebar,
        close: closeSidebar
    }
})();

// Menu
(function() {
    $(document).off('click', '.sidebar .treeview-group').on('click', '.sidebar .treeview-group', function (e) {
        var parentBlock = $(this).closest('.treeview');
        parentBlock.find('.treeview-menu').slideToggle(400);
        parentBlock.toggleClass('active');
        parentBlock.find('.treeview-menu').toggleClass('is-active');
    });
})();

// Submit form
(function() {
    var $button = document.querySelector('.js-submit');

    if ($button) {
        init();
    }

    function init() {
        setEvents();
    }

    function setEvents() {
        $button.addEventListener('click', clickHandler);
    }

    function clickHandler(e) {
        e.preventDefault();
        updateCKEditor();
        submitForm();
    }

    // CKEditor должен сначал обновиться чтобы данные в нём сохранились
    // Берём их из массива CKEditorInstances
    function updateCKEditor() {
        if (CKEditorInstances.length) {
            for (var i = CKEditorInstances.length - 1; i >= 0; i--) {
                if (CKEditorInstances[i].editor) {
                    CKEditorInstances[i].editor.updateElement();
                }
            }
        }
    }

    function submitForm() {
        document.forms[0].submit();
    }
})();
