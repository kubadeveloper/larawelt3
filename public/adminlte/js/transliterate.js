// Транслитерируем текст
var Transliterate = (function() {
    var $from;
    var $to;

    var options;

    function init(newOptions) {
        setOptions(newOptions);

        setElements();
        setEvents();
        type();
    }

    function setOptions(newOptions) {
        options = newOptions;
    }

    function setElements() {
        $from = document.querySelector(options.from);
        $to = document.querySelector(options.to);
    }

    function setEvents() {
        $from.addEventListener('keyup', type);
        $from.addEventListener('change', type);
    }

    function type() {
        var newValue = $from.value;
        newValue = translitLetters(newValue);
        newValue = translitAnother(newValue);
        $to.value = newValue;
    }

    function translitLetters(text) {
        var en = getEnglishAlphabet();
        var ru = getRussianAlphabet();

        text = text.trim().toLowerCase();

        for (var i = 0; i < ru.length; i++) {
            var reg = new RegExp(ru[i], 'g');
            text = text.replace(reg, en[i]);
        }

        return text;
    }

    function getEnglishAlphabet() {
        return [
            'a', 'b', 'v', 'g', 'd', 'e', 'yo', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'shsh', '', 'y', '', 'e', 'yu', 'ya'
        ];
    }

    function getRussianAlphabet() {
        return [
            'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'
        ];
    }

    function translitAnother(text) {
        var newText = text;

        newText = newText.replace(/\s/g, '-');
        newText = newText.replace(/[^a-z0-9\-]+/g, '');
        newText = newText.replace(/\-+/g, '-');
        newText = newText.replace(/^\-|\-$/g, '');

        return newText;
    }

    return {
        init: init
    }
})();
