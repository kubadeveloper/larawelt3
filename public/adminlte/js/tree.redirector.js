'use strict';

(function () {
    var selectors = {
        checkbox: 'redirector',
        redirectorField: 'redirect_animation',
        contentField: 'content_animation',
        moduleField: 'module'
    };

    function init() {
        var $checkbox = document.getElementById(selectors.checkbox);
        var $module = document.getElementById(selectors.moduleField);
        var $hide = document.getElementById(selectors.redirectorField);

        if ($checkbox === null) {
            return null;
        }

        if ($checkbox.checked) {
            $hide = document.getElementById(selectors.contentField);
            $module.setAttribute('disabled', 'disabled');
        }

        $hide.style.display = 'none';
        $hide.style.opacity = '0';

        $checkbox.onclick = function (ev) {
            var $redirect = document.getElementById(selectors.redirectorField);
            var $content = document.getElementById(selectors.contentField);

            if (this.checked) {
                $module.setAttribute('disabled', 'disabled');
                animate($content, $redirect, 60);
            } else {
                $module.removeAttribute('disabled');
                animate($redirect, $content, 60);
            }
        }
    }

    function animate(hideElement, showElement, FPS) {
        var step = 1000 / FPS;

        hideElement.style.opacity = '1';

        var timer = setInterval(function () {
            if (parseFloat(hideElement.style.opacity) <= 0) {
                hideElement.style.display = 'none';
                showElement.style.display = 'block';

                clearInterval(timer);

                var childTimer = setInterval(function () {
                    if (showElement.style.opacity >= 1) {
                        clearInterval(childTimer);

                        return null;
                    }

                    showElement.style.opacity = (parseFloat(showElement.style.opacity) + 0.1).toString();
                }, step);

                return null;
            }

            hideElement.style.opacity = (parseFloat(hideElement.style.opacity) - 0.1).toString();
        }, step)
    }

    init();
})();
