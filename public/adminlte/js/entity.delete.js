'use strict';

function deleteUpload(sure) {
    if (confirm(sure)) {
        var url = this.dataset.href;
        var token = this.dataset.token;

        if (url && token) {
            var xhr = new XMLHttpRequest();

            xhr.open('DELETE', url);
            xhr.setRequestHeader('X-CSRF-TOKEN', token);
            xhr.send();

            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    location.reload();
                }
            }
        }
    }
}
